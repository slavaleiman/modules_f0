#!/usr/bin/python3
from modbus import *
from di_registers import *

if len(sys.argv) > 2:
    port = sys.argv[1]
    module_addr = sys.argv[2]
else:
    print("USAGE: %s PORT DEVICE_ADDR" % sys.argv[0])
    exit(0)

set_port(port, 115200)

while True:
    read_holding_regs(module_addr, di_holding_regs)
    read_input_regs(module_addr, di_input_regs)
    system('clear')
    for hr in di_holding_regs:
        print("%15s \t%d\t%s\t%s"%(hr, di_holding_regs[hr][0], di_holding_regs[hr][2], di_holding_regs[hr][3]))
    for ir in di_input_regs:
        print("%15s \t%d\t%s\t%s"%(ir, di_input_regs[ir][0], di_input_regs[ir][2], di_input_regs[ir][3]))

ser.close()
