#!/usr/bin/python3
from modbus import *
from di_registers import *
from do_registers import *

if len(sys.argv) > 1:
    port = sys.argv[1]
else:
    print("USAGE: %s PORT" % sys.argv[0])
    exit(0)

do_module_addr = 57
di_module_addr = 58
#module_baudrate = 9600
module_baudrate = 115200

baudrate_hi = module_baudrate >> 16
baudrate_lo = module_baudrate & 0xFFFF

INIT_ADDR = 1
DI_SPARE_INIT_REG = 109
DO_SPARE_INIT_REG = 69

# set spare = 1 to all possible rates
set_port(port, 9600)
write_holding_reg(1, DI_SPARE_INIT_REG, 1)
write_holding_reg(1, DO_SPARE_INIT_REG, 1)

set_port(port, module_baudrate)
write_holding_reg(di_module_addr, DI_SPARE_INIT_REG, 1)
write_holding_reg(do_module_addr, DO_SPARE_INIT_REG, 1)

set_port(port, 38400)
write_holding_reg(INIT_ADDR, DI_SPARE_INIT_REG, 1)
write_holding_reg(INIT_ADDR, DO_SPARE_INIT_REG, 1)

# TODO why this can be? not init state, but rate 38400
write_holding_reg(di_module_addr, DI_SPARE_INIT_REG, 1)
write_holding_reg(do_module_addr, DO_SPARE_INIT_REG, 1)
