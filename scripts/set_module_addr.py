#!/usr/bin/python3
import sys 
from modbus import *
if len(sys.argv) < 2:
	print("set module to INIT state")
	print("USAGE: %s ADDR BAUDRATE" % sys.argv[0])
	exit(1)

set_port("/dev/ttyUSB0", int(sys.argv[2]))
write_holding_reg(1, 22, int(sys.argv[1]))
