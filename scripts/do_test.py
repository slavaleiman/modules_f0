#!/usr/bin/python3
from modbus import *
from di_registers import *
from do_registers import *
from blackbox_test import *

if len(sys.argv) > 1:
    port = sys.argv[1]
else:
    print("USAGE: %s PORT" % sys.argv[0])
    exit(0)

def test_DOmode2(channel):
	# clean old params
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 0)
	# set mode 2 
	print("set 1 mode for DO{}".format(channel))
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 2)

	# test rate
	print("set 4 mode for DI{}".format(channel))
	# set DI input 0 mode 4
	write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 4)
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_L".format(channel)][0], 200) # valid delta
	test_rate(channel, 1, 2)
	test_rate(channel, 10)
	test_rate(channel, 100)
	test_rate(channel, 500)
	test_rate(channel, 5000) # it is maximum for DO
	test_rate(channel, 10)
	test_rate(channel, 1, 2)
	
def test_DOmode3(channel):
	# clean old params
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 0)
	# set mode 2 
	print("set 1 mode for DO{}".format(channel))
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 3)
	
	# test rate
	print("set 4 mode for DI{}".format(channel))
	# set DI input 0 mode 4
	write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 4)
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_L".format(channel)][0], 200) # valid delta
	test_rate(channel, 1, 2)
	test_rate(channel, 10)
	test_rate(channel, 100)
	test_rate(channel, 500)
	test_rate(channel, 5000) # it is maximum for DO
	test_rate(channel, 10)
	test_rate(channel, 1, 2)

def test_channel(channel):
	print("######### CHANNEL: {} ########".format(channel))
	test_DOmode2(channel)
	# exit_test(channel)
	test_DOmode3(channel)
	# exit_test(channel)

if __name__ == "__main__":
	# exit from init mode
	set_port(port, 38400)
	readid = read_input_reg(1, 0, 2)
	if readid: # modules in init state, SPARES UP
		print("in init rate")
		write_holding_reg(1, DI_SPARE_INIT_REG, 1)
		write_holding_reg(1, DO_SPARE_INIT_REG, 1)
	else:
		print("modules in work state")

	set_port(port, module_baudrate)

# РЕЖИМЫ DI
# 0 чтение выхода компаратора
# 1 устранение дребезга
# 2 счетчик количества импульсов
# 3 измерение длительности активного сигнала
# 4 измерение частоты, на прерываниях и общем таймере
# 5 измерение частоты, на основе INPUT_CAPTURE
# 6 измерение параметров импульсов, на основе PWM_INPUT

# РЕЖИМЫ DO
# 0 - высокоимпедансное состояние
# 1 - дискретный режим
# 2 - генератор прямоугольника на общем таймере 7 (мигающие лампочки)
# 3 - тревожный сигнал (генерация ШИМ сигналов пилообразной формы на аппаратном таймере для пьезоизлучателя)

	test_channel(0)
	test_channel(1)
	test_channel(2)
	test_channel(3)
	test_channel(4)
	test_channel(5)
	test_channel(6)
	test_channel(7)

	print("Everything passed")
