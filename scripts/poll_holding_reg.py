#!/usr/bin/python3
from modbus import *

if len(sys.argv) > 2:
	set_port(sys.argv[1])
	device_addr = sys.argv[2]
	regnum = sys.argv[3]
else:
	print("USAGE: %s PORT DEVICE_ADDR REGISTER")
	exit(0)

while True:
    read_holding_reg(device_addr, int(regnum), 2)
    sleep(0.3)
