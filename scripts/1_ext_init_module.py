#!/usr/bin/python3
from modbus import *
from di_registers import *
from do_registers import *

if len(sys.argv) > 1:
    port = sys.argv[1]
else:
    print("USAGE: %s PORT" % sys.argv[0])
    exit(0)

DO_ID = 21800
DI_ID = 21801

do_module_addr = 57
di_module_addr = 58
module_baudrate = 9600
#module_baudrate = 115200

baudrate_hi = module_baudrate >> 16
baudrate_lo = module_baudrate & 0xFFFF

INIT_ADDR = 1
DI_SPARE_INIT_REG = 109
DO_SPARE_INIT_REG = 69

def configure_do():
	print("configure_do...")
    # write module address
	# ["RS485_ADDRESS",    22, 	1, 	0,	0],
	write_holding_reg(INIT_ADDR, 22, do_module_addr)
	# write module baudrate
	# ["RS485_BAUDRATE_H", 23, 	1, 	0,	0],
	# ["RS485_BAUDRATE_L", 24, 	1, 	0,	0],
	write_holding_reg(INIT_ADDR, 23, baudrate_hi)
	write_holding_reg(INIT_ADDR, 24, baudrate_lo)
	# write config params
	write_holding_reg(INIT_ADDR, 29, 3)

	print("spare_init = 1")
	write_holding_reg(INIT_ADDR, DO_SPARE_INIT_REG, 1)

def configure_di():
	print("configure_di...")

	write_holding_reg(INIT_ADDR, 22, di_module_addr)
	write_holding_reg(INIT_ADDR, 23, baudrate_hi)
	write_holding_reg(INIT_ADDR, 24, baudrate_lo)
	write_holding_reg(INIT_ADDR, 29, 4)

	print("spare_init = 1")
	write_holding_reg(INIT_ADDR, DI_SPARE_INIT_REG, 1)

def get_module_id(ret=""):
	print(ret)
	mess = str(ret).replace(" ", "")
	mod_id = mess[6:10]
	return mod_id

def init_module():
	# read module ID
	module_address = 1
	print("read module id")
	ret = read_holding_reg(module_address, 0, 1)
	if ret:
		module_id = int(get_module_id(ret), 16)
		print("module_id: " + str(module_id))
		if module_id == DO_ID:
			configure_do()
		if module_id == DI_ID:
			configure_di()
	else:
		print("MODULE NOT RESPONDING")

# set spare = 0 to all possible rates
set_port(port, 9600)
write_holding_reg(di_module_addr, DI_SPARE_INIT_REG, 0)
write_holding_reg(do_module_addr, DO_SPARE_INIT_REG, 0)

set_port(port, module_baudrate)
write_holding_reg(do_module_addr, DO_SPARE_INIT_REG, 0)
write_holding_reg(di_module_addr, DI_SPARE_INIT_REG, 0)

set_port(port, 38400)
write_holding_reg(di_module_addr, DI_SPARE_INIT_REG, 0)
write_holding_reg(do_module_addr, DO_SPARE_INIT_REG, 0)

sleep(0.5)
input("\ntry init module 1")
init_module()

sleep(0.5)
input("\ntry init module 2")
init_module()

# sleep(0.5)
# set_port(port, module_baudrate)
# write_holding_reg(do_module_addr, DO_SPARE_INIT_REG, 0)
# write_holding_reg(di_module_addr, DI_SPARE_INIT_REG, 0)
