#!/usr/bin/python3

from modbus import *
from di_registers import *
from do_registers import *
from blackbox_test import *

if len(sys.argv) > 1:
    port = sys.argv[1]
else:
    print("USAGE: %s PORT" % sys.argv[0])
    exit(0)

def test_duty(w_rate, w_duty, sleep_time=0.3): # sleep in sec
	channel = 0
	print("set rate : %d" % w_rate)
	for channel in (0,4):
		print("\n -------------- channel:%d -------------- " % channel)
		set_do_channel_rate(channel, w_rate)
		set_do_channel_duty(channel, w_duty)
		sleep(sleep_time)

	read_duties = [get_duty_cycle(channel) for channel in (0,4)]
	print("              duty: " + str(read_duties))
	for read_duty in read_duties:
		d = read_duty / 10
		assert (d <= w_duty + 4 and d >= w_duty - 4), "read duty invalid!"

def test_rates():
	test_duty(1, 1, 2)
	test_duty(2, 2, 1)
	test_duty(10, 10, 1)
	test_duty(100, 20, 1)
	test_duty(200, 30)
	test_duty(500, 60)
	test_duty(900, 80)
	test_duty(1000, 90, 1)
	test_duty(1600, 91)
	test_duty(2000, 95, 1)
	test_duty(5000, 96, 1)
	test_duty(10, 98)
	test_duty(1, 99, 2)

def clear_modes():
	print("set 0 mode for all DO channels")
	for channel in range(0,8):
		ret = write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 0)
		print("<< " + str(ret))
		ret = write_holding_reg(do_module_addr, do_holding_regs["DO{}_PARAM_1".format(channel)][0], 200) # valid delta
		print("<< " + str(ret))
		ret = write_holding_reg(do_module_addr, do_holding_regs["DO{}_PARAM_3".format(channel)][0], 0)
		print("<< " + str(ret))
	print("set 0 mode all DI channels")
	for channel in range(0,8):
		ret = write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 0)
		print("<< " + str(ret))

def test_all_6(channel=0):
	# test compare read and write
	# set DO output 0 mode 2
	print("set 3 mode for all DO channels")
	for channel in (0,4):
		write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 3)
		write_holding_reg(do_module_addr, do_holding_regs["DO{}_PARAM_1".format(channel)][0], 200) # valid delta
		write_holding_reg(do_module_addr, do_holding_regs["DO{}_PARAM_3".format(channel)][0], 0)

	print("set 6 mode all DI channels")
	for channel in (0,4):
		write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 6)
		write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_1".format(channel)][0], 1)

	test_rates()

if __name__ == "__main__":
	# exit from init mode
	set_port(port, 38400)
	readid = read_input_reg(1, 0, 2)
	if readid: # modules in init state, SPARES UP
		print("in init rate")
		write_holding_reg(1, di_holding_regs["SPARE-INIT"][0], 1)
		write_holding_reg(1, do_holding_regs["SPARE-INIT"][0], 1)
	else:
		print("modules in work state")

	set_port(port, module_baudrate)
	clear_modes()
	test_all_6()
