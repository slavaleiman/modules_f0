#!/usr/bin/python3

from modbus import *
from di_registers import *
from do_registers import *
from blackbox_test import *

if len(sys.argv) > 1:
    port = sys.argv[1]
else:
    print("USAGE: %s PORT" % sys.argv[0])
    exit(0)

def test_rate_all(w_rate, sleep_time=0.4): # sleep in sec
	print("set rate : %d" % w_rate)

	for channel in range(first_channel,num_channels + first_channel):
		# input("press enter")
		set_do_channel_rate(channel, w_rate)

	sleep(sleep_time)

	read_rates = [get_rate(channel) for channel in range(first_channel,num_channels + first_channel)]
	print(read_rates)
	for read_rate in read_rates:
		r = read_rate / 100
		k = r / w_rate
		assert (k <= 1.07 and k >= 0.93), "read rate invalid!"
		assert check_valid_flag(channel) != -1, "valid delta flag set"
	# input()

def test_rates():
	test_rate_all(1, 4)
	# test_rate_all(5, 1)
	# test_rate_all(10)
	test_rate_all(50)
	# test_rate_all(100)
	test_rate_all(200)
	test_rate_all(2, 2)
	# test_rate_all(300)
	test_rate_all(500)
	test_rate_all(1000)
	test_rate_all(1, 4)

def set_all_0():
	print("set 0 mode for all DO channels")
	for channel in range(first_channel,num_channels + first_channel):
		write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 0)

	# set DI input 0 mode 4
	print("set 0 mode all DI channels")
	for channel in range(first_channel, num_channels + first_channel):
		write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 0)

def test_all_4():
	chan_from = 0
	# test compare read and write
	# set DO output 0 mode 2
	print("set 2 mode for all DO channels")
	for channel in range(first_channel,num_channels + first_channel):
		write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 0)
		write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 2)

	# set DI input 0 mode 4
	print("set 4 mode all DI channels")
	for channel in range(first_channel,num_channels + first_channel):
		write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 0)
		write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_L".format(channel)][0], 200) # valid delta
		write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 4)
	test_rates()

def test_all_5():
	# test compare read and write
	# set DO output 0 mode 2
	print("set 2 mode for all DO channels")
	for channel in range(first_channel,num_channels + first_channel):
		write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 0)
		write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 2)

	print("set 5 mode all DI channels")
	for channel in range(first_channel,num_channels + first_channel):
		write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 0)
		write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 5)
		write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_L".format(channel)][0], 200) # valid delta
	test_rates()

first_channel = 0
num_channels = 8

if __name__ == "__main__":
	# exit from init mode
	set_port(port, 115200)
	readid = read_input_reg(1, 0, 2)
	if readid: # modules in init state, SPARES UP
		print("in init rate")
		write_holding_reg(1, di_holding_regs["SPARE-INIT"][0], 1)
		write_holding_reg(1, do_holding_regs["SPARE-INIT"][0], 1)
	else:
		print("modules in work state")

	set_port(port, module_baudrate)

	set_all_0()
	# test_all_4()
	# test_all_5()
	for i in range(20):
		test_all_5()
		# test_all_4()
	print("DONE")
