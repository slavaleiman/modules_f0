#!/usr/bin/python3
# -*- coding: iso-8859-15 -*-

import time
import serial
import sys
import struct

if len(sys.argv) < 2:
    print("USAGE: %s [port]" % sys.argv[0])
    sys.exit(1)

ser = serial.Serial(sys.argv[1], baudrate=9600, timeout=3.0)

def pack_command(values):
    for i in values:
        string += struct.pack('!B', i)

while 1 :
    if(ser.isOpen() == False):
        ser.open()

    input_string = input(">> ")

    if input_string == 'exit' or input_string == 'q':
        ser.close()
        exit()
    else:
        ser.write(str.encode(input_string))
        out = ''
        seq = []
        time.sleep(0.6)
        while ser.inWaiting() > 0:
            for c in ser.read():
                seq.append(chr(c)) # convert from ANSII
                out = ''.join(str(v) for v in seq) # Make a string from array
                if chr(c) == '\n':
                    break
        if out != '':
            print("%s" % out)
