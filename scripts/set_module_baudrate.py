#!/usr/bin/python3
import sys 
from modbus import *
if len(sys.argv) < 3:
	print("set module to INIT state")
	print("USAGE: %s ADDR CURRENT_BAUDRATE NEW_BAUDRATE" % sys.argv[0])
	exit(1)

set_port("/dev/ttyUSB0", int(sys.argv[2]))
br = int(sys.argv[3])
write_holding_reg(int(sys.argv[1]), 23, br >> 16)
write_holding_reg(int(sys.argv[1]), 24, br & 0xFFFF)

