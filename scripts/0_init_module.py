#!/usr/bin/python3
from modbus import *

if len(sys.argv) > 2:
    port        = sys.argv[1]
    assign_addr = int(sys.argv[2])
    module_baudrate = int(sys.argv[3])
else:
    print("USAGE: %s PORT ASSIGN_ADDR BAUDRATE" % sys.argv[0])
    exit(0)

DO_ID = 21800
DI_ID = 21801

if not module_baudrate:
    module_baudrate = 115200

baudrate_hi = module_baudrate >> 16
baudrate_lo = module_baudrate & 0xFFFF

INIT_ADDR = 1
DI_SPARE_INIT_REG = 109
DO_SPARE_INIT_REG = 69

def configure_do():
    print("configure_do...")
        # write module address
    # ["RS485_ADDRESS",    22,  1,  0,  0],
    write_holding_reg(INIT_ADDR, 22, assign_addr)
    # write module baudrate
    # ["RS485_BAUDRATE_H", 23,  1,  0,  0],
    # ["RS485_BAUDRATE_L", 24,  1,  0,  0],
    write_holding_reg(INIT_ADDR, 23, baudrate_hi)
    write_holding_reg(INIT_ADDR, 24, baudrate_lo)
    # write config params
    write_holding_reg(INIT_ADDR, 29, 3)

def configure_di():
    print("configure_di...")

    write_holding_reg(INIT_ADDR, 22, assign_addr)
    write_holding_reg(INIT_ADDR, 23, baudrate_hi)
    write_holding_reg(INIT_ADDR, 24, baudrate_lo)
    write_holding_reg(INIT_ADDR, 29, 4)

def get_module_id(ret=""):
    print(ret)
    mess = str(ret).replace(" ", "")
    mod_id = mess[6:10]
    return mod_id

def init_module():
    # read module ID
    ret = read_holding_reg(INIT_ADDR, 0, 1, 1)
    if ret:
        print("read module id: %s" % ret)
        module_id = int(get_module_id(ret), 16)
        print("module_id: " + str(module_id))
        if module_id == DO_ID:
            configure_do()
        if module_id == DI_ID:
            configure_di()
    else:
        print("MODULE NOT RESPONDING")

set_port(port, 4800)
sleep(0.1)

init_module()
