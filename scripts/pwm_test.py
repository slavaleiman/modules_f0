#!/usr/bin/python3

from modbus import *
from di_registers import *
from do_registers import *
from blackbox_test import *

if len(sys.argv) > 1:
    port = sys.argv[1]
else:
    print("USAGE: %s PORT" % sys.argv[0])
    exit(0)

def test_rate_all(w_rate, w_duty, channels, sleep_time=0.3): # sleep in sec
	channel = 0
	print("set rate : %d" % w_rate)

	for channel in channels:
		print("\n -------------- channel:%d -------------- " % channel)
		set_do_channel_rate(channel, w_rate)
		set_do_channel_duty(channel, w_duty)
		sleep(sleep_time)

	read_rates = [get_rate(channel) for channel in channels]
	print("              rate: " + str(read_rates))
	for read_rate in read_rates:
		r = read_rate / 100
		k = r / w_rate
		assert (k <= 1.05 and k >= 0.95), "read rate invalid!"

def test_rates(channels):
	test_rate_all(1, 40, channels, 2)
	test_rate_all(2, 40, channels, 1)
	test_rate_all(3, 40, channels, 1)
	test_rate_all(5, 40, channels, 1)
	test_rate_all(10, 40, channels, 1)
	test_rate_all(100, 40, channels, 1)
	test_rate_all(200, 40, channels)
	test_rate_all(300, 40, channels)
	test_rate_all(400, 40, channels, 1)
	test_rate_all(500, 40, channels)
	test_rate_all(700, 40, channels)
	test_rate_all(900, 40, channels)
	test_rate_all(1000, 40, channels, 1)
	test_rate_all(1600, 40, channels)
	test_rate_all(2000, 40, channels, 1)
	test_rate_all(5000, 40, channels, 1)
	test_rate_all(15000, 40, channels, 1)
	test_rate_all(10, 40, channels)
	test_rate_all(1, 40, channels, 2)

def clear_modes():
	print("set 0 mode for all DO channels")
	for channel in range(0,8):
		ret = write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 0)
		print("<< " + str(ret))
		ret = write_holding_reg(do_module_addr, do_holding_regs["DO{}_PARAM_1".format(channel)][0], 200) # valid delta
		print("<< " + str(ret))
		ret = write_holding_reg(do_module_addr, do_holding_regs["DO{}_PARAM_3".format(channel)][0], 0)
		print("<< " + str(ret))
	print("set 0 mode all DI channels")
	for channel in range(0,8):
		ret = write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 0)
		print("<< " + str(ret))

def test_all_6(channels=(0,4)):
	# test compare read and write
	# set DO output 0 mode 2
	print("set 3 mode for all DO channels")
	for channel in channels:
		write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 3)
		write_holding_reg(do_module_addr, do_holding_regs["DO{}_PARAM_1".format(channel)][0], 200) # valid delta
		write_holding_reg(do_module_addr, do_holding_regs["DO{}_PARAM_3".format(channel)][0], 0)

	print("set 6 mode all DI channels")
	for channel in channels:
		write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 6)

	test_rates(channels)

if __name__ == "__main__":
	# exit from init mode
	set_port(port, 38400)
	readid = read_input_reg(1, 0, 2)
	if readid: # modules in init state, SPARES UP
		print("in init rate")
		write_holding_reg(1, di_holding_regs["SPARE-INIT"][0], 1)
		write_holding_reg(1, do_holding_regs["SPARE-INIT"][0], 1)
	else:
		print("modules in work state")

	set_port(port, module_baudrate)
	clear_modes()
	test_all_6((0,4))
	clear_modes()
	test_all_6((1,5))
