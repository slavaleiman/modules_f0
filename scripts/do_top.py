#!/usr/bin/python3
from modbus import *
from do_registers import *

if len(sys.argv) > 2:
    port = sys.argv[1]
    module_addr = sys.argv[2]
else:
    print("USAGE: %s PORT DEVICE_ADDR" % sys.argv[0])
    exit(0)

set_port(port, 115200)

while True:
    read_holding_regs(module_addr, do_holding_regs)
    system('clear')
    for hr in do_holding_regs:
        print("%15s \t%d\t%s\t%s"%(hr, do_holding_regs[hr][0], do_holding_regs[hr][2], do_holding_regs[hr][3]))
    sleep(0.02)

ser.close()
