#!/usr/bin/python3
from modbus import *
from di_registers import *
from do_registers import *

if len(sys.argv) > 1:
    port = sys.argv[1]
else:
    print("USAGE: %s PORT" % sys.argv[0])
    exit(0)

DO_ID = 21800
DI_ID = 21801

do_module_addr = 57
di_module_addr = 58

DI_SPARE_INIT_REG = 109
DO_SPARE_INIT_REG = 69

module_baudrate = 115200

def set_do_channel_rate(channel, rate):
	print("            set rate: %d" % rate)
	write_holding_reg(do_module_addr, do_holding_regs["DO{}_PARAM_1".format(str(channel))][0], rate)

def set_do_channel_duty(channel, duty):
	print("            set duty: %d" % duty)
	write_holding_reg(do_module_addr, do_holding_regs["DO{}_PARAM_2".format(str(channel))][0], duty)

def get_rate(channel):
	# read rate VAL0
	str_r_rate = read_input_reg(di_module_addr, di_input_regs["VAL{}".format(str(channel))][0], 2)
	if(str_r_rate):
		print(str_r_rate)
		spl = str_r_rate.split()
		r_rate = int(''.join(spl[3:7]), 16)
		return r_rate
	else:
		print("ERROR")
		return -1
	return 0

def get_duty_cycle(channel):
	# read rate VAL1
	str_cycle = read_input_reg(di_module_addr, di_input_regs["duty_cycle_{}".format(str(channel))][0], 2)
	if(str_cycle):
		spl = str_cycle.split()
		r_cycle = int(''.join(spl[3:7]), 16)
		return r_cycle
	else:
		print("ERROR")
		return -1
	return 0

def check_valid_flag(channel):
	validness = read_input_reg(di_module_addr, di_input_regs["FLAG{}".format(str(channel))][0], 1)
	if(validness):
		print(validness)
		spl = validness.split()
		flag = int(''.join(spl[3:5]), 16)
		print("read valid flag : %s" % flag)
		return int(flag)
	else:
		return -1

def test_rate(channel, w_rate, sleep_time=0.5): # sleep in sec
	set_do_channel_rate(channel, w_rate)
	# pause
	sleep(sleep_time)
	read_rate = get_rate(channel)
	print("read rate: %d" % read_rate)
	r = read_rate / 100
	k = r / w_rate
	print(k)
	assert (k <= 1.05 and k >= 0.95), "read rate invalid!"
	check_valid_flag(channel)

def test_dimode0(channel):
	print("######### channel: %d TEST 0 COMPARATOR ######### " % channel)
	# check inverse bit here
	# set hi level output read state
	print("set 0 mode for DI0")
	write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(str(channel))][0], 0) # discrete mode
	
	print("set 1 mode for DO0")
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(str(channel))][0], 1) # discrete mode

	print("set DO0 = 1 wait DI0 == 0")
	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(str(channel))][0], 1)
	rd = read_input_reg(di_module_addr, di_input_regs["VAL{}".format(str(channel))][0], 2)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte1 = int(spl[6],16)
	assert rd_byte1 == 0, "read incorrect" # must be inversed

	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 0)
	print("set DO0 = 0 wait DI0 == 1")
	rd = read_input_reg(di_module_addr, di_input_regs["VAL{}".format(channel)][0], 2)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte2 = int(spl[6],16)
	assert rd_byte2 == 1, "read incorrect" # must be inversed

def test_dimode1(channel):
	print("######### channel: %d TEST 1 FLUTTER #########" % channel)
	print("set 1 mode for DI{}".format(channel))
	write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 0) # discrete mode
	write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 1) # discrete mode
	print("set 1 mode for DO{}".format(channel))
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 0)
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 1) # discrete mode

	print("set DO0 = 1 wait DI{} == 0".format(channel))
	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 1)
	rd = read_input_reg(di_module_addr, di_input_regs["VAL{}".format(channel)][0], 2)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte1 = int(spl[6],16)
	assert rd_byte1 == 0, "read incorrect" # must be inversed

	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 0)
	print("set DO{0} = 0 wait DI{0} == 1".format(channel))
	rd = read_input_reg(di_module_addr, di_input_regs["VAL{}".format(channel)][0], 2)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte2 = int(spl[6],16)
	assert rd_byte2 == 1, "read incorrect" # must be inversed

def test_mode2_default_polarity(channel): 
	print("######### channel: %d TEST 2 IMPULSE COUNT DEFAULT #########" % channel)
	print("set 1 mode for DO".format(channel)) 
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 1) # discrete mode
	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 1)

	print("set 2 mode for DI{}".format(channel))
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_H".format(channel)][0], 0) # start counter value
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_L".format(channel)][0], 0) # start counter value

	rd = read_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_L".format(channel)][0], 1)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte1 = int(spl[4],16)
	# print("read PARAM_2 = " + str(rd_byte1))
	assert rd_byte1 == 0, "read incorrect" # must be inversed

	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_1".format(channel)][0], 0 | (1 << 1)) # polarity
	write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 2) # impulse count

	rd = read_input_reg(di_module_addr, di_input_regs["VAL{}".format(channel)][0], 2)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte1 = int(spl[6],16)
	print("read count = " + str(rd_byte1))
	assert rd_byte1 == 0, "init count must be 0"
	# default polarity 
	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 1)
	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 0)

	rd = read_input_reg(di_module_addr, di_input_regs["VAL{}".format(channel)][0], 2)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte1 = int(spl[6],16)
	print("read count = " + str(rd_byte1))
	assert rd_byte1 == 1, "read incorrect"

	for i in range(0,26):
		write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 1)
		write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 0)

	rd = read_input_reg(di_module_addr, di_input_regs["VAL{}".format(channel)][0], 2)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte1 = int(spl[6],16)
	print("read count = " + str(rd_byte1))
	assert rd_byte1 == 27, "read incorrect" # must be inversed

def test_mode2_inverse_polarity(channel): 
	print("######### channel: %d TEST 2 IMPULSE COUNT INVERSE #########" % channel)
	print("set 1 mode for DO0") 
	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 0)
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 1) # discrete mode
	print("set 2 mode for DI0")
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_H".format(channel)][0], 0) # start counter value
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_L".format(channel)][0], 0) # start counter value
	rd = read_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_L".format(channel)][0], 1)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte1 = int(spl[4],16)
	print("read PARAM_2 = " + str(rd_byte1))
	assert rd_byte1 == 0, "read incorrect" # must be inversed
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_1".format(channel)][0], 3) # polarity and start counter from
	write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 2) # impulse count
	rd = read_input_reg(di_module_addr, di_input_regs["VAL{}".format(channel)][0], 2)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte1 = int(spl[6],16)
	print("read count = " + str(rd_byte1))
	assert rd_byte1 == 0, "init count must be 0"
	# inverse polarity 
	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 0)
	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 1)

	rd = read_input_reg(di_module_addr, di_input_regs["VAL{}".format(channel)][0], 2)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte1 = int(spl[6],16)
	print("read count = " + str(rd_byte1))
	assert rd_byte1 == 1, "read incorrect" # must be inversed

	for i in range(0,26):
		write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 0)
		write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 1)

	rd = read_input_reg(di_module_addr, di_input_regs["VAL{}".format(channel)][0], 2)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte1 = int(spl[6],16)
	print("read count = " + str(rd_byte1))
	assert rd_byte1 == 27, "read incorrect" # must be inversed

def test_dimode2(channel): 
	test_mode2_default_polarity(channel)
	test_mode2_inverse_polarity(channel)

def test_dimode3_default(channel):
	print("######### channel: %d TEST 3 ACTIVE COUNT DEFAULT #########" % channel)
	print("set 1 mode for DO{}".format(channel))
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 1) # discrete mode
	print("set 3 mode for DI{}".format(channel))
	write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 3) # active count
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_H".format(channel)][0], 0)
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_L".format(channel)][0], 0)
	rd = read_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_L".format(channel)][0], 1)
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_1".format(channel)][0], 1)
	rd = read_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_1".format(channel)][0], 1)
	print("read param1: " + str(rd))
	# -----------------------------------------------------------------------------------
	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 0)
	sleep(5)
	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 1)

	rd = read_input_reg(di_module_addr, di_input_regs["VAL{}".format(channel)][0], 2)
	print(rd)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte1 = int(spl[6],16)
	print("read count = " + str(rd_byte1))
	assert rd_byte1 >= 5 and rd_byte1 < 7, "init count must be 5"

def test_dimode3_inverse(channel):
	print("######### channel: %d TEST 3 ACTIVE COUNT INVERSE #########" % channel)
	print("set 1 mode for DO{}".format(channel))
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 1) # discrete mode
	print("set 2 mode for DI{}".format(channel))
	write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 3) # active count
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_H".format(channel)][0], 0)
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_L".format(channel)][0], 0)
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_1".format(channel)][0], 2)
	# --------------------------------------------------------------------------------

	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 1)
	sleep(5)
	write_holding_reg(do_module_addr, do_holding_regs["DO{}_VAL".format(channel)][0], 0)

	rd = read_input_reg(di_module_addr, di_input_regs["VAL{}".format(channel)][0], 2)
	print(rd)
	assert rd, "no return"
	spl = rd.split(" ")
	rd_byte1 = int(spl[6],16)
	print("read count = " + str(rd_byte1))
	assert rd_byte1 >= 5 and rd_byte1 < 7, "init count must be 5"

def test_dimode3(channel):
	test_dimode3_default(channel)
	test_dimode3_inverse(channel)

def test_dimode4(channel):
	print("######### channel: %d TEST 4 RATE MEASURE SOFT #########" % channel)
	print("set 2 mode for DO{}".format(channel))
	# test compare read and write
	# set DO output 0 mode 2
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 0)
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 2)
	print("set 4 mode for DI{}".format(channel))
	# set DI input 0 mode 4
	write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 0)
	write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 4)
	# write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_H".format(channel)][0], 0)
	# write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_L".format(channel)][0], 200)

	test_rate(channel, 1, 3)
	test_rate(channel, 10, 1)
	test_rate(channel, 100)
	test_rate(channel, 500)
	test_rate(channel, 5000) # it is maximum for DO
	test_rate(channel, 10, 1)
	test_rate(channel, 1, 3)

def test_dimode5(channel):
	print("######### channel: %d TEST 5 RATE MEASURE HARD #########" % channel)
	print("set 2 mode for DO{}".format(channel))
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 0)
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(channel)][0], 2)
	print("set 5 mode for DI{}".format(channel))

	write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(channel)][0], 5)
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_H".format(channel)][0], 0)
	write_holding_reg(di_module_addr, di_holding_regs["DI{}_PARAM_2_L".format(channel)][0], 200)
	test_rate(channel, 1, 3)
	test_rate(channel, 10, 1)
	test_rate(channel, 100, 1)
	test_rate(channel, 500)
	test_rate(channel, 5000) # it is maximum for DO
	test_rate(channel, 10, 1)
	test_rate(channel, 1, 3)

def enter_test(channel):
	# set mode 0 for di and do, this wil clear all params
	write_holding_reg(di_module_addr, di_holding_regs["MODE{}".format(str(channel))][0], 0) # discrete mode
	write_holding_reg(do_module_addr, do_holding_regs["MODE{}".format(str(channel))][0], 0) # discrete mode

def test_channel(channel):
	enter_test(channel)
	print("######### CHANNEL: {} ########".format(channel))
	test_dimode0(channel) # with direct connection modules you don't catch contact fluttering
	test_dimode1(channel)
	test_dimode2(channel)
	test_dimode3(channel)
	test_dimode4(channel)
	test_dimode5(channel)

if __name__ == "__main__":
	# exit from init mode
	set_port(port, 38400)
	readid = read_input_reg(1, 0, 2)
	if readid: # modules in init state, SPARES UP
		print("in init rate")
		write_holding_reg(1, DI_SPARE_INIT_REG, 1)
		write_holding_reg(1, DO_SPARE_INIT_REG, 1)
	else:
		print("modules in work state")

	set_port(port, module_baudrate)

# РЕЖИМЫ DI
# 0 чтение выхода компаратора
# 1 устранение дребезга
# 2 счетчик количества импульсов
# 3 измерение длительности активного сигнала
# 4 измерение частоты, на прерываниях и общем таймере
# 5 измерение частоты, на основе INPUT_CAPTURE
# 6 измерение параметров импульсов, на основе PWM_INPUT

# РЕЖИМЫ DO
# 0 - высокоимпедансное состояние
# 1 - дискретный режим
# 2 - генератор прямоугольника на общем таймере 7 (мигающие лампочки)
# 3 - тревожный сигнал (генерация ШИМ сигналов пилообразной формы на аппаратном таймере для пьезоизлучателя)

	test_channel(0)
	test_channel(1)
	test_channel(2)
	test_channel(3)
	test_channel(4)
	test_channel(5)
	test_channel(6)
	test_channel(7)

	print("Everything passed")
