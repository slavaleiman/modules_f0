# add-auto-load-safe-path C:\work\PLC_TECHO\PLC_TECHNO_F0\.gdbinit
tar ext :4242
set print pretty on
set pagination off

define suka
    set TIM6->CR1 &= ~1
    set TIM7->CR1 &= ~1
end

define killtims
    set TIM6->CR1 &= ~1
    set TIM1->CR1 &= ~1
    set TIM2->CR1 &= ~1
    set TIM3->CR1 &= ~1
    set TIM7->CR1 &= ~1    
end

define killall
    set TIM6->CR1 &= ~1
    set TIM1->CR1 &= ~1
    set TIM2->CR1 &= ~1
    set TIM3->CR1 &= ~1
    set TIM7->CR1 &= ~1
    set USART4->CR1 &= ~1
end

define resumeall
    set TIM6->CR1 |= 1
    set TIM1->CR1 |= 1
    set TIM2->CR1 |= 1
    set TIM3->CR1 |= 1
    set TIM7->CR1 |= 1
    set USART4->CR1 |= 1
end

define t1
    p/t *TIM1
end

define t2
    p/t *TIM2
end

define setall
    set device.input[0].mode = $arg0
    set device.input[1].mode = $arg0
    set device.input[2].mode = $arg0
    set device.input[3].mode = $arg0
    set device.input[4].mode = $arg0
    set device.input[5].mode = $arg0
    set device.input[6].mode = $arg0
    set device.input[7].mode = $arg0
end

define water_marks
    p modbusHighWaterMark
    p inputHighWaterMark
    p tickHighWaterMark
end

define status
    p device
    printf "current buadrate %d\n", huart4->Init->BaudRate
end
b HardFault_Handler
#b errors_on_error
#b ext_mem_load


#display /t GPIOB->ODR & (1<<13)
#display /t GPIOB->IDR & (1<<14)
#display /t GPIOB->IDR & (1<<15)
#display device.is_init
#display device.addr

b init_pwm_output
