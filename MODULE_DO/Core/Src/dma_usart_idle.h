#ifndef __DMA_USART_IDLE__
#define __DMA_USART_IDLE__ 1
#include "stm32f0xx_hal.h"
#include "cmsis_os.h"
extern UART_HandleTypeDef huart4;
#define HUART &huart4

#define DMA_RX_BUFFER_SIZE          255

extern uint8_t DMA_RX_Buffer_2[DMA_RX_BUFFER_SIZE];
typedef struct
{
    uint8_t* data;
    uint16_t len;
}message_t;

void USART4_IrqHandler(UART_HandleTypeDef *huart, DMA_HandleTypeDef *hdma);
void USART2_IrqHandler(UART_HandleTypeDef *huart, DMA_HandleTypeDef *hdma);
void DMA_IrqHandler(DMA_HandleTypeDef *hdma);
void rs485_recieve_enable(void);
void rs485_transmit_enable(void);
void dma_usart_idle_init(void);
void dma_usart_idle_reinit(void);

#endif
