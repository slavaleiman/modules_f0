#include "soft_timer.h"
#include <stdint.h>

#define TIMERS_NUM 9
soft_timer_t timers[TIMERS_NUM];

void soft_timer_init(uint32_t period, void (*cb)(uint8_t), uint8_t is_one_shot, uint8_t index)
{
    soft_timer_t* timer = &timers[index];
    if(timer->is_active && (timer->period == period) && (timer->cb == cb))
        return;
    timer->count = period;
    timer->period = period;
    timer->cb = cb;
    timer->one_shot = is_one_shot;
    timer->is_active = true;
}

void soft_timer_stop(uint8_t id)
{
    for(uint8_t i = 0; i < TIMERS_NUM; ++i)
    {
        if(i == id)
        {
            timers[i].is_active = false;
        }
    }
}

// called from tim7 irq
void soft_timer_tick(void)
{
    for(uint8_t i = 0; i < TIMERS_NUM; ++i)
    {
        soft_timer_t* t = &timers[i];
        if(t->is_active && t->count)
        {
            --t->count;
        }
    }
}

void soft_timer_poll(void)
{
    uint8_t toremove = 0;
    for(uint8_t i = 0; i < TIMERS_NUM; ++i)
    {
        soft_timer_t* t = &timers[i];
        if(t->is_active && (t->count == 0) && t->cb)
        {
            t->cb(i);
            if(t->one_shot)
            {
                t->is_active = false;
                ++toremove;
            }else{
                t->count = t->period;
            }
        }else{
            ++toremove;
        }
    }
    if(toremove)
    {
        for(uint8_t i = 0; i < TIMERS_NUM; ++i)
        {
            soft_timer_t* t = &timers[i];
            if(!t->is_active)
            {
                t->period = 0;
            }
        }
    }
}
