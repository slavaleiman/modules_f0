#ifndef _SOFT_TIMER_
#define _SOFT_TIMER_ 1

#include <stdbool.h>
#include <stdint.h>

// typedef struct soft_timer_s soft_timer_t;
typedef struct soft_timer_s
{
	uint32_t period;
    uint32_t count;
    void (*cb)(uint8_t);
    bool one_shot;
    bool is_active;
}soft_timer_t;

void soft_timer_init(uint32_t period, void (*cb)(uint8_t), uint8_t is_one_shot, uint8_t index);
void soft_timer_stop(uint8_t id);
void soft_timer_tick(void);
void soft_timer_poll(void);

#endif
