#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "errors.h"
#include "log.h"
#include "main.h"

#define ERRORS_BUFFER_SIZE 32 // количество типов ошибок

const char* const error_strings[] =
{
    "ошибка чтения конфигурации",         // CONFIG_LOAD_ERROR,
    "ошибка записи во внешнюю память",    // EXT_MEM_WRITE_ERROR,
    "RS485 оверран",                      // RS485_OVERRUN_ERROR,
    "ошибка доступа к регистру хранения", // MODBUS_HOLDING_REG_LOCKED,
    0
};

typedef struct
{
    int      err;       // номер ошибки
    uint16_t repeated;  // количество повторов
    uint32_t last_time; // from start time
}error_item_t;

struct errors_s
{
    bool         update;
    error_item_t buffer[ERRORS_BUFFER_SIZE];
    uint16_t     last_index; // позиция последней сохраненной ошибки
}errors;

const char* str_error(int32_t err)
{
    if(err > 0)
        return (char*)error_strings[err - 1];
    else
        return "";
}

void errors_on_error(int err)
{
    uint8_t index = 0;
    for(; index < ERRORS_BUFFER_SIZE; ++index)
    {
        if(err == errors.buffer[index].err)
        {
            ++errors.buffer[index].repeated;
            errors.buffer[index].last_time = operation_time();
            break;
        }
        else if(errors.buffer[index].err == 0)
        {
            errors.buffer[index].err = err;
            errors.buffer[index].last_time = operation_time();
            errors.last_index = index;
            break;
        }
    }
    errors.update = true;
}

void errors_init(void)
{
    memset(errors.buffer, 0, sizeof(errors.buffer));
    errors.last_index = 0;
    errors.update = true;
}

void errors_log_errors(void)
{
    uint16_t ecount = errors.last_index;
    log_out("---------------- DI errors: %u ----------------\n", ecount);
    for(uint16_t err_num = 0; err_num < ecount; ++err_num)
    {
        uint8_t err = errors.buffer[err_num].err;
        log_out("%u: %lu: %u %s", err_num, errors.buffer[err_num].last_time, err, str_error(err));
    }
}

uint8_t* errors_buffer(void)
{
    return (uint8_t*)errors.buffer;
}

uint16_t errors_size(void) // bytes
{
    return (sizeof(error_item_t) * errors.last_index);
}

bool errors_is_update(void)
{
    return errors.update;
}

void errors_update_clear(void)
{
    errors.update = false;
}

bool errors_is_error(int err)
{
    for(uint32_t index = 0; index < ERRORS_BUFFER_SIZE; ++index)
    {
        if(err == errors.buffer[index].err)
        {
            return true;
        }
    }
    return false;
}
