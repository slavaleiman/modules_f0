#ifndef __USER_CMD__
#define __USER_CMD__ 1

#include "stm32f0xx.h"
void user_cmd(uint8_t* message, uint8_t size);

#endif // __USER_CMD__
