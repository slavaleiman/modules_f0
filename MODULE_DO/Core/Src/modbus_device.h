#ifndef __MODBUS_DEVICE__
#define __MODBUS_DEVICE__ 1

#include <stdbool.h>
#include "ext_mem.h"
#include "stm32f0xx_hal_tim.h"

#define MBD_INIT_ADDR    1
#define MBD_NUM_OUTPUTS  8

#define DO0_PORT    GPIOC
#define DO0_PIN     GPIO_PIN_9
#define DO0_TIM     TIM3
#define DO1_PORT    GPIOC
#define DO1_PIN     GPIO_PIN_8
#define DO1_TIM     TIM3
#define DO2_PORT    GPIOC
#define DO2_PIN     GPIO_PIN_7
#define DO2_TIM     TIM3
#define DO3_PORT    GPIOC
#define DO3_PIN     GPIO_PIN_6
#define DO3_TIM     TIM3
#define DO4_PORT    GPIOA
#define DO4_PIN     GPIO_PIN_11
#define DO4_TIM     TIM1
#define DO5_PORT    GPIOA
#define DO5_PIN     GPIO_PIN_10
#define DO5_TIM     TIM1
#define DO6_PORT    GPIOA
#define DO6_PIN     GPIO_PIN_9
#define DO6_TIM     TIM1
#define DO7_PORT    GPIOA
#define DO7_PIN     GPIO_PIN_8
#define DO7_TIM     TIM1

#define DRV_ENABLE_PORT     GPIOC
#define DRV_ENABLE_PIN      GPIO_PIN_4

#define ADUM_ENABLE_PORT    GPIOC
#define ADUM_ENABLE_PIN     GPIO_PIN_5

#define NFAULT2_PORT        GPIOA
#define NFAULT2_PIN         GPIO_PIN_4

#define NFAULT1_PORT        GPIOA
#define NFAULT1_PIN         GPIO_PIN_5

#define EXT_INIT_BAUDRATE 38400
#define INT_INIT_BAUDRATE 4800

typedef struct
{
    uint8_t  mode;      // режим входа, все режимы используют значение со входа после масок
    uint16_t param_1;   // период активного сигнала в миллисекундах
    uint16_t param_2;   // высота звука (несущая частота) в килогерцах
    uint16_t param_3;   // скважность шима в процентах
    GPIO_TypeDef* gpio;
    TIM_TypeDef*  tim;
    uint16_t      pin;
    int8_t        new_mode; // для переконфигурации
}output_t;

typedef struct
{
    uint8_t  addr;
    uint32_t baudrate;

    uint32_t counter;
    uint32_t start_time;
    uint16_t count_ovr; // счетчик переполнений системного тика

    uint32_t operation_time;
    uint16_t program_crc;
    uint16_t ext_mem_crc;
    uint32_t change_time;
    uint32_t total_rq;   // счетчик запросов
    uint32_t handled_rq; // счетчик обработанных запросов
    uint32_t err_crc_rq; // счетчик запросов с ошибкой CRC
    uint32_t err_param_rq; // счетчик запросов с некорректными параметрами
    uint32_t err_send;  // счетчик ошибок при отправке

    uint16_t and_mask;
    uint16_t or_mask;

    uint16_t masked_output;
    uint16_t output_reg; // discrete values of DO ports

    output_t output[MBD_NUM_OUTPUTS];
    uint16_t update; // for store to ext memory : used 9 bits => 8 outputs + 1 common flag

    bool     is_init;
}device_t;

enum{
    RS485_ADDRESS_REG = 22,
    RS485_BAUDRATE_HREG,
    RS485_BAUDRATE_LREG,

    DO_REG,
    AND_MASK_REGISTER, // 26
    OR_MASK_REGISTER,
    MASKED_REGISTER,

// адреса параметров входов
    DO0_MODE_REG,   // 29
    PARAM1_REG_0,
    PARAM2_REG_0,
    PARAM3_REG_0,

    DO1_MODE_REG,  // 33
    PARAM1_REG_1,
    PARAM2_REG_1,
    PARAM3_REG_1,

    DO2_MODE_REG,  // 37
    PARAM1_REG_2,
    PARAM2_REG_2,
    PARAM3_REG_2,

    DO3_MODE_REG,  // 41
    PARAM1_REG_3,
    PARAM2_REG_3,
    PARAM3_REG_3,

    DO4_MODE_REG,  // 45
    PARAM1_REG_4,
    PARAM2_REG_4,
    PARAM3_REG_4,

    DO5_MODE_REG,  // 49
    PARAM1_REG_5,
    PARAM2_REG_5,
    PARAM3_REG_5,

    DO6_MODE_REG, // 53
    PARAM1_REG_6,
    PARAM2_REG_6,
    PARAM3_REG_6,

    DO7_MODE_REG,  // 57
    PARAM1_REG_7,
    PARAM2_REG_7,
    PARAM3_REG_7,

    DO0_VAL,    // 61
    DO1_VAL,    // 62
    DO2_VAL,    // 63
    DO3_VAL,    // 64
    DO4_VAL,    // 65
    DO5_VAL,    // 66
    DO6_VAL,    // 67
    DO7_VAL,    // 68

    SPARE_INIT, // 69
    DRV_ENABLE, // 70
    ADUM_ENABLE,// 71
}DO_MODE;

uint32_t mbd_curr_time(void);
uint32_t mbd_curr_time_ms(void);

int8_t mbd_discrete_output(uint8_t reg, uint16_t* value);
int8_t mbd_holding_reg(uint16_t reg, uint16_t* value);
int8_t mbd_write_reg(uint16_t reg, uint16_t value);

void mbd_inc_crc_err_rq(void);
void mbd_inc_handled_rq(void);
void mbd_inc_total_rq(void);

uint32_t mbd_update_time(void);
void mbd_process(void);
void mbd_init(void);

void mbd_on_output_capture(TIM_HandleTypeDef *htim);
void mbd_on_ovf(void);

void mbd_inc_counter(TIM_HandleTypeDef *htim);

void mbd_pwm_output_reset(TIM_HandleTypeDef *htim);
void mdb_inc_send_err(void);

void mbd_output_load(output_params_t* output, uint8_t output_num);
void mbd_output_params(output_params_t* output, uint8_t output_num);
void mbd_common_params(common_params_t* cp);

uint16_t mbd_update_status(void);
void mbd_clear_flags(void);

void mbd_set_common_params(common_params_t* cp);
uint32_t operation_time(void);

void mbd_log_status(void);

uint16_t mbd_module_id(void);

void mbd_log_config(void);
void mbd_init_mode(bool is_on);
void mbd_set_baudrate(uint32_t baudrate);

uint8_t mbd_addr(void);

int8_t mbd_rs485_init(uint32_t baudrate);
void mbd_ext_init(void);
void mbd_int_init(void);
void mbd_exit_init_mode(void);
bool mbd_can_init(void);
bool mbd_is_init(void);

#endif //__MODBUS_DEVICE__
