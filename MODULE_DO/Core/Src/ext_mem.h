#ifndef _EXT_MEM_
#define _EXT_MEM_ 1

#include <stdbool.h>

typedef struct __attribute__((__packed__))
{
    uint8_t  addr;
    uint16_t module_id;
    uint32_t operation_time;
    uint16_t program_crc;

    uint32_t change_time;
    uint32_t total_rq;   // счетчик запросов
    uint32_t handled_rq; // счетчик обработанных запросов
    uint32_t err_crc_rq; // счетчик запросов с ошибкой CRC
    uint32_t err_param_rq; // счетчик запросов с некорректными параметрами
    uint32_t err_send;  // счетчик ошибок при отправке

    uint16_t and_mask;
    uint16_t or_mask;

    uint32_t baudrate;
}common_params_t;

typedef struct __attribute__((__packed__))
{
    uint8_t mode;
    uint16_t param_1;
    uint16_t param_2;
    uint16_t param_3;
}output_params_t;

int8_t ext_mem_load(void);
int8_t ext_mem_store(void);
int8_t ext_mem_store_time(uint32_t time);
int8_t ext_mem_store_errors(void);
int8_t ext_mem_read_errors(void);
void   ext_mem_set_update_flag(void);
bool   ext_mem_get_update_flag(void);

#endif //_EXT_MEM_
