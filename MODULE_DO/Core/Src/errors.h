#ifndef _ERROR_H_
#define _ERROR_H_ 1

#include "stm32f0xx.h"
#include <stdbool.h>
#include "main.h"

#define MAX_ERRORS 36

#ifdef _UNITTEST_
	#define ERROR(err) \
	errors_on_error(err);
#else
	#define ERROR(err) \
	errors_on_error(err); \
	FAULT_LED_PORT->ODR |= FAULT_LED_PIN;
#endif    
	
typedef enum
{
    NO_ERROR = 0,
    CONFIG_LOAD_ERROR,
    EXT_MEM_WRITE_ERROR, // never saved
    RS485_OVERRUN_ERROR,
    MODBUS_HOLDING_REG_LOCKED,

    ERRORS_SIZE
}error_t;

const char* str_error(int32_t err);
void     errors_on_error(int err);
void     errors_init(void);
uint8_t* errors_buffer(void);
uint16_t errors_size(void); // bytes
void 	 errors_log_errors(void);
bool 	 errors_update(void);
bool 	 errors_is_update(void);
void 	 errors_update_clear(void);
bool     errors_is_error(int err); // for test
#endif //_ERROR_H_
