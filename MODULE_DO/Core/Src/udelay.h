#ifndef __UDELAY_H__
#define __UDELAY_H__
// at SystemCoreClock = 48000000
// 1 tick = 1.67e-7 sec
// 1usec = 8 ticks
#define delayUS(us) do {\
    asm volatile (  "MOV R0,%[loops]\n\t"\
            "1: \n\t"\
            "SUB R0, #1\n\t"\
            "CMP R0, #0\n\t"\
            "BNE 1b \n\t" : : [loops] "r" (8*us) : "memory"\
              );\
}while(0)
#endif
