#include "main.h"
#include "stm32f0xx.h"
#include "modbus_device.h"
#include "modbus.h"
#include <stdbool.h>
#include <string.h>
#include "ext_mem.h"
#include "log.h"
#include "errors.h"
#include "soft_timer.h"
#include "dma_usart_idle.h"
#include "version.h"

const uint16_t MODULE_ID = 21800;

#define PCB_VER     1
#define PCB_MNF     1 // manufacturer
#define PCB_ASSEM   1
#define SW_VER      MODULE_VERSION_MAJOR
#define SW_SUBVER   MODULE_VERSION_MINOR
#define SW_BUILD    1

#define MBD_RISING_FRONT_BIT    0x0
#define MBD_FALLING_FRONT_BIT   0x1

uint64_t TIM_CLOCK = 48000000;

TIM_HandleTypeDef htim7;

enum{
    INACTIVE = 0,
    DISCRETE_OUTPUT = 1, // запись значения через обычный
    RECTANGLE = 2,       // генерация периодичных сигналов на общем таймере
    HARD_PWM = 3,        // генерация ШИМ сигналов на аппаратном таймере
}output_mode_e;

device_t device;

uint32_t mbd_update_time(void)
{
    uint32_t counter = HAL_GetTick() / 1000;
    if(counter < device.counter) // если произошло переполнение
    {
        ++device.count_ovr;
    }
    device.counter = counter;
    device.operation_time = device.start_time + device.counter + device.count_ovr * (0xFFFFFFFF / 1000); // секунд
    return device.operation_time;
}

uint32_t operation_time(void)
{
    return device.operation_time;
}

int8_t mbd_discrete_output(uint8_t reg, uint16_t* value)
{
    switch(reg)
    {
        case DO_REG:
            *value = device.output_reg;
            return 0;
        default:
            return DATA_ADDR_ERR;
    }
}

void update_output(void)
{
#ifndef _UNITTEST_
    // device.output_reg = ((GPIOC->ODR >> 6) & 0xF) | (((GPIOA->ODR >> 8) & 0xF) << 4); // before Ilias peretasoval
    device.output_reg = ((__RBIT(GPIOC->ODR) >> 22) & 0xF) | (((__RBIT(GPIOA->ODR) >> 20) & 0xF) << 4);
#endif
}

void update_masked(void)
{
    update_output();
    device.masked_output = (device.output_reg & device.and_mask) | device.or_mask;
}

// oooooooooo  ooooooooooo      o      ooooooooo        oooooooooo   o      oooooooooo       o      oooo     oooo
//  888    888  888    88      888      888    88o       888    888 888      888    888     888      8888o   888
//  888oooo88   888ooo8       8  88     888    888       888oooo88 8  88     888oooo88     8  88     88 888o8 88
//  888  88o    888    oo    8oooo88    888    888       888      8oooo88    888  88o     8oooo88    88  888  88
// o888o  88o8 o888ooo8888 o88o  o888o o888ooo88        o888o   o88o  o888o o888o  88o8 o88o  o888o o88o  8  o88o

int8_t mbd_holding_reg(uint16_t reg, uint16_t* value)
{
    switch(reg)
    {
        case 0: *value = MODULE_ID;
            return 0;
        case 1: *value = PCB_VER;
            return 0;
        case 2: *value = PCB_MNF; // manufacturer
            return 0;
        case 3: *value = PCB_ASSEM;
            return 0;
        case 4: *value = SW_VER;
            return 0;
        case 5: *value = SW_SUBVER;
            return 0;
        case 6: *value = SW_BUILD;
            return 0;
        case 7:
            *value = device.operation_time >> 16;
            return 0;
        case 8:
            *value = device.operation_time & 0xFFFF;
            return 0;
        case 9:
            *value = HAL_GetTick() % 1000; // CURR_MS
            return 0;
        case 10:
            *value = device.program_crc;
            return 0;
        case 11:
            *value = device.ext_mem_crc;
            return 0;
        case 12:
            *value = device.change_time >> 16;
            return 0;
        case 13:
            *value = device.change_time & 0xFFFF;
            return 0;
        case 14:
            *value = device.total_rq >> 16;
            return 0;
        case 15:
            *value = device.total_rq & 0xFFFF;
            return 0;
        case 16:
            *value = device.handled_rq >> 16;
            return 0;
        case 17:
            *value = device.handled_rq & 0xFFFF;
            return 0;
        case 18:
            *value = device.err_crc_rq >> 16;
            return 0;
        case 19:
            *value = device.err_crc_rq & 0xFFFF;
            return 0;
        case 20:
            *value = device.err_param_rq >> 16;
            return 0;
        case 21:
            *value = device.err_param_rq & 0xFFFF;
            return 0;
        //  выходы
        case DO_REG:
            update_output();
            *value = device.output_reg;
            return 0;
        case AND_MASK_REGISTER:
            *value = device.and_mask;
            return 0;
        case OR_MASK_REGISTER:
            *value = device.or_mask;
            return 0;
        case MASKED_REGISTER:
            update_masked();
            *value = device.masked_output;
            return 0;

        // режимы измерений и их параметры
        case DO0_MODE_REG:
            *value = device.output[0].mode;
            return 0;
        case PARAM1_REG_0:
            *value = device.output[0].param_1;
            return 0;
        case PARAM2_REG_0:
            *value = device.output[0].param_2;
            return 0;
        case PARAM3_REG_0:
            *value = device.output[0].param_3;
            return 0;

        case DO1_MODE_REG:
            *value = device.output[1].mode;
            return 0;
        case PARAM1_REG_1:
            *value = device.output[1].param_1;
            return 0;
        case PARAM2_REG_1:
            *value = device.output[1].param_2;
            return 0;
        case PARAM3_REG_1:
            *value = device.output[1].param_3;
            return 0;

        case DO2_MODE_REG:
            *value = device.output[2].mode;
            return 0;
        case PARAM1_REG_2:
            *value = device.output[2].param_1;
            return 0;
        case PARAM2_REG_2:
            *value = device.output[2].param_2;
            return 0;
        case PARAM3_REG_2:
            *value = device.output[2].param_3;
            return 0;

        case DO3_MODE_REG:
            *value = device.output[3].mode;
            return 0;
        case PARAM1_REG_3:
            *value = device.output[3].param_1;
            return 0;
        case PARAM2_REG_3:
            *value = device.output[3].param_2;
            return 0;
        case PARAM3_REG_3:
            *value = device.output[3].param_3;
            return 0;

        case DO4_MODE_REG:
            *value = device.output[4].mode;
            return 0;
        case PARAM1_REG_4:
            *value = device.output[4].param_1;
            return 0;
        case PARAM2_REG_4:
            *value = device.output[4].param_2;
            return 0;
        case PARAM3_REG_4:
            *value = device.output[4].param_3;
            return 0;

        case DO5_MODE_REG:
            *value = device.output[5].mode;
            return 0;
        case PARAM1_REG_5:
            *value = device.output[5].param_1;
            return 0;
        case PARAM2_REG_5:
            *value = device.output[5].param_2;
            return 0;
        case PARAM3_REG_5:
            *value = device.output[5].param_3;
            return 0;

        case DO6_MODE_REG:
            *value = device.output[6].mode;
            return 0;
        case PARAM1_REG_6:
            *value = device.output[6].param_1;
            return 0;
        case PARAM2_REG_6:
            *value = device.output[6].param_2;
            return 0;
        case PARAM3_REG_6:
            *value = device.output[6].param_3;
            return 0;

        case DO7_MODE_REG:
            *value = device.output[7].mode;
            return 0;
        case PARAM1_REG_7:
            *value = device.output[7].param_1;
            return 0;
        case PARAM2_REG_7:
            *value = device.output[7].param_2;
            return 0;
        case PARAM3_REG_7:
            *value = device.output[7].param_3;
            return 0;
        case DO0_VAL:
            *value = (uint16_t)(device.output[0].gpio->ODR & device.output[0].pin) ? 1 : 0;
            return 0;
        case DO1_VAL:
            *value = (uint16_t)(device.output[1].gpio->ODR & device.output[1].pin) ? 1 : 0;
            return 0;
        case DO2_VAL:
            *value = (uint16_t)(device.output[2].gpio->ODR & device.output[2].pin) ? 1 : 0;
            return 0;
        case DO3_VAL:
            *value = (uint16_t)(device.output[3].gpio->ODR & device.output[3].pin) ? 1 : 0;
            return 0;
        case DO4_VAL:
            *value = (uint16_t)(device.output[4].gpio->ODR & device.output[4].pin) ? 1 : 0;
            return 0;
        case DO5_VAL:
            *value = (uint16_t)(device.output[5].gpio->ODR & device.output[5].pin) ? 1 : 0;
            return 0;
        case DO6_VAL:
            *value = (uint16_t)(device.output[6].gpio->ODR & device.output[6].pin) ? 1 : 0;
            return 0;
        case DO7_VAL:
            *value = (uint16_t)(device.output[7].gpio->ODR & device.output[7].pin) ? 1 : 0;
            return 0;
        case SPARE_INIT:
            *value = (SPARE_INIT_PORT->ODR & SPARE_INIT_PIN) ? 1 : 0; // ACTIVE 1
            return 0;
        case DRV_ENABLE:
            *value = (DRV_ENABLE_PORT->ODR & DRV_ENABLE_PIN) ? 0 : 1; // INVERSED!
            return 0;
        case ADUM_ENABLE:
            *value = (ADUM_ENABLE_PORT->ODR & ADUM_ENABLE_PIN) ? 1 : 0;
            return 0;

        case RS485_ADDRESS_REG:
            *value = device.addr;
            return 0;
        case RS485_BAUDRATE_HREG:
            *value = device.baudrate >> 16;
            return 0;
        case RS485_BAUDRATE_LREG:
            *value = device.baudrate & 0xFFFF;
            return 0;            
        default:
            break;
    }
    return DATA_ADDR_ERR;
}

TIM_TypeDef* get_tim(uint16_t DO)
{
    TIM_TypeDef* tim = NULL;
    if(DO < 4)
    {
        tim = TIM3;
    }
    else if(DO < 8){
        tim = TIM1;
    }
    return tim;
}

// TIM_CHANNEL_1
// TIM_CHANNEL_2
// TIM_CHANNEL_3
// TIM_CHANNEL_4
static uint32_t get_tim_channel(uint8_t DO)
{
    uint32_t channel = 0;
    switch(DO)
    {
        case 0: channel = TIM_CHANNEL_4;
            break;
        case 1: channel = TIM_CHANNEL_3;
            break;
        case 2: channel = TIM_CHANNEL_2;
            break;
        case 3: channel = TIM_CHANNEL_1;
            break;
        case 4: channel = TIM_CHANNEL_4;
            break;
        case 5: channel = TIM_CHANNEL_3;
            break;
        case 6: channel = TIM_CHANNEL_2;
            break;
        case 7: channel = TIM_CHANNEL_1;
            break;
        default:
            break;
    }
    return channel;
}

static uint16_t get_pin(uint8_t DO)
{
    if(DO < 4)
    {
        return __RBIT(1 << DO) >> 22; // PIN9 .. PIN6
    }
    else if(DO < 8)
    {
        return __RBIT(1 << DO) >> 16; // PIN11 .. PIN8
    }
    return 0;
}

void clear_output(uint8_t DO)
{
#ifndef _UNITTEST_
    soft_timer_stop(DO);
    if(device.output[DO].gpio && device.output[DO].pin)
        device.output[DO].gpio->ODR &= ~device.output[DO].pin;
    if(HARD_PWM == device.output[DO].mode && device.output[DO].tim)
    {
        TIM_TypeDef* tim = get_tim(DO);
        uint32_t channel = get_tim_channel(DO);
        if(!tim)
            return;
        tim->CR1 &= ~TIM_CR1_CEN;
        if(tim->CCER & (TIM_CCER_CC1E << channel))
        {
            TIM_CCxChannelCmd(tim, channel, TIM_CCx_DISABLE);
            device.output[DO].gpio->ODR &= ~device.output[DO].pin;
        }
    }
    memset(&device.output[DO], 0, sizeof(output_t));
#endif
}

void timer_7_init(void)
{
#ifndef _UNITTEST_
    if(htim7.State == HAL_TIM_STATE_READY)
        return;
    __HAL_RCC_TIM7_CLK_ENABLE();
    htim7.Instance = TIM7;
    htim7.Init.Period = 100 - 1;
    htim7.Init.Prescaler = 48 - 1;
    htim7.Init.ClockDivision = 0;
    htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
    if(HAL_TIM_Base_Init(&htim7) == HAL_OK)
    {
        HAL_TIM_Base_Start_IT(&htim7);
    }
    HAL_NVIC_SetPriority(TIM7_IRQn, 4, 0);
    HAL_NVIC_EnableIRQ(TIM7_IRQn);
#else
    printf("%s\n", __FUNCTION__);
#endif
}

void deinit_output(uint8_t DO)
{
    switch(DO)
    {
        case 0:
            HAL_GPIO_DeInit(DO0_PORT, DO0_PIN);
            break;
        case 1:
            HAL_GPIO_DeInit(DO1_PORT, DO1_PIN);
            break;
        case 2:
            HAL_GPIO_DeInit(DO2_PORT, DO2_PIN);
            break;
        case 3:
            HAL_GPIO_DeInit(DO3_PORT, DO3_PIN);
            break;
        case 4:
            HAL_GPIO_DeInit(DO4_PORT, DO4_PIN);
            break;
        case 5:
            HAL_GPIO_DeInit(DO5_PORT, DO5_PIN);
            break;
        case 6:
            HAL_GPIO_DeInit(DO6_PORT, DO6_PIN);
            break;
        case 7:
            HAL_GPIO_DeInit(DO7_PORT, DO7_PIN);
            break;
        default:
            break;
    }    
}

void init_discrete_output(uint8_t DO)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    switch(DO)
    {
        case 0:
            GPIO_InitStruct.Pin = DO0_PIN;
            HAL_GPIO_DeInit(DO0_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DO0_PORT, &GPIO_InitStruct);
            device.output[DO].gpio = DO0_PORT;
            device.output[DO].pin = DO0_PIN;
            device.output[DO].tim = DO0_TIM;
            break;
        case 1:
            GPIO_InitStruct.Pin = DO1_PIN;
            HAL_GPIO_DeInit(DO1_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DO1_PORT, &GPIO_InitStruct);
            device.output[DO].gpio = DO1_PORT;
            device.output[DO].pin = DO1_PIN;
            device.output[DO].tim = DO1_TIM;
            break;
        case 2:
            GPIO_InitStruct.Pin = DO2_PIN;
            HAL_GPIO_DeInit(DO2_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DO2_PORT, &GPIO_InitStruct);
            device.output[DO].gpio = DO2_PORT;
            device.output[DO].pin = DO2_PIN;
            device.output[DO].tim = DO2_TIM;
            break;
        case 3:
            GPIO_InitStruct.Pin = DO3_PIN;
            HAL_GPIO_DeInit(DO3_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DO3_PORT, &GPIO_InitStruct);
            device.output[DO].gpio = DO3_PORT;
            device.output[DO].pin = DO3_PIN;
            device.output[DO].tim = DO3_TIM;
            break;
        case 4:
            GPIO_InitStruct.Pin = DO4_PIN;
            HAL_GPIO_DeInit(DO4_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DO4_PORT, &GPIO_InitStruct);
            device.output[DO].gpio = DO4_PORT;
            device.output[DO].pin = DO4_PIN;
            device.output[DO].tim = DO4_TIM;
            break;
        case 5:
            GPIO_InitStruct.Pin = DO5_PIN;
            HAL_GPIO_DeInit(DO5_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DO5_PORT, &GPIO_InitStruct);
            device.output[DO].gpio = DO5_PORT;
            device.output[DO].pin = DO5_PIN;
            device.output[DO].tim = DO5_TIM;
            break;
        case 6:
            GPIO_InitStruct.Pin = DO6_PIN;
            HAL_GPIO_DeInit(DO6_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DO6_PORT, &GPIO_InitStruct);
            device.output[DO].gpio = DO6_PORT;
            device.output[DO].pin = DO6_PIN;
            device.output[DO].tim = DO6_TIM;
            break;
        case 7:
            GPIO_InitStruct.Pin = DO7_PIN;
            HAL_GPIO_DeInit(DO7_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DO7_PORT, &GPIO_InitStruct);
            device.output[DO].gpio = DO7_PORT;
            device.output[DO].pin = DO7_PIN;
            device.output[DO].tim = DO7_TIM;
            break;
        default:
            break;
    }
}

static void turn_output_state(uint8_t DO)
{
#ifndef _UNITTEST_
    if(DO > 7)
        return;

    if((device.output[DO].mode == RECTANGLE) && device.output[DO].gpio)
    {
        device.output[DO].gpio->ODR ^= device.output[DO].pin;
    }
    if((device.output[DO].mode == HARD_PWM) && device.output[DO].tim)
    {
        uint32_t channel = get_tim_channel(DO);
        TIM_TypeDef* tim = get_tim(DO);
        if(!tim)
            return;
        if(device.output[DO].tim->CCER & (TIM_CCER_CC1E << channel))
        {
            TIM_CCxChannelCmd(tim, channel, TIM_CCx_DISABLE);
            device.output[DO].gpio->ODR &= ~device.output[DO].pin;
        }else{
            TIM_CCxChannelCmd(tim, channel, TIM_CCx_ENABLE);
        }
    }
#else
    printf("%s\n", __FUNCTION__);
#endif
}

static void init_rectangle_output(uint8_t DO, uint32_t rate)
{
    if(!rate)
    {
        soft_timer_stop(DO);
        return;
    }
    uint32_t period = (10000 >> 1) / rate;
    if(!period)
        period = 1;
    soft_timer_init(period, &turn_output_state, false, DO);
    if(~TIM7->CR1 & TIM_CR1_CEN)
        TIM7->CR1 |= TIM_CR1_CEN;
}

static void init_pwm_timer(uint8_t DO, TIM_TypeDef* TIM, uint16_t rate, uint8_t duty)
{
    if(!rate)
        return;
    TIM->PSC = 4 - 1;
    uint32_t arr = SystemCoreClock / ((TIM->PSC + 1) * rate);
    while(arr > 0xFFFF)
    {
        if(TIM->PSC < 0xFFFF)
            TIM->PSC += 1;
        else
            break;
        arr = SystemCoreClock / ((TIM->PSC + 1) * rate);
    }
    TIM->ARR = (uint16_t)arr;

    volatile uint32_t channel = get_tim_channel(DO);
    volatile uint32_t* CCR = (__IO uint32_t *)(&(device.output[DO].tim->CCR1) + ((channel) >> 2U));
    *CCR = TIM->ARR * ((duty + 1) % 100) / 100;
    
    if(DO == 0){ // tim3 ch4
        TIM->CCMR2 |= TIM_CCMR2_OC4M_2 | TIM_CCMR2_OC4M_1 | TIM_CCMR2_OC4PE;
        TIM->CCER |= TIM_CCER_CC4E;
    }else if(DO == 1){ // tim3 ch3
        TIM->CCMR2 |= TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3PE;
        TIM->CCER |= TIM_CCER_CC3E;
    }else if(DO == 2){ // tim3 ch2
        TIM->CCMR1 |= TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2PE;
        TIM->CCER |= TIM_CCER_CC2E;
    }else if(DO == 3){ // tim3 ch1
        TIM->CCMR1 |= TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1PE;
        TIM->CCER |= TIM_CCER_CC1E;
    }else if(DO == 4){ // tim1 ch 4
        TIM1->CCMR2 |= TIM_CCMR2_OC4M_2 | TIM_CCMR2_OC4M_1 | TIM_CCMR2_OC4PE;
        TIM1->CCER |= TIM_CCER_CC4E;
    }else if(DO == 5){ // tim1 ch 3
        TIM1->CCMR2 |= TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3PE;
        TIM1->CCER |= TIM_CCER_CC3E;
    }else if(DO == 6){ // tim1 ch 2
        TIM1->CCMR1 |= TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2PE;
        TIM1->CCER |= TIM_CCER_CC2E;
    }else if(DO == 7){ // tim 1 ch 1
        TIM1->CCMR1 |= TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1PE;
        TIM1->CCER |= TIM_CCER_CC1E;
    }
    TIM->BDTR |= TIM_BDTR_MOE; /* (6) */
    TIM->CR1 |= TIM_CR1_CEN; /* (7) */
    TIM->EGR |= TIM_EGR_UG; /* (8) */
}

static int8_t init_pwm_output(uint8_t DO, uint16_t rate, uint16_t duty)
{
    if(!rate) // Hz
    {
        rate = 1;
    }
    if(!duty) // percent
    {
        duty = 50;
        device.output[DO].param_2 = duty;
    }
    if(DO < 4)
    {
#ifndef _UNITTEST_
        device.output[DO].tim = TIM3;
        device.output[DO].gpio = GPIOC;
        device.output[DO].pin = get_pin(DO); // PC9 .. PC6
        
        __HAL_RCC_GPIOC_CLK_ENABLE();
        GPIO_InitTypeDef GPIO_InitStruct = {0};
        GPIO_InitStruct.Pin = device.output[DO].pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_PULLDOWN;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF0_TIM3;
        HAL_GPIO_DeInit(device.output[DO].gpio, GPIO_InitStruct.Pin);
        HAL_GPIO_Init(device.output[DO].gpio, &GPIO_InitStruct);

        __HAL_RCC_TIM3_CLK_ENABLE();
        init_pwm_timer(DO, TIM3, rate, duty);
#endif
        return 0;
    }
    else if(DO < 8)
    {
#ifndef _UNITTEST_
        device.output[DO].tim = TIM1;
        device.output[DO].gpio = GPIOA;
        device.output[DO].pin = get_pin(DO); // PA12 .. PA8
        
        __HAL_RCC_GPIOA_CLK_ENABLE();
        GPIO_InitTypeDef GPIO_InitStruct = {0};
        GPIO_InitStruct.Pin = device.output[DO].pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_PULLDOWN;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF2_TIM1;
        HAL_GPIO_DeInit(device.output[DO].gpio, GPIO_InitStruct.Pin);
        HAL_GPIO_Init(device.output[DO].gpio, &GPIO_InitStruct);

        __HAL_RCC_TIM1_CLK_ENABLE();
        init_pwm_timer(DO, TIM1, rate, duty);
#endif
        return 0;
    }
    return REQUEST_ERR;
}

int8_t check_init_posibility(uint8_t DO, uint8_t mode)
{
    if(mode == HARD_PWM)
    {
        switch(DO)
        { // TIM3
            case 0:
                if((device.output[1].mode == HARD_PWM)
                || (device.output[2].mode == HARD_PWM)
                || (device.output[3].mode == HARD_PWM))
                    return -1;
                break;
            case 1:
                if((device.output[0].mode == HARD_PWM)
                || (device.output[2].mode == HARD_PWM)
                || (device.output[3].mode == HARD_PWM))
                    return -1;
                break;
            case 2:
                if((device.output[0].mode == HARD_PWM)
                || (device.output[1].mode == HARD_PWM)
                || (device.output[3].mode == HARD_PWM))
                    return -1;
                break;
            case 3:
                if((device.output[0].mode == HARD_PWM)
                || (device.output[1].mode == HARD_PWM)
                || (device.output[2].mode == HARD_PWM))
                    return -1;
                break;
            // TIM1
            case 4:
                if((device.output[5].mode == HARD_PWM)
                || (device.output[6].mode == HARD_PWM)
                || (device.output[7].mode == HARD_PWM))
                    return -1;
                break;
            case 5:
                if((device.output[4].mode == HARD_PWM)
                || (device.output[6].mode == HARD_PWM)
                || (device.output[7].mode == HARD_PWM))
                    return -1;
                break;
            case 6:
                if((device.output[4].mode == HARD_PWM)
                || (device.output[5].mode == HARD_PWM)
                || (device.output[7].mode == HARD_PWM))
                    return -1;
                break;
            case 7:
                if((device.output[4].mode == HARD_PWM)
                || (device.output[5].mode == HARD_PWM)
                || (device.output[6].mode == HARD_PWM))
                    return -1;
                break;
            default:
                return -1;
        }
    }
    return 0;
}

static int8_t validate_configure(uint8_t DO, uint8_t mode)
{
#ifdef _UNITTEST_
    printf("%s\n", __FUNCTION__);
#endif
    if((DO > MBD_NUM_OUTPUTS - 1) || (mode > HARD_PWM))
        return -1;
    device.output[DO].new_mode = mode;
    return 0;
}

// channel = DO
static int8_t output_configure(uint8_t DO)
{
    int8_t ret = 0;
#ifdef _UNITTEST_
    printf("%s\n", __FUNCTION__);
#endif
    volatile uint8_t mode = device.output[DO].mode;
    if(device.output[DO].new_mode != -1)
    {
        mode = device.output[DO].new_mode;
    }
    switch(mode)
    {
        case INACTIVE:
            clear_output(DO); // clear old data
            deinit_output(DO);
            break;
        case DISCRETE_OUTPUT:
            init_discrete_output(DO);
            break;
        case RECTANGLE:
            init_discrete_output(DO);
            init_rectangle_output(DO, device.output[DO].param_1);
            break;
        case HARD_PWM:
            if(check_init_posibility(DO, mode))
            {
                ++device.err_param_rq;
                ret = REQUEST_ERR;
                break;
            }
            if(init_pwm_output(DO, device.output[DO].param_1, device.output[DO].param_2))
            {
                ret = REQUEST_ERR;
                break;
            }
            init_rectangle_output(DO, device.output[DO].param_3);
            break;
        default:
            break;
    }
    if(!ret)
    {
        device.output[DO].mode = mode;
    }
    device.output[DO].new_mode = -1;
    return ret;
}

static int8_t set_do_output_value(uint8_t DO, uint8_t value)
{
    if(device.output[DO].mode == INACTIVE)
        return -1;

    if(value)
        device.output[DO].gpio->ODR |= device.output[DO].pin;
    else
        device.output[DO].gpio->ODR &= ~device.output[DO].pin;
    return 0;
}

void mbd_exit_init_mode(void)
{
    device.is_init = false;
    INIT_MODE_LED_PORT->ODR &= ~INIT_MODE_LED_PIN;
    mbd_rs485_init(device.baudrate);
}

static void set_spare_output(uint16_t value)
{
    if(value)
    {
        device.is_init = false;
        mbd_exit_init_mode();
        SPARE_INIT_PORT->ODR |= SPARE_INIT_PIN;
    }else{
        SPARE_INIT_PORT->ODR &= ~SPARE_INIT_PIN;
    }
}

// oooo     oooo oooooooooo  ooooo ooooooooooo ooooooooooo      oooooooooo   o      oooooooooo       o      oooo     oooo
//  88   88  88   888    888  888  88  888  88  888    88        888    888 888      888    888     888      8888o   888
//   88 888 88    888oooo88   888      888      888ooo8          888oooo88 8  88     888oooo88     8  88     88 888o8 88
//    888 888     888  88o    888      888      888    oo        888      8oooo88    888  88o     8oooo88    88  888  88
//     8   8     o888o  88o8 o888o    o888o    o888ooo8888      o888o   o88o  o888o o888o  88o8 o88o  o888o o88o  8  o88o

int8_t mbd_write_reg(uint16_t reg, uint16_t value)
{
    int8_t ret = 0;
    uint16_t update_ext_mem = 0;
    switch(reg)
    {
        // маски
        case AND_MASK_REGISTER:
            device.and_mask = value;
            update_ext_mem |= (1 << 8);
            break;
        case OR_MASK_REGISTER:
            device.or_mask = value;
            update_ext_mem |= (1 << 8);
            break;
        // параметры входов
        case DO0_MODE_REG:
            ret = validate_configure(0, value);
            if(!ret)
                update_ext_mem |= (1 << 0);
            else
                return ret;
            break;
        case PARAM1_REG_0:
            device.output[0].param_1 = value;
            update_ext_mem |= (1 << 0);
            break;
        case PARAM2_REG_0:
            device.output[0].param_2 = value;
            update_ext_mem |= (1 << 0);
            break;
        case PARAM3_REG_0:
            device.output[0].param_3 = value;
            update_ext_mem |= (1 << 0);
            break;
        
        case DO1_MODE_REG:
            ret = validate_configure(1, value);
            if(!ret)
                update_ext_mem |= (1 << 1);
            else
                return ret;
            break;
        case PARAM1_REG_1:
            device.output[1].param_1 = value;
            update_ext_mem |= (1 << 1);
            break;
        case PARAM2_REG_1:
            device.output[1].param_2 = value;
            update_ext_mem |= (1 << 1);
            break;
        case PARAM3_REG_1:
            device.output[1].param_3 = value;
            update_ext_mem |= (1 << 1);
            break;
        
        case DO2_MODE_REG:
            ret = validate_configure(2, value);
            if(!ret)
                update_ext_mem |= (1 << 2);
            else
                return ret;
            break;
        case PARAM1_REG_2:
            device.output[2].param_1 = value;
            update_ext_mem |= (1 << 2);
            break;
        case PARAM2_REG_2:
            device.output[2].param_2 = value;
            update_ext_mem |= (1 << 2);
            break;
        case PARAM3_REG_2:
            device.output[2].param_3 = value;
            update_ext_mem |= (1 << 2);
            break;
        
        case DO3_MODE_REG:
            ret = validate_configure(3, value);
            if(!ret)
                update_ext_mem |= (1 << 3);
            else
                return ret;
            break;
        case PARAM1_REG_3:
            device.output[3].param_1 = value;
            update_ext_mem |= (1 << 3);
            break;
        case PARAM2_REG_3:
            device.output[3].param_2 = value;
            update_ext_mem |= (1 << 3);
            break;
        case PARAM3_REG_3:
            device.output[3].param_3 = value;
            update_ext_mem |= (1 << 3);
            break;
        
        case DO4_MODE_REG:
            ret = validate_configure(4, value);
            if(!ret)
                update_ext_mem |= (1 << 4);
            else
                return ret;
            break;
        case PARAM1_REG_4:
            device.output[4].param_1 = value;
            update_ext_mem |= (1 << 4);
            break;
        case PARAM2_REG_4:
            device.output[4].param_2 = value;
            update_ext_mem |= (1 << 4);
            break;
        case PARAM3_REG_4:
            device.output[4].param_3 = value;
            update_ext_mem |= (1 << 4);
            break;
        
        case DO5_MODE_REG:
            ret = validate_configure(5, value);
            if(!ret)
                update_ext_mem |= (1 << 5);
            else
                return ret;
            break;
        case PARAM1_REG_5:
            device.output[5].param_1 = value;
            update_ext_mem |= (1 << 5);
            break;
        case PARAM2_REG_5:
            device.output[5].param_2 = value;
            update_ext_mem |= (1 << 5);
            break;
        case PARAM3_REG_5:
            device.output[5].param_3 = value;
            update_ext_mem |= (1 << 5);
            break;
        
        case DO6_MODE_REG:
            ret = validate_configure(6, value);
            if(!ret)
                update_ext_mem |= (1 << 6);
            else
                return ret;
            break;
        case PARAM1_REG_6:
            device.output[6].param_1 = value;
            update_ext_mem |= (1 << 6);
            break;
        case PARAM2_REG_6:
            device.output[6].param_2 = value;
            update_ext_mem |= (1 << 6);
            break;
        case PARAM3_REG_6:
            device.output[6].param_3 = value;
            update_ext_mem |= (1 << 6);
            break;
        
        case DO7_MODE_REG:
            ret = validate_configure(7, value);
            if(!ret)
                update_ext_mem |= (1 << 7);
            else
                return ret;
            break;
        case PARAM1_REG_7:
            device.output[7].param_1 = value;
            update_ext_mem |= (1 << 7);
            break;
        case PARAM2_REG_7:
            device.output[7].param_2 = value;
            update_ext_mem |= (1 << 7);
            break;
        case PARAM3_REG_7:
            device.output[7].param_3 = value;
            update_ext_mem |= (1 << 7);
            break;

        case DO0_VAL:
            return set_do_output_value(0, value);
        case DO1_VAL:
            return set_do_output_value(1, value);
        case DO2_VAL:
            return set_do_output_value(2, value);
        case DO3_VAL:
            return set_do_output_value(3, value);
        case DO4_VAL:
            return set_do_output_value(4, value);
        case DO5_VAL:
            return set_do_output_value(5, value);
        case DO6_VAL:
            return set_do_output_value(6, value);
        case DO7_VAL:
            return set_do_output_value(7, value);

        case DRV_ENABLE:
            if(value)
                DRV_ENABLE_PORT->ODR &= ~DRV_ENABLE_PIN; // INVERSED
            else
                DRV_ENABLE_PORT->ODR |= DRV_ENABLE_PIN;
            return 0;
        case ADUM_ENABLE:
            if(value)
                ADUM_ENABLE_PORT->ODR |= ADUM_ENABLE_PIN;
            else
                ADUM_ENABLE_PORT->ODR &= ~ADUM_ENABLE_PIN;
            return 0;

        case SPARE_INIT:
            set_spare_output(value);
            return 0;
        case RS485_ADDRESS_REG:
            if(device.is_init)
            {
                device.addr = value;
                update_ext_mem |= (1 << 8);
            }
            break;
        case RS485_BAUDRATE_HREG:
            if(device.is_init)
            {
                device.baudrate &= ~(0xFFFF << 16);
                device.baudrate |= value << 16;
            // update_ext_mem |= (1 << 8); // do not update
            }
            return 0;
        case RS485_BAUDRATE_LREG:
            if(device.is_init)
            {
                device.baudrate &= ~0xFFFF;
                device.baudrate |= value;
                update_ext_mem |= (1 << 8);
            }
            break;
        default:
            return DATA_ADDR_ERR;
    }
    if(update_ext_mem)
    {
        for(uint8_t i = 0; i < 8; ++i)
        {
            if(update_ext_mem & (1 << i))
            {
                ret = output_configure(i);
                if(ret)
                    return ret;
                break;
            }
        }
        int8_t ret = ext_mem_store();
        if(ret)
        {
            ERROR(EXT_MEM_WRITE_ERROR);
            return FATAL_ERR;
        }
    }
    return 0;
}

void mbd_inc_crc_err_rq(void)
{
    ++device.err_crc_rq;
}

void mbd_inc_handled_rq(void)
{
    ++device.handled_rq;
}

void mbd_inc_total_rq(void)
{
    ++device.total_rq;
}

void mdb_inc_send_err(void)
{
    ++device.err_send;
}

void process_rect(void)
{
}

void process_pwm_output(uint8_t DO)
{
    (void)DO;
}

void mbd_process(void)
{
    for(uint8_t DO = 0; DO < MBD_NUM_OUTPUTS; ++DO)
    {
        switch(device.output[DO].mode)
        {
            case INACTIVE:
                break;
            case DISCRETE_OUTPUT:
                update_output();
                break;
            case RECTANGLE:
                process_rect();
                break;
            case HARD_PWM:
                process_pwm_output(DO);
                break;
            default:
                break;
        }
    }
}

void drv_adum_enable_output_config(void)
{
#ifndef _UNITTEST_
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;

    GPIO_InitStruct.Pin = ADUM_ENABLE_PIN;
    HAL_GPIO_Init(ADUM_ENABLE_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = DRV_ENABLE_PIN;
    HAL_GPIO_Init(DRV_ENABLE_PORT, &GPIO_InitStruct);

    ADUM_ENABLE_PORT->ODR |= ADUM_ENABLE_PIN;
    DRV_ENABLE_PORT->ODR &= ~DRV_ENABLE_PIN;
#endif
}

extern UART_HandleTypeDef huart4;

int8_t mbd_rs485_init(uint32_t baudrate)
{
    if(!baudrate)
        return -1;
    HAL_NVIC_DisableIRQ(USART3_8_IRQn);
    HAL_UART_DeInit(&huart4);
    huart4.Init.BaudRate = baudrate;
    huart4.Init.WordLength = UART_WORDLENGTH_8B;
    huart4.Init.StopBits = UART_STOPBITS_1;
    huart4.Init.Parity = UART_PARITY_NONE;
    huart4.Init.Mode = UART_MODE_TX_RX;
    huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart4.Init.OverSampling = UART_OVERSAMPLING_16;
    huart4.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
    huart4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
    HAL_RS485Ex_Init(&huart4, UART_DE_POLARITY_HIGH, 31, 31);
    if(HAL_UART_Init(&huart4) != HAL_OK)
    {
        return -1;
    }
    HAL_NVIC_EnableIRQ(USART3_8_IRQn);
    dma_usart_idle_reinit();
    return 0;
}

void mbd_init(void)
{
    update_output();
    device.and_mask = 0xFFFF;
    device.is_init = false;
    device.baudrate = huart4.Init.BaudRate;
    if(ext_mem_load())
    {
        ERROR(CONFIG_LOAD_ERROR);
        output_configure(0);
        output_configure(1);
        output_configure(2);
        output_configure(3);
        output_configure(4);
        output_configure(5);
        output_configure(6);
        output_configure(7);
    }
    mbd_rs485_init(device.baudrate);
    drv_adum_enable_output_config();
    timer_7_init();
}

void mbd_output_params(output_params_t* output, uint8_t output_num)
{
    // кастить нельзя потому что output packed
    output->mode = device.output[output_num].mode;
    output->param_1 = device.output[output_num].param_1;
    output->param_2 = device.output[output_num].param_2;
    output->param_3 = device.output[output_num].param_3;
}

void mbd_output_load(output_params_t* output, uint8_t output_num)
{
    device.output[output_num].new_mode = output->mode;
    device.output[output_num].param_1 = output->param_1;
    device.output[output_num].param_2 = output->param_2;
    device.output[output_num].param_3 = output->param_3;

    output_configure(output_num);
}

void mbd_common_params(common_params_t* cp)
{
    cp->addr            = device.addr;
    cp->module_id       = MODULE_ID;
    cp->operation_time  = device.operation_time;
    cp->program_crc     = device.program_crc;
    cp->total_rq        = device.total_rq;   // счетчик запросов
    cp->handled_rq      = device.handled_rq; // счетчик обработанных запросов
    cp->err_crc_rq      = device.err_crc_rq; // счетчик запросов с ошибкой CRC
    cp->err_param_rq    = device.err_param_rq; // счетчик запросов с некорректными параметрами
    cp->err_send        = device.err_send;  // счетчик ошибок при отправке
    cp->and_mask        = device.and_mask;
    cp->or_mask         = device.or_mask;
    cp->change_time     = device.operation_time;
    cp->baudrate        = device.baudrate;
}

void mbd_log_status(void)
{
    log_out("start_time:\t %lu \n",   device.start_time);
    log_out("program_crc:\t %u \n",   device.program_crc);
    log_out("total_rq:\t %lu \n",     device.total_rq);
    log_out("handled_rq:\t %lu \n",   device.handled_rq);
    log_out("err_crc_rq:\t %lu \n",   device.err_crc_rq);
    log_out("err_param_rq:\t %lu \n", device.err_param_rq);
    log_out("err_send:\t %lu \n",     device.err_send);
    log_out("and_mask:\t 0x%x \n",      device.and_mask);
    log_out("or_mask:\t 0x%x \n",       device.or_mask);
    log_out("change_time:\t %lu\n", device.change_time);
}

void mbd_set_common_params(common_params_t* cp)
{
    if(cp->addr)
        device.addr     = cp->addr;
    device.start_time   = cp->operation_time;
    device.program_crc  = cp->program_crc;
    device.total_rq     = cp->total_rq;   // счетчик запросов
    device.handled_rq   = cp->handled_rq; // счетчик обработанных запросов
    device.err_crc_rq   = cp->err_crc_rq; // счетчик запросов с ошибкой CRC
    device.err_param_rq = cp->err_param_rq; // счетчик запросов с некорректными параметрами
    device.err_send     = cp->err_send;  // счетчик ошибок при отправке
    device.and_mask     = cp->and_mask;
    device.or_mask      = cp->or_mask;
    device.change_time  = cp->change_time;
    device.baudrate     = cp->baudrate;
}

uint16_t mbd_module_id(void)
{
    return MODULE_ID;
}

uint16_t mbd_update_status(void)
{
    return device.update;
}

void mbd_log_config(void)
{
    log_out("---------------- DO config ----------------\n");
    log_out("DO baudrate: %lu\n", huart4.Init.BaudRate);
    log_out("DO0: mode: %lu\tparam1: %u\tparam2: %u\tparam3: %u\n", device.output[0].mode, device.output[0].param_1, device.output[0].param_2, device.output[0].param_3);
    log_out("DO1: mode: %lu\tparam1: %u\tparam2: %u\tparam3: %u\n", device.output[1].mode, device.output[1].param_1, device.output[1].param_2, device.output[1].param_3);
    log_out("DO2: mode: %lu\tparam1: %u\tparam2: %u\tparam3: %u\n", device.output[2].mode, device.output[2].param_1, device.output[2].param_2, device.output[2].param_3);
    log_out("DO3: mode: %lu\tparam1: %u\tparam2: %u\tparam3: %u\n", device.output[3].mode, device.output[3].param_1, device.output[3].param_2, device.output[3].param_3);
    log_out("DO4: mode: %lu\tparam1: %u\tparam2: %u\tparam3: %u\n", device.output[4].mode, device.output[4].param_1, device.output[4].param_2, device.output[4].param_3);
    log_out("DO5: mode: %lu\tparam1: %u\tparam2: %u\tparam3: %u\n", device.output[5].mode, device.output[5].param_1, device.output[5].param_2, device.output[5].param_3);
    log_out("DO6: mode: %lu\tparam1: %u\tparam2: %u\tparam3: %u\n", device.output[6].mode, device.output[6].param_1, device.output[6].param_2, device.output[6].param_3);
    log_out("DO7: mode: %lu\tparam1: %u\tparam2: %u\tparam3: %u\n", device.output[7].mode, device.output[7].param_1, device.output[7].param_2, device.output[7].param_3);
}

void mbd_set_baudrate(uint32_t baudrate)
{
    device.baudrate = baudrate;
    mbd_rs485_init(device.baudrate);
}

#define IS_EOF_INIT() (SPARE_INIT_PORT->ODR & SPARE_INIT_PIN)

bool mbd_can_init(void)
{
    if(device.is_init)
        return false;
    if(IS_EOF_INIT())
        return false;
    return true;
}

bool mbd_is_init(void)
{
    return device.is_init;
}

void mbd_int_init(void)
{
    device.is_init = true;
    INIT_MODE_LED_PORT->ODR |= INIT_MODE_LED_PIN;
    mbd_rs485_init(INT_INIT_BAUDRATE);
}

void mbd_ext_init(void)
{
    device.is_init = true;
    INIT_MODE_LED_PORT->ODR |= INIT_MODE_LED_PIN;
    mbd_rs485_init(EXT_INIT_BAUDRATE);
}

uint8_t mbd_addr(void)
{
    if(!device.addr)
        return 1;
    return device.addr;
}
