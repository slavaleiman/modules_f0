
// The MODBUS protocol defines three PDUs. They are :
// - MODBUS Request PDU,            mb_req_pdu
// - MODBUS Response PDU,           mb_rsp_pdu
// - MODBUS Exception Response PDU, mb_excep_rsp_pdu

// адрес устройства         1 byte
// номер функции            1 byte
// Адрес регистра           2 byte
// Количество флагов        2 byte
// Количество byte данных   1 byte
// Данные                   ...
// CRC                      2 byte

#include "modbus.h"
#include "modbus_device.h"

#include "stm32f0xx_hal_uart.h"
#include "string.h"
#include "dma_usart_idle.h"
#include "udelay.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "CRC.h"
#include "errors.h"

#define DEVICE_DEFAULT_ADDR     1
#define TXBUFFERSIZE            255

extern UART_HandleTypeDef huart4;
extern bool is_usart4_sending;
#define HUART &huart4

#define UART_TRANSMIT(BUF, LEN)  \
	is_usart4_sending = true;\
	HAL_UART_Transmit_DMA(HUART, BUF, LEN);\

struct{
    uint8_t     tx_buffer[TXBUFFERSIZE];
    SemaphoreHandle_t holding_mutex;
    SemaphoreHandle_t output_mutex;
}modbus;

static void modbus_response_error(uint8_t func_num, uint8_t error_code)
{
    modbus.tx_buffer[0] = mbd_addr();
    modbus.tx_buffer[1] = func_num | 0x80;
    modbus.tx_buffer[2] = error_code;
    const uint16_t crc = CRC16(&modbus.tx_buffer[0], 3);
    modbus.tx_buffer[3] = crc & 0xFF;
    modbus.tx_buffer[4] = crc >> 8;
    UART_TRANSMIT(modbus.tx_buffer, 5);
}

static void modbus_response_echo(uint8_t* message)
{
    uint16_t _crc = message[7] << 8 | message[6];
    modbus.tx_buffer[0] = mbd_addr();
    modbus.tx_buffer[1] = 0x8;
    modbus.tx_buffer[2] = 0;
    modbus.tx_buffer[3] = 0;
    modbus.tx_buffer[4] = message[4];
    modbus.tx_buffer[5] = message[5];
    const uint16_t calc_crc = CRC16(&modbus.tx_buffer[0], 6);
    if(calc_crc != _crc)
    {
        mbd_inc_crc_err_rq();
        return;
    }
    modbus.tx_buffer[6] = calc_crc & 0xFF;
    modbus.tx_buffer[7] = calc_crc >> 8;
    UART_TRANSMIT(modbus.tx_buffer, 8);
}

static int8_t get_holding_reg(uint8_t* message)
{
    const uint16_t start_addr = message[2] << 8 | message[3];
    const uint16_t req_regs_num = message[4] << 8 | message[5];
    uint16_t _crc = message[6] | message[7] << 8;
    uint16_t calc_crc = CRC16(&message[0], 6);
    if(calc_crc != _crc)
    {
        mbd_inc_crc_err_rq();
        return 0;
    }
    modbus.tx_buffer[0] = mbd_addr();
    modbus.tx_buffer[1] = 3;

    uint8_t count_reg = 0;
    int8_t ret = 0;
    if(modbus.holding_mutex != NULL)
    {
        if(xSemaphoreTake(modbus.holding_mutex, (TickType_t)10) == pdTRUE) 
        {
            while(count_reg < req_regs_num)
            {
                uint16_t reg_value;
                ret = mbd_holding_reg(start_addr + count_reg, &reg_value);
                if(ret < 0)
                    break;
                modbus.tx_buffer[3 + count_reg * 2] = reg_value >> 8;
                modbus.tx_buffer[3 + count_reg * 2 + 1] = reg_value & 0xFF;
                ++count_reg;
            }
            xSemaphoreGive(modbus.holding_mutex);
        }else{
            ERROR(MODBUS_HOLDING_REG_LOCKED);
        }
    }
    if(!count_reg)
        return ret;

    modbus.tx_buffer[2] = count_reg * 2;
    const uint16_t crc = CRC16(&modbus.tx_buffer[0], count_reg * 2 + 3);
    modbus.tx_buffer[3 + count_reg * 2] = crc & 0xFF;
    modbus.tx_buffer[3 + count_reg * 2 + 1] = crc >> 8;
    UART_TRANSMIT(modbus.tx_buffer, count_reg * 2 + 5);
    return 0;
}

// Read Holding Registers
void modbus_f3(uint8_t* message)
{
    uint8_t errcode = get_holding_reg(message);
    if(errcode)
        modbus_response_error(message[1], errcode);
    else
        mbd_inc_handled_rq();
}

static int8_t write_single_reg(uint8_t* message)
{
    const uint16_t start_addr = message[2] << 8 | message[3];
    const uint16_t write_value = message[4] << 8 | message[5];
    uint16_t _crc = message[6] | message[7] << 8;
    uint16_t calc_crc = CRC16(&message[0], 6);
    if(calc_crc != _crc)
    {
        mbd_inc_crc_err_rq();
        return 0;
    }
    int8_t ret = -1;
    if(modbus.holding_mutex != NULL)
    {
        if(xSemaphoreTake(modbus.holding_mutex, (TickType_t)10) == pdTRUE) 
        {
            ret = mbd_write_reg(start_addr, write_value);
            xSemaphoreGive(modbus.holding_mutex);
        }else{
            ERROR(MODBUS_HOLDING_REG_LOCKED);
        }
    }
    if(ret)
        return ret;
    modbus.tx_buffer[0] = mbd_addr();
    modbus.tx_buffer[1] = 6;
    modbus.tx_buffer[2] = message[2];
    modbus.tx_buffer[3] = message[3];
    modbus.tx_buffer[4] = message[4];
    modbus.tx_buffer[5] = message[5];
    modbus.tx_buffer[6] = message[6];
    modbus.tx_buffer[7] = message[7];

    UART_TRANSMIT(modbus.tx_buffer, 8);
    return 0;
}

void modbus_f6(uint8_t* message)
{
    int8_t errcode = write_single_reg(message);
    if(errcode)
        modbus_response_error(message[1], errcode);
    else
        mbd_inc_handled_rq();
}

static int8_t write_multiple_reg(uint8_t* message, uint16_t size)
{
    const uint16_t start_addr = message[2] << 8 | message[3];
    const uint16_t regs_to_write = message[4] << 8 | message[5];
    const uint8_t bytes_to_write = message[6];

    uint16_t _crc = message[7 + bytes_to_write] | message[8 + bytes_to_write] << 8;
    uint16_t calc_crc = CRC16(&message[0], 7 + bytes_to_write);
    if(calc_crc != _crc)
    {
        mbd_inc_crc_err_rq();
        return 0;
    }

    modbus.tx_buffer[0] = mbd_addr();
    modbus.tx_buffer[1] = 16;

    uint8_t count_reg = 0;
    if(modbus.holding_mutex != NULL)
    {
        if(xSemaphoreTake(modbus.holding_mutex, (TickType_t)10) == pdTRUE) 
        {
            while(count_reg < regs_to_write)
            {
                const uint8_t index = 7 + count_reg * 2;
                if(index + 1 > size - 2)
                {
                    break;
                }
                uint16_t reg_value = message[index] << 8 | message[index + 1];
                int8_t ret = mbd_write_reg(start_addr + count_reg * 2, reg_value);
                if(ret)
                    break;
                // если записался хоть один регистр - то
                // при ошибке записи в следующий, просто выходим
                // и отправляем количество успешно записанных регистров
                ++count_reg;
            }
            xSemaphoreGive(modbus.holding_mutex);
        }else{
            ERROR(MODBUS_HOLDING_REG_LOCKED);
        }
    }    

    if(!count_reg) // если не записалось ниодного - ошибка в запросе
        return REQUEST_ERR;
    modbus.tx_buffer[2] = message[2];
    modbus.tx_buffer[3] = message[3];
    modbus.tx_buffer[4] = count_reg >> 8;
    modbus.tx_buffer[5] = count_reg & 0xFF;

    const uint16_t crc = CRC16(&modbus.tx_buffer[0], 6);
    modbus.tx_buffer[6] = crc & 0xFF;
    modbus.tx_buffer[7] = crc >> 8;
    UART_TRANSMIT(modbus.tx_buffer, 8);
    return 0;
}

void modbus_f16(uint8_t* message, uint8_t size)
{
    int8_t errcode = write_multiple_reg(message, size);
    if(errcode)
        modbus_response_error(message[1], errcode);
    else
        mbd_inc_handled_rq();
}

void modbus_on_rtu(uint8_t* message, uint16_t size)
{
    const uint8_t addr = message[0];
    mbd_inc_total_rq();

    if(size < 8)
        return;

    if(mbd_is_init())
    {
        if(addr != MBD_INIT_ADDR)
            return;
    }else{
        if(addr != mbd_addr())
            return;
    }

    const uint8_t func_num = message[1];
    switch(func_num)
    {
        case 3: // чтение значений из нескольких регистров хранения (Read Holding Registers).
            modbus_f3(message);
            break;
        case 6: // запись значения в один регистр хранения
            modbus_f6(message);
            break;
        case 8: // Диагностика
            modbus_response_echo(message);
            break;
        case 16: // запись значений в несколько регистров хранения (Preset Multiple Registers)
            modbus_f16(message, size);
            break;
        default:
            modbus_response_error(func_num, FUNC_CODE_ERR);
    }
}

void modbus_check_trasmition(uint8_t *buffer, size_t len)
{
    for(uint8_t i = 0; i < len; ++i)
    {
        if(buffer[i] ^ modbus.tx_buffer[i])
        {
            mdb_inc_send_err();
            return;
        }
    }
}

void modbus_init(void)
{
    if(!modbus.output_mutex)
        modbus.output_mutex = xSemaphoreCreateMutex();
    if(!modbus.holding_mutex)
        modbus.holding_mutex = xSemaphoreCreateMutex();
    mbd_init();
}
