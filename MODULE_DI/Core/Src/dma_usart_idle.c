/*
    To enable circular buffer, you have to enable IDLE LINE DETECTION interrupt

    __HAL_UART_ENABLE_IT (UART_HandleTypeDef *huart, UART_IT_IDLE);   // enable idle line interrupt
    __HAL_DMA_ENABLE_IT (DMA_HandleTypeDef *hdma, DMA_IT_TC);  // enable DMA Tx cplt interrupt
    also enable RECEIVE DMA
    HAL_UART_Receive_DMA (UART_HandleTypeDef *huart, DMA_RX_Buffer, 64);
    IF you want to transmit the received data uncomment lines
*/

#include "dma_usart_idle.h"
#include "string.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "cmsis_os.h"
#include "modbus.h"
#include "udelay.h"
#include "user_cmd.h"
#include "errors.h"
#include <stdbool.h>

extern UART_HandleTypeDef huart4;
extern DMA_HandleTypeDef hdma_usart4_rx;
extern QueueHandle_t RxQueue;
extern osSemaphoreId Usart4RxSemaphoreHandle;

extern UART_HandleTypeDef huart2;
extern DMA_HandleTypeDef hdma_usart2_rx;

extern bool is_usart4_sending; 

uint8_t DMA_RX_Buffer_1[DMA_RX_BUFFER_SIZE];
uint8_t DMA_RX_Buffer_2[DMA_RX_BUFFER_SIZE];
uint8_t UART_Buffer[DMA_RX_BUFFER_SIZE];
int __irq_status = 1;

void USART4_IrqHandler(UART_HandleTypeDef *huart, DMA_HandleTypeDef *hdma)
{
    if(huart->Instance->ISR & UART_FLAG_IDLE)
    {
        huart->Instance->ICR |= UART_CLEAR_IDLEF;
        size_t len = DMA_RX_BUFFER_SIZE - hdma->Instance->CNDTR;
        if(len)
        {
            if(is_usart4_sending) // отправленное сообщение
            {
                is_usart4_sending = false;

                hdma->Instance->CCR &= ~DMA_CCR_EN;
                modbus_check_trasmition(DMA_RX_Buffer_1, len);
                memset(DMA_RX_Buffer_1, 0, len);
                hdma->Instance->CMAR = (uint32_t)DMA_RX_Buffer_1; /* Set memory address for DMA again */
                hdma->Instance->CNDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
                hdma->Instance->CCR |= DMA_CCR_EN;              /* Start DMA transfer */
            }
            else
            {
                hdma->Instance->CCR &= ~DMA_CCR_EN;
                memset(&UART_Buffer[0], 0, len);
                memcpy(&UART_Buffer[0], DMA_RX_Buffer_1, len);
                memset(DMA_RX_Buffer_1, 0, len);
                message_t mess = (message_t){&UART_Buffer[0], len};

                hdma->Instance->CMAR = (uint32_t)DMA_RX_Buffer_1; /* Set memory address for DMA again */
                hdma->Instance->CNDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
                hdma->Instance->CCR |= DMA_CCR_EN;              /* Start DMA transfer */
                
                BaseType_t xHigherPriorityTaskWoken = pdFALSE;
                xQueueSendToBackFromISR(RxQueue, &mess, &xHigherPriorityTaskWoken);
                xHigherPriorityTaskWoken = pdFALSE;
                xSemaphoreGiveFromISR(Usart4RxSemaphoreHandle, &xHigherPriorityTaskWoken);
            }
        }
    }
    if(huart->Instance->ISR & USART_ISR_ORE)
    {
        huart->Instance->ICR |= USART_ICR_ORECF;
        (void)huart->Instance->RDR;
        huart->Instance->CR1 &= ~USART_CR1_UE;
        huart->Instance->CR3 |= USART_CR3_EIE | USART_CR3_DMAR;
        huart->Instance->CR1 |= USART_CR1_UE;
        // reinit_dma
        memset(DMA_RX_Buffer_1, 0, DMA_RX_BUFFER_SIZE);
        hdma->Instance->CCR &= ~DMA_CCR_EN;
        hdma->Instance->CMAR = (uint32_t)DMA_RX_Buffer_1; /* Set memory address for DMA again */
        hdma->Instance->CNDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
        hdma->Instance->CCR |= DMA_CCR_TCIE | DMA_CCR_TEIE;
        hdma->Instance->CCR |= DMA_CCR_EN;              /* Start DMA transfer */

        errors_on_error(RS485_OVERRUN_ERROR); // without led indicate
    }
}

void USART2_IrqHandler(UART_HandleTypeDef *huart, DMA_HandleTypeDef *hdma)
{
    if(huart->Instance->ISR & UART_FLAG_IDLE)
    {
        huart->Instance->ICR |= UART_CLEAR_IDLEF;
        size_t len = DMA_RX_BUFFER_SIZE - hdma->Instance->CNDTR;
        if(len)
        {
            hdma->Instance->CCR &= ~DMA_CCR_EN;
            user_cmd(&DMA_RX_Buffer_2[0]);
            hdma->Instance->CMAR = (uint32_t)DMA_RX_Buffer_2; /* Set memory address for DMA again */
            hdma->Instance->CNDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
            hdma->Instance->CCR |= DMA_CCR_EN;              /* Start DMA transfer */
            memset(DMA_RX_Buffer_2, 0, len);
        }
    }
    if(huart->Instance->ISR & USART_ISR_ORE)
    {
        huart->Instance->ICR |= USART_ICR_ORECF;
        (void)huart->Instance->RDR;
        huart->Instance->CR1 &= ~USART_CR1_UE;
        huart->Instance->CR3 |= USART_CR3_EIE | USART_CR3_DMAR;
        huart->Instance->CR1 |= USART_CR1_UE;
        // reinit_dma
        memset(DMA_RX_Buffer_2, 0, DMA_RX_BUFFER_SIZE);
        hdma->Instance->CCR &= ~DMA_CCR_EN;
        hdma->Instance->CMAR = (uint32_t)DMA_RX_Buffer_2; /* Set memory address for DMA again */
        hdma->Instance->CNDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
        hdma->Instance->CCR |= DMA_CCR_TCIE | DMA_CCR_TEIE;
        hdma->Instance->CCR |= DMA_CCR_EN;              /* Start DMA transfer */

        errors_on_error(RS485_OVERRUN_ERROR); // without led indicate
    }    
}

void dma_usart_idle_init(void)
{
// USART4
    __HAL_RCC_GPIOA_CLK_ENABLE();
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Pin = GPIO_PIN_15;
    GPIO_InitStruct.Alternate = GPIO_AF4_USART4;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    is_usart4_sending = false;
    // USART4
    __HAL_UART_ENABLE_IT(&huart4, UART_IT_IDLE);   // enable idle line interrupt
    HAL_UART_Receive_DMA(&huart4, DMA_RX_Buffer_1, DMA_RX_BUFFER_SIZE);
    // USART2
    __HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);   // enable idle line interrupt
    HAL_UART_Receive_DMA(&huart2, DMA_RX_Buffer_2, DMA_RX_BUFFER_SIZE);
}

void dma_usart_idle_reinit(void)
{
    USART4->ICR |= UART_CLEAR_IDLEF;
    is_usart4_sending = false;
    memset(DMA_RX_Buffer_1, 0, DMA_RX_BUFFER_SIZE);
    __HAL_UART_ENABLE_IT(&huart4, UART_IT_IDLE);   // enable idle line interrupt
    HAL_UART_Receive_DMA(&huart4, DMA_RX_Buffer_1, DMA_RX_BUFFER_SIZE);
}
