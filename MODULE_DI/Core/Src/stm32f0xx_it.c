/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f0xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_it.h"
#include "cmsis_os.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "dma_usart_idle.h"
#include "modbus_device.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern DMA_HandleTypeDef hdma_usart2_rx;
extern DMA_HandleTypeDef hdma_usart2_tx;
extern DMA_HandleTypeDef hdma_usart4_rx;
extern DMA_HandleTypeDef hdma_usart4_tx;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart4;
extern TIM_HandleTypeDef htim6;

/* USER CODE BEGIN EV */
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M0 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/******************************************************************************/
/* STM32F0xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f0xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles DMA1 channel 1 interrupt.
  */
void DMA1_Ch1_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Ch1_IRQn 0 */

  /* USER CODE END DMA1_Ch1_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart4_rx);
  /* USER CODE BEGIN DMA1_Ch1_IRQn 1 */
  /* USER CODE END DMA1_Ch1_IRQn 1 */
}

/**
  * @brief This function handles DMA1 channel 2 to 3 and DMA2 channel 1 to 2 interrupts.
  */
void DMA1_Ch2_3_DMA2_Ch1_2_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Ch2_3_DMA2_Ch1_2_IRQn 0 */

  /* USER CODE END DMA1_Ch2_3_DMA2_Ch1_2_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart4_tx);
  /* USER CODE BEGIN DMA1_Ch2_3_DMA2_Ch1_2_IRQn 1 */
  if(DMA1->ISR & (DMA_FLAG_TC2 | DMA_FLAG_TE2 | DMA_FLAG_HT2))
  {
    DMA1->IFCR |= DMA_FLAG_GL2;
  }
  if(DMA1->ISR & (DMA_FLAG_TC3 | DMA_FLAG_TE3 | DMA_FLAG_HT3))
  {
    DMA1->IFCR |= DMA_FLAG_GL3;
  }
  /* USER CODE END DMA1_Ch2_3_DMA2_Ch1_2_IRQn 1 */
}

/**
  * @brief This function handles DMA1 channel 4 to 7 and DMA2 channel 3 to 5 interrupts.
  */
void DMA1_Ch4_7_DMA2_Ch3_5_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Ch4_7_DMA2_Ch3_5_IRQn 0 */

  /* USER CODE END DMA1_Ch4_7_DMA2_Ch3_5_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart2_tx); // DMA2 CH4
  // HAL_DMA_IRQHandler(&hdma_usart2_rx); // DMA2 CH3

  /* USER CODE BEGIN DMA1_Ch4_7_DMA2_Ch3_5_IRQn 1 */
  if(DMA2->ISR & (DMA_FLAG_TC3 | DMA_FLAG_TE3 | DMA_FLAG_HT3))
  {
    DMA2->IFCR |= DMA_FLAG_GL3;
  }
  if(DMA2->ISR & (DMA_FLAG_TC4 | DMA_FLAG_TE4 | DMA_FLAG_HT4))
  {
    DMA2->IFCR |= DMA_FLAG_GL4;
  }
  /* USER CODE END DMA1_Ch4_7_DMA2_Ch3_5_IRQn 1 */
}

/**
  * @brief This function handles TIM6 global and DAC channel underrun error interrupts.
  */
void TIM6_DAC_IRQHandler(void)
{
  /* USER CODE BEGIN TIM6_DAC_IRQn 0 */

  /* USER CODE END TIM6_DAC_IRQn 0 */
  HAL_TIM_IRQHandler(&htim6);
  /* USER CODE BEGIN TIM6_DAC_IRQn 1 */

  /* USER CODE END TIM6_DAC_IRQn 1 */
}

/**
  * @brief This function handles USART3 to USART8 global interrupts / USART3 wake-up interrupt through EXTI line 28.
  */
void USART3_8_IRQHandler(void)
{
  /* USER CODE BEGIN USART3_8_IRQn 0 */

  /* USER CODE END USART3_8_IRQn 0 */
  HAL_UART_IRQHandler(&huart4);
  /* USER CODE BEGIN USART3_8_IRQn 1 */
  USART4_IrqHandler(&huart4, &hdma_usart4_rx);
  /* USER CODE END USART3_8_IRQn 1 */
}

/* USER CODE BEGIN 1 */

void USART2_IRQHandler(void)
{
  HAL_UART_IRQHandler(&huart2);
  USART2_IrqHandler(&huart2, &hdma_usart2_rx);
}
/**
  * @brief This function handles TIM1 capture compare interrupt.
  */

/**
  * @brief This function handles TIM3 global interrupt.
  */
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
  if((htim->Instance == TIM1)
  || (htim->Instance == TIM2))
  {
    if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_3)
    {
      mbd_on_impulse_measure_timeout(htim);
    }
  }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  // тут приходит прерывание от переполнения в режиме 5
  // в режиме 5 надо только инкрементировать счетчик
  if((htim->Instance == TIM1)
  || (htim->Instance == TIM2)
  || (htim->Instance == TIM3))
  {
    mbd_inc_counter(htim);
  }

  if (htim->Instance == TIM6) // systick
  {
    HAL_IncTick();
  }
}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
  mbd_on_input_capture(htim);
}

void TIM1_BRK_UP_TRG_COM_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&htim1);
}

void TIM1_CC_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&htim1);
}

void TIM3_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&htim3);
}

void EXTI0_1_IRQHandler(void)
{
  // get tim2 value
  uint16_t val = TIM7->CNT;

  if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_0))
  {
    mbd_on_exti(0, val);
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_0);
  }

  if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_1))
  {
    mbd_on_exti(1, val);
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_1);
  }
}

void EXTI2_3_IRQHandler(void)
{
  // uint16_t val = TIM7->CNT;

  // if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_2))
  // {
  //   __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_2);
  //   mbd_on_exti(2, val);
  // }

  // if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_3))
  // {
  //   __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_3);
  //   mbd_on_exti(3, val);
  // }
}

void EXTI4_15_IRQHandler(void)
{
  uint16_t val = TIM7->CNT;

  if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_6))
  {
    mbd_on_exti(3, val);
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_6);
  }

  if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_7))
  {
    mbd_on_exti(2, val);
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_7);
  }
  
  if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_8))
  {
    mbd_on_exti(4, val);
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_8);
  }

  if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_9))
  {
    mbd_on_exti(5, val);
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_9);
  }

  if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_10))
  {
    mbd_on_exti(6, val);
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_10);
  }

  if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_11))
  {
    mbd_on_exti(7, val);
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_11);
  }
}

void TIM2_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&htim2);
}

void TIM7_IRQHandler(void)
{
  TIM7->SR &= ~TIM_SR_UIF;
  mbd_on_ovf();
}

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
