#include "log.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include <stdio.h>
#include <stdarg.h>
#include "dma_usart_idle.h"
#include <string.h>
#include <stdbool.h>

#define LOG_BUFF_SIZE 255

struct{
    uint8_t level;
    char buffer[LOG_BUFF_SIZE];
}logger;

static const char *level_names[] = {
  "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL"
};

extern UART_HandleTypeDef huart2;
SemaphoreHandle_t logSemaphore = NULL;

void log_log(uint8_t level, const char *file, int line, const char* fmt, ...)
{
    if(logSemaphore == NULL)
        logSemaphore = xSemaphoreCreateMutex();

    if(logSemaphore != NULL)
    {
        if(xSemaphoreTake(logSemaphore, ( TickType_t )10) == pdTRUE)
        {
            uint32_t time = operation_time();
            uint8_t skip = 0;
            skip = snprintf(logger.buffer, LOG_BUFF_SIZE, "%lu [%s] %s:%d ", time, level_names[level], file, line);

            va_list args;
            va_start(args, fmt);
            int rc = vsnprintf(&logger.buffer[skip], sizeof(logger.buffer), fmt, args);
            va_end(args);

            HAL_UART_Transmit(&huart2, (uint8_t *)logger.buffer, skip + rc, 200);

            xSemaphoreGive(logSemaphore);
        }
    }
}

void log_out(const char *fmt, ...)
{
    if(logSemaphore == NULL)
        logSemaphore = xSemaphoreCreateMutex();

    if(logSemaphore != NULL)
    {
        portBASE_TYPE taskWoken = pdFALSE;
        if(xSemaphoreTakeFromISR(logSemaphore, &taskWoken) == pdTRUE)
        {
            vPortEnterCritical();

            va_list args;
            va_start(args, fmt);
            int rc = vsnprintf(logger.buffer, sizeof(logger.buffer), fmt, args);
            va_end(args);

            HAL_UART_Transmit(&huart2, (uint8_t *)logger.buffer, rc, 200);

            xSemaphoreGiveFromISR(logSemaphore, &taskWoken);
            vPortExitCritical();
        }
    }
}
