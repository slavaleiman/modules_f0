#include "stm32f0xx.h"
#include "stm32f0xx_hal_uart.h"
#include "string.h"
#include "dma_usart_idle.h"
#include "log.h"
#include "modbus_device.h"
#include "errors.h"
#include <stdio.h>
#include <stdlib.h>

const char* help_msg = "\
help        - show this message\n\
status      - show modbus device status\n\
config      - show modbus device config\n\
baud        - set baudrate for RS485 interface (max = 460800)\n\
errors      - read errors\n\
";

void user_cmd(uint8_t* message)
{
    char* help = strstr((char*)message, "help");
    if(help)
    {
        log_out((char*)help_msg);
        return;
    }

    char* status_str = strstr((char*)message, "status");
    if(status_str)
    {
        mbd_log_status();
        return;
    }

    char* config_str = strstr((char*)message, "config");
    if(config_str)
    {
        mbd_log_config();
        return;
    }

    char* baud_str = strstr((char*)message, "baud");
    if(baud_str)
    {
        uint32_t baudrate = atoi((char*)&baud_str[4]);
        if(baudrate && (baudrate < 460801))
        {
            mbd_set_baudrate(baudrate);
        }
        mbd_log_config();
        return;
    }

    char* errors_str = strstr((char*)message, "errors");
    if(errors_str)
    {
        errors_log_errors();
        return;
    }
}
