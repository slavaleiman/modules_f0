#ifndef _CRC_H_
#define _CRC_H_ 1

#include <stdbool.h>
#include "stm32f091xc.h"

uint16_t CRC16(unsigned char* msg, uint16_t data_len);
#endif // _CRC_H_
