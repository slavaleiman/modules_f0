#ifndef __MODBUS_DEVICE__
#define __MODBUS_DEVICE__ 1

#include <stdbool.h>
#include "ext_mem.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

#define MBD_INIT_ADDR   1
#define DI_LENGTH       1

#define MBD_NUM_INPUTS  8

#define EXT_INIT_BAUDRATE 38400
#define INT_INIT_BAUDRATE 4800

#define DI0_PORT    GPIOA
#define DI0_PIN     GPIO_PIN_0
#define DI1_PORT    GPIOA
#define DI1_PIN     GPIO_PIN_1
#define DI2_PORT    GPIOC
#define DI2_PIN     GPIO_PIN_7
#define DI3_PORT    GPIOC
#define DI3_PIN     GPIO_PIN_6
#define DI4_PORT    GPIOA
#define DI4_PIN     GPIO_PIN_8
#define DI5_PORT    GPIOA
#define DI5_PIN     GPIO_PIN_9
#define DI6_PORT    GPIOA
#define DI6_PIN     GPIO_PIN_10
#define DI7_PORT    GPIOA
#define DI7_PIN     GPIO_PIN_11

#define NUM_SAMPLES 4
typedef struct
{
    uint8_t  mode;      // режим входа, все режимы используют значение со входа после масок
    int8_t   new_mode;
    uint16_t param_1;   // настройки, для каждого режима свои, см. описание
    uint32_t param_2;   // используется для стартового значения в режимах IMPULSE_COUNT и ACTIVE_COUNT

    uint16_t result_id; // порядковый номер измерения
    uint16_t flags;     // флаги валидности значений
    uint32_t val_0;     // результат измерения 1
    uint32_t val_1;     // результат измерения 2

    uint16_t counter;   // счетчик подавителя дребезга в режиме 1, 2, 3
                        // счетчик переполнений частотомеров в режиме 4,5,6
    uint16_t duty_counter;  // счетчик переполнений частотомеров в режиме 6

    uint32_t last_capture; // предыдущее значение, для рассчета частоты

    int32_t ic_period[NUM_SAMPLES]; // для режима 5 интервал между отсчетами
    uint8_t sample;
}input_t;

typedef struct
{
    uint8_t  addr;
    uint32_t baudrate;

    uint32_t counter;
    uint32_t start_time;
    uint16_t count_ovr; // счетчик переполнений системного тика
// ----------лежит во внешней памяти---------------------------
    uint64_t operation_time;
    uint16_t program_crc;
    uint16_t ext_mem_crc;
    uint32_t change_time; // ext mem change time
    uint32_t total_rq;   // счетчик запросов
    uint32_t handled_rq; // счетчик обработанных запросов
    uint32_t err_crc_rq; // счетчик запросов с ошибкой CRC
    uint32_t err_param_rq; // счетчик запросов с некорректными параметрами
    uint32_t err_send;  // счетчик ошибок при отправке

    uint16_t and_mask;
    uint16_t or_mask;
// ------------------------------------------------------------
    uint16_t masked_input;
    uint16_t input_reg; // discrete values of DI ports

    input_t input[MBD_NUM_INPUTS];

    bool     is_init;
    uint16_t update; // for store to ext memory : used 11 bits => 8 inputs + 1 common flag + 2 tim flags
}device_t;

enum{
    RS485_ADDRESS_REG = 22,
    RS485_BAUDRATE_HREG,
    RS485_BAUDRATE_LREG,

    DI_REG = 25,
    AND_MASK_REGISTER,
    OR_MASK_REGISTER,
    MASKED_REGISTER,
// адреса параметров входов
    DI0_MODE_REG = 29,
    PARAM1_REG_0,
    PARAM2_1_REG_0,
    PARAM2_2_REG_0,
    DI1_MODE_REG,  // 33
    PARAM1_REG_1,
    PARAM2_1_REG_1,
    PARAM2_2_REG_1,
    DI2_MODE_REG,  // 37
    PARAM1_REG_2,
    PARAM2_1_REG_2,
    PARAM2_2_REG_2,
    DI3_MODE_REG,  // 41
    PARAM1_REG_3,
    PARAM2_1_REG_3,
    PARAM2_2_REG_3,
    DI4_MODE_REG,  // 45
    PARAM1_REG_4,
    PARAM2_1_REG_4,
    PARAM2_2_REG_4,
    DI5_MODE_REG,  // 49
    PARAM1_REG_5,
    PARAM2_1_REG_5,
    PARAM2_2_REG_5,
    DI6_MODE_REG, // 53
    PARAM1_REG_6,
    PARAM2_1_REG_6,
    PARAM2_2_REG_6,
    DI7_MODE_REG,  // 57
    PARAM1_REG_7,
    PARAM2_1_REG_7,
    PARAM2_2_REG_7,

// адреса измеренных значений
// при обращении к регистру, значение копируется из device.rate_meter.period1 и т.д в регистры val_0 b val_1 канала
    RESULT_ID_0, // 61
    FLAGS_0,
    VAL_0_0_0,  // 63
    VAL_0_1_0,
    VAL_1_0_0,
    VAL_1_1_0,
    RESULT_ID_1,
    FLAGS_1,
    VAL_0_0_1,  // 69
    VAL_0_1_1,
    VAL_1_0_1,
    VAL_1_1_1,
    RESULT_ID_2,
    FLAGS_2,
    VAL_0_0_2,  // 75
    VAL_0_1_2,
    VAL_1_0_2,
    VAL_1_1_2,
    RESULT_ID_3,
    FLAGS_3,
    VAL_0_0_3,  // 81
    VAL_0_1_3,
    VAL_1_0_3,
    VAL_1_1_3,
    RESULT_ID_4,
    FLAGS_4,
    VAL_0_0_4,  // 87
    VAL_0_1_4,
    VAL_1_0_4,
    VAL_1_1_4,
    RESULT_ID_5,
    FLAGS_5,
    VAL_0_0_5,  // 93
    VAL_0_1_5,
    VAL_1_0_5,
    VAL_1_1_5,
    RESULT_ID_6,
    FLAGS_6,
    VAL_0_0_6,  // 99
    VAL_0_1_6,
    VAL_1_0_6,
    VAL_1_1_6,
    RESULT_ID_7,
    FLAGS_7,
    VAL_0_0_7,  // 105
    VAL_0_1_7,
    VAL_1_0_7,
    VAL_1_1_7,

    SPARE_INIT  // 109
}DI_REGISTERS;

uint32_t mbd_curr_time(void);
uint32_t mbd_curr_time_ms(void);

int8_t  mbd_discrete_input(uint8_t addr, uint16_t* value);
int8_t  mbd_holding_reg(uint16_t addr, uint16_t* value);
int8_t  mbd_input_reg(uint16_t addr, uint16_t* value);
int8_t  mbd_write_reg(uint16_t addr, uint16_t value);

void mbd_inc_crc_err_rq(void);
void mbd_inc_handled_rq(void);
void mbd_inc_total_rq(void);

uint32_t update_time(void);
void mbd_process(void);
void mbd_init(void);

void mbd_on_input_capture(TIM_HandleTypeDef *htim);
void mbd_on_ovf(void);
void mbd_on_exti(uint8_t channel, uint16_t val);
void mbd_inc_counter(TIM_HandleTypeDef *htim);

void mbd_pwm_input_reset(TIM_HandleTypeDef *htim);
void mdb_inc_send_err(void);

void mbd_input_load(input_params_t* input, uint8_t input_num);
void mbd_input_params(input_params_t* input, uint8_t input_num);
void mbd_common_params(common_params_t* cp);

uint16_t mbd_update_status(void);
void mbd_clear_flag(void);

void mbd_set_common_params(common_params_t* cp);
uint32_t operation_time(void);

void mbd_log_status(void);
void mbd_on_impulse_measure_timeout(TIM_HandleTypeDef *htim);
uint16_t mbd_module_id(void);

void mbd_init_mode(bool is_on);
void mbd_log_config(void);
void mbd_set_baudrate(uint32_t baudrate);

uint8_t mbd_addr(void);

int8_t mbd_rs485_init(uint32_t baudrate);
void mbd_int_init(void);
void mbd_ext_init(void);
void mbd_exit_init_mode(void);
bool mbd_can_init(void);
bool mbd_is_init(void);

#endif //__MODBUS_DEVICE__
