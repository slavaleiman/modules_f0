/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V.
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice,
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other
  *    contributors to this software may be used to endorse or promote products
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under
  *    this license is void and will automatically terminate your rights under
  *    this license.
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "dma_usart_idle.h"
#include "modbus.h"
#include "modbus_device.h"
#include <string.h>
#include "ext_mem.h"
#include "log.h"
#include "errors.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
TIM_HandleTypeDef        htim3;
TIM_HandleTypeDef        htim1;
TIM_HandleTypeDef        htim6;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;
bool is_usart4_sending = false;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart4;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart2_tx; 
DMA_HandleTypeDef hdma_usart4_rx;
DMA_HandleTypeDef hdma_usart4_tx;

osThreadId modbusHandle;
osThreadId Tick_TaskHandle;
osThreadId InputProcessHandle;

osSemaphoreId Usart4RxSemaphoreHandle;
/* USER CODE BEGIN PV */
QueueHandle_t RxQueue;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART4_UART_Init(void);
static void MX_I2C1_Init(void);
void modbus_task(void const * argument);
void tick_task(void const * argument);
void input_process(void const * argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

CRC_HandleTypeDef hcrc;

// AHTUNG !!! check rom_end addr after build
#define ROM_START     (uint8_t*)(0x8000000)
#define ROM_END       (uint8_t*)(0x8008FFB)
#define CHECKSUM      (uint8_t*)(0x8008FFC)
#define ROM_LEN       (uint32_t)(ROM_END - ROM_START + 1u)
#define ROM_LEN_WORD  (uint32_t)(ROM_LEN / 4u)
#define ROM_PAGE_SIZE (uint32_t)0x1000
#define ROM_PAGE_LEN_WORD  (uint32_t)(ROM_PAGE_SIZE / 4u)
#define ROM_LEN_PAGES (uint32_t)(ROM_LEN / ROM_PAGE_SIZE + 1)

uint32_t crc, checksum;

static void CRC_Init(void)
{
  __HAL_RCC_CRC_CLK_ENABLE();
  hcrc.Instance = CRC;
  hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_WORDS;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    Error_Handler();
  }
}

void int_ext_inputs_init(void)
{
  // INT_INIT EXT_INIT
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  GPIO_InitStruct.Pin = INT_INIT_PIN | EXT_INIT_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(INT_INIT_PORT, &GPIO_InitStruct);

  // LED INIT MODE
  GPIO_InitStruct.Pin = INIT_MODE_LED_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(INIT_MODE_LED_PORT, &GPIO_InitStruct);

  // FAULT LED
  GPIO_InitStruct.Pin = FAULT_LED_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(FAULT_LED_PORT, &GPIO_InitStruct);

  // SPARE INIT - выход имеет свой регистр в модбас
  GPIO_InitStruct.Pin = SPARE_INIT_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(SPARE_INIT_PORT, &GPIO_InitStruct);

  // HARDWARE WATCHDOG - нужно постоянно дергать за поводок не реже 1.6 с
  GPIO_InitStruct.Pin = WATCHDOG_OUTPUT_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(WATCHDOG_OUTPUT_PORT, &GPIO_InitStruct);
}

uint32_t crc_calc_page(uint32_t *buffer, uint32_t page_len_word)
{
  uint32_t temp = hcrc.Instance->DR;
  for(uint32_t i = 0; i < page_len_word; ++i)
  { 
    hcrc.Instance->DR = buffer[i]; 
  } 
  temp = hcrc.Instance->DR;
  return temp;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_USART4_UART_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */
  CRC_Init();
  crc = HAL_CRC_Calculate(&hcrc, (uint32_t*)ROM_START, (uint32_t)ROM_LEN_WORD);
  checksum = *(uint32_t*)CHECKSUM;
  if(crc != checksum)
  {
    Error_Handler();
  }
  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of UsartRxSemaphore */
  osSemaphoreDef(Usart4RxSemaphore);
  Usart4RxSemaphoreHandle = osSemaphoreCreate(osSemaphore(Usart4RxSemaphore), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  RxQueue = xQueueCreate(6, sizeof(message_t));

  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of modbus */
  osThreadDef(modbus, modbus_task, osPriorityIdle, 0, 128);
  modbusHandle = osThreadCreate(osThread(modbus), NULL);

  /* definition and creation of Tick_Task */
  osThreadDef(Tick_Task, tick_task, osPriorityIdle, 0, 128);
  Tick_TaskHandle = osThreadCreate(osThread(Tick_Task), NULL);

  /* definition and creation of InputProcess */
  osThreadDef(InputProcess, input_process, osPriorityIdle, 0, 256);
  InputProcessHandle = osThreadCreate(osThread(InputProcess), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  log_info("Module %s started\n", MODULE_NAME);
  errors_init();
  modbus_init();
  dma_usart_idle_init();

  int_ext_inputs_init();

  ext_mem_read_errors();

  log_info("Init done\n");

// #ifndef DEBUG
#if 0 // soft watchdog not tested
  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_32;
  hiwdg.Init.Reload = 0xFFF;
  hiwdg.Init.Window = 0xFFF;
  HAL_IWDG_Init(&hiwdg);

  __HAL_DBGMCU_FREEZE_IWDG(); // на время дебага
#endif


  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* USER CODE END RTOS_QUEUES */


  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Initializes the CPU, AHB and APB busses clocks
  */
  //RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  // RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  // RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  // RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  } 
 /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART4_UART_Init(void)
{

  /* USER CODE BEGIN USART4_Init 0 */

  /* USER CODE END USART4_Init 0 */

  /* USER CODE BEGIN USART4_Init 1 */

  /* USER CODE END USART4_Init 1 */
  huart4.Instance = USART4;
  huart4.Init.BaudRate = 9600;
  mbd_set_baudrate(huart4.Init.BaudRate);
  huart4.Init.WordLength = UART_WORDLENGTH_8B;
  huart4.Init.StopBits = UART_STOPBITS_1;
  huart4.Init.Parity = UART_PARITY_NONE;
  huart4.Init.Mode = UART_MODE_TX_RX;
  huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart4.Init.OverSampling = UART_OVERSAMPLING_16;
  huart4.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;

  HAL_RS485Ex_Init(&huart4, UART_DE_POLARITY_HIGH, 31, 31);
  if (HAL_UART_Init(&huart4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART4_Init 2 */
  /* USER CODE END USART4_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Ch1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Ch1_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(DMA1_Ch1_IRQn);
  /* DMA1_Ch2_3_DMA2_Ch1_2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Ch2_3_DMA2_Ch1_2_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(DMA1_Ch2_3_DMA2_Ch1_2_IRQn);
  /* DMA1_Ch4_7_DMA2_Ch3_5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Ch4_7_DMA2_Ch3_5_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(DMA1_Ch4_7_DMA2_Ch3_5_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_modbus_task */
/**
  * @brief  Function implementing the modbus thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_modbus_task */
UBaseType_t modbusHighWaterMark;

void modbus_task(void const * argument)
{
  /* USER CODE BEGIN 5 */
  (void)argument;
  /* Infinite loop */
  for(;;)
  {
    // modbusHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
    if(xSemaphoreTake(Usart4RxSemaphoreHandle, portMAX_DELAY) == pdTRUE)
    {
      message_t mess;
      if(RxQueue)
      {
        if(xQueueReceive(RxQueue, &mess, pdMS_TO_TICKS(15)))
        {
          if(mess.len != 0)
          {
            modbus_on_rtu(mess.data, mess.len);
            memset(mess.data, 0, mess.len);
          }
        }
      }
    }
  }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_tick_task */
/**
* @brief Function implementing the Tick_Task thread.
* @param argument: Not used
* @retval None
*/

uint8_t write_rom_error = 0; // for test
/* USER CODE END Header_tick_task */

UBaseType_t tickHighWaterMark;
void tick_task(void const * argument)
{
  /* USER CODE BEGIN tick_task */
  (void)argument;
  /* Infinite loop */
  uint32_t stored_time = 0;
  uint32_t crc_page = 0;
  for(;;)
  {
    uint32_t time = update_time();

    tickHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
    osDelay(1000);
    // time save
    if(time > stored_time + 60)
    {
      int8_t ret = ext_mem_store_time(time);
      if(!ret)
        stored_time = time;
    }
    if(errors_is_update())
    {
      ext_mem_store_errors();
      errors_update_clear();
    }
    if(!crc_page)
    {
      __HAL_CRC_DR_RESET(&hcrc);
    }
    uint32_t page_size = (crc_page == ROM_LEN_PAGES - 1) ? ROM_PAGE_LEN_WORD - 1 : ROM_PAGE_LEN_WORD;
    crc = crc_calc_page((uint32_t*)(ROM_START + ROM_PAGE_SIZE * crc_page), page_size);
    ++crc_page;
    if(crc_page >= ROM_LEN_PAGES)
    {
      checksum = *(uint32_t*)CHECKSUM;
      if(crc != checksum)
      {
        Error_Handler();
      }
      crc_page = 0; 
    }
  }
  /* USER CODE END tick_task */
}

/* USER CODE BEGIN Header_input_process */
/**
* @brief Function implementing the InputProcess thread.
* @param argument: Not used
* @retval None
*/

/* USER CODE END input_process */

/* USER CODE END Header_input_process */
UBaseType_t inputHighWaterMark;

void input_process(void const * argument)
{
  /* USER CODE BEGIN input_process */
  (void)argument;
  /* Infinite loop */
  for(;;)
  {
    // inputHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
    mbd_process();

    if(mbd_can_init())
    {
      if(~EXT_INIT_PORT->IDR & EXT_INIT_PIN)
      {
        mbd_ext_init();
      }
      else if(~INT_INIT_PORT->IDR & INT_INIT_PIN)
      {
        mbd_int_init();
      }
    }else if((INT_INIT_PORT->IDR & INT_INIT_PIN) && (EXT_INIT_PORT->IDR & EXT_INIT_PIN) && mbd_is_init())
    {
      mbd_exit_init_mode();
    }

    // wake up external watchdog
    WATCHDOG_OUTPUT_PORT->ODR ^= WATCHDOG_OUTPUT_PIN;
    osDelay(1);
  }
  /* USER CODE END input_process */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1);
  /* USER CODE END Error_Handler_Debug */
}

void vApplicationStackOverflowHook( TaskHandle_t xTask, char *pcTaskName )
{
    (void)xTask;
    (void)pcTaskName;
    while(1);
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
