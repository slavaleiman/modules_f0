#include "main.h"
#include "stm32f091xc.h"
#include <string.h>
#include "ext_mem.h"
#include "CRC.h"
#include "modbus_device.h"
#include "cmsis_os.h" // test delay for
#include "errors.h"

#define CRC_SIZE            1
#define USED_EEPROM         0
#define DATA_SIZE sizeof(mem_data)

#if USED_EEPROM
    #define EEPROM_PAGE_SIZE    64
    #define TIME_PAGE (DATA_SIZE / EEPROM_PAGE_SIZE + 2)
#else
    #define TIME_SKIP (DATA_SIZE + 2)
    #define ERRORS_SKIP (TIME_SKIP + 4) // time size = 4
#endif

#define EXT_MEM_MCU_ADDR         (80 << 1) // 1010000

#define WP_PIN  GPIO_PIN_12
#define WP_PORT GPIOC

#define WRITE_PROTECT_DISABLE() HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_RESET);
#define WRITE_PROTECT_ENABLE()  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_SET);

extern I2C_HandleTypeDef hi2c1;

typedef struct __attribute__((__packed__))
{
    common_params_t cp; // common addr = 0
    input_params_t input[8];
}mem_data_t;
mem_data_t mem_data;

static int8_t store_all(void)
{
    uint8_t* buff = (uint8_t*)&mem_data;

    uint16_t crc = CRC16(buff, DATA_SIZE);

    WRITE_PROTECT_DISABLE();
#if USED_EEPROM

    uint8_t pages = DATA_SIZE / EEPROM_PAGE_SIZE + 1;
    int16_t last = DATA_SIZE;
    uint16_t skip = 0;

    for(uint8_t i = 0; i < pages; ++i)
    {
        int16_t to_write = (EEPROM_PAGE_SIZE > last) ? last : EEPROM_PAGE_SIZE;
        if(to_write <= 0)
            break;
        HAL_StatusTypeDef st = HAL_I2C_Mem_Write(&hi2c1, EXT_MEM_MCU_ADDR, skip, 2, &buff[skip], to_write, 200);
        if(st)
        {
            WRITE_PROTECT_ENABLE();
            return st;
        }
        osDelay(5);
        skip += to_write;
        last -= to_write;
    }

    HAL_StatusTypeDef st = HAL_I2C_Mem_Write(&hi2c1, EXT_MEM_MCU_ADDR, pages * EEPROM_PAGE_SIZE, 2, (uint8_t*)&crc, 2, 200);
    osDelay(5);
#else
    // FRAM
    HAL_StatusTypeDef st = HAL_I2C_Mem_Write(&hi2c1, EXT_MEM_MCU_ADDR, 0, 2, buff, DATA_SIZE, 200);
    if(st)
    {
        WRITE_PROTECT_ENABLE();
        return st;
    }
    st = HAL_I2C_Mem_Write(&hi2c1, EXT_MEM_MCU_ADDR, DATA_SIZE, 2, (uint8_t*)&crc, 2, 200);
#endif
    WRITE_PROTECT_ENABLE();
    if(st)
        return st;

    return 0;
}

int8_t ext_mem_store(void)
{
    int8_t ret = 0;
    for(uint8_t i = 0; i < 8; ++i)
    {
        mbd_input_params(&mem_data.input[i], i);
    }
    mbd_common_params(&mem_data.cp);

    ret = store_all();
    return ret;
}

// time stored at own page
int8_t ext_mem_store_time(uint32_t time)
{
    WRITE_PROTECT_DISABLE();
#if USED_EEPROM
    HAL_StatusTypeDef st = HAL_I2C_Mem_Write(&hi2c1, EXT_MEM_MCU_ADDR, TIME_PAGE * EEPROM_PAGE_SIZE, 2, (uint8_t*)&time, 4, 200);
#else
    HAL_StatusTypeDef st = HAL_I2C_Mem_Write(&hi2c1, EXT_MEM_MCU_ADDR, TIME_SKIP, 2, (uint8_t*)&time, 4, 200);
#endif
    WRITE_PROTECT_ENABLE();
    return st;    
}

void ext_mem_init(void)
{
    // WP pin init
    __HAL_RCC_GPIOC_CLK_ENABLE();
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Pin = WP_PIN;
    HAL_GPIO_Init(WP_PORT, &GPIO_InitStruct);
}

int8_t ext_mem_load(void)
{
    HAL_StatusTypeDef st = {0};

    ext_mem_init();
    WRITE_PROTECT_ENABLE();

    uint8_t buffer[sizeof(mem_data_t) + 2]; // data + crc
    uint16_t read_size = sizeof(mem_data);
#if USED_EEPROM
    uint8_t pages = read_size / EEPROM_PAGE_SIZE + 1;
    uint16_t last = read_size;
    uint16_t skip = 0;
    for(uint8_t i = 0; i < pages; ++i)
    {
        int16_t to_read = (EEPROM_PAGE_SIZE > last) ? last : EEPROM_PAGE_SIZE;
        if(to_read <= 0)
            break;
        st = HAL_I2C_Mem_Read(&hi2c1, EXT_MEM_MCU_ADDR, skip, 2, &buffer[skip], to_read, 200);
        if(st)
            return st;
        
        osDelay(5);

        skip += to_read;
        last -= to_read;
    }

    uint16_t read_crc;
    st = HAL_I2C_Mem_Read(&hi2c1, EXT_MEM_MCU_ADDR, pages * EEPROM_PAGE_SIZE, 2, (uint8_t*)&read_crc, 2, 200);
    if(st)
        return st;

    uint32_t operation_time;
    st = HAL_I2C_Mem_Read(&hi2c1, EXT_MEM_MCU_ADDR, TIME_PAGE * EEPROM_PAGE_SIZE, 2, (uint8_t*)&operation_time, 4, 200);
    if(st)
        return st;
    
#else
    // FRAM
    st = HAL_I2C_Mem_Read(&hi2c1, EXT_MEM_MCU_ADDR, 0, 2, buffer, read_size + 2, 200);
    if(st)
        return st;
    uint16_t read_crc = buffer[read_size] | buffer[read_size + 1] << 8;
    
    uint32_t operation_time;
    st = HAL_I2C_Mem_Read(&hi2c1, EXT_MEM_MCU_ADDR, TIME_SKIP, 2, (uint8_t*)&operation_time, 4, 200);
    if(st)
       return st;
#endif
    uint16_t calc_crc = CRC16(buffer, read_size);
    if(calc_crc != read_crc)
        return -6;

    memcpy(&mem_data, buffer, sizeof(mem_data));
    mem_data.cp.operation_time = operation_time;
    
    if(mbd_module_id() != mem_data.cp.module_id)
        return -7;

    mbd_set_common_params(&mem_data.cp);

    for(uint8_t i = 0; i < 8; ++i)
    {
        input_params_t* input = &mem_data.input[i];
        mbd_input_load(input, i);
    }
    return 0;
}

int8_t ext_mem_store_errors(void)
{
    WRITE_PROTECT_DISABLE();
#if USED_EEPROM
    // HAL_StatusTypeDef st = HAL_I2C_Mem_Write(&hi2c1, EXT_MEM_MCU_ADDR, TIME_PAGE * EEPROM_PAGE_SIZE, 2, (uint8_t*)&time, 4, 200); // TODO someday maybe never
#else
    uint8_t* errors = errors_buffer();
    HAL_StatusTypeDef st = HAL_I2C_Mem_Write(&hi2c1, EXT_MEM_MCU_ADDR, ERRORS_SKIP, 2, errors, errors_size(), 200);
#endif
    WRITE_PROTECT_ENABLE();
    return st;
}

int8_t ext_mem_read_errors(void)
{
    uint8_t* buffer = errors_buffer();
    uint16_t read_size = errors_size();
    HAL_StatusTypeDef st = HAL_I2C_Mem_Read(&hi2c1, EXT_MEM_MCU_ADDR, 0, 2, buffer, read_size + 2, 200);
    if(st)
        return st;
    return 0;
}
