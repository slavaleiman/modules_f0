#include "main.h"
#include "stm32f0xx.h"
#include "modbus_device.h"
#include "modbus.h"
#include "dma_usart_idle.h"
#include <stdbool.h>
#include <string.h>
#include "ext_mem.h"
#include "log.h"
#include "errors.h"
#include "soft_timer.h"
#include "version.h"

#define MODULE_ID   21801
#define PCB_VER     1
#define PCB_MNF     1 // manufacturer
#define PCB_ASSEM   1
#define SW_VER      MODULE_VERSION_MAJOR
#define SW_SUBVER   MODULE_VERSION_MINOR
#define SW_BUILD    1

#define TIM2_DUTY_FLAG (1 << 12) // для подсчета переполнений таймера в разных состояниях входа (если флаг установлен - считаем низкий уровень)
#define TIM1_DUTY_FLAG (1 << 11)

#define TIM2_UPDATE_FLAG (1 << 10)
#define TIM1_UPDATE_FLAG (1 << 9)

#define PWM_INPUT_TIMEOUT       0xFFFF
#define TIM7_PERIOD             0xFFFF // soft rate meter

#define IC_TIMEOUT              0x600 // если counter досчитал до этого значения,- значение обнуляется // TODO пересчитать

#define MBD_FRONT_BIT    0x1
#define MBD_FALLING_FRONT_BIT   0x1

#define TIM_PSC                 3

uint64_t TIM_CLOCK = 48000000;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim7;

device_t device;

enum{
    COMPARATOR          = 0, // чтение выхода компаратора
    FLUTTER_REDUCTION   = 1, // устранение дребезга
    IMPULSE_COUNT       = 2, // счетчик количества импульсов
    ACTIVE_COUNT        = 3, // измерение длительности активного сигнала
    RATE_MEASURE_SOFT   = 4, // измерение частоты, на прерываниях и одном таймере, не точное измерение!
    RATE_MEASURE_HARD   = 5, // измерение частоты, на основе INPUT_CAPTURE
    IMPULSE_MEASURE     = 6, // измерение параметров импульсов, на основе PWM_INPUT
}input_mode_e;

uint16_t mbd_module_id(void)
{
    return MODULE_ID;
}

uint32_t update_time(void)
{
    uint32_t counter = HAL_GetTick() / 1000;
    if(counter < device.counter) // если произошло переполнение
    {
        ++device.count_ovr;
    }
    device.counter = counter;
    device.operation_time = device.start_time + device.counter + device.count_ovr * (0xFFFFFFFF / 1000); // секунд
    return device.operation_time;
}

uint32_t operation_time(void)
{
    return device.operation_time;
}

int8_t mbd_discrete_input(uint8_t reg, uint16_t* value)
{
    switch(reg)
    {
        case DI_REG:
            *value = device.input_reg;
            return 0;
        default:
            break;
    }
    return DATA_ADDR_ERR;
}

void update_input(void)
{
#ifndef _UNITTEST_
    device.input_reg = (GPIOA->IDR & 3) | ((GPIOC->IDR & (1 << 6)) >> 3) | (((GPIOC->IDR & (1 << 7)) >> 5)) | (((GPIOA->IDR >> 8) & 0xF) << 4);
#endif
}

void update_masked(void)
{
    update_input();
    device.masked_input = (device.input_reg & device.and_mask) | device.or_mask;
}

void mbd_exit_init_mode(void)
{
    device.is_init = false;
    INIT_MODE_LED_PORT->ODR &= ~INIT_MODE_LED_PIN;
    mbd_rs485_init(device.baudrate);
}

static void set_spare_output(uint16_t value)
{
    if(value)
    {
        device.is_init = false;
        soft_timer_init(1, (void (*)(uint8_t))&mbd_exit_init_mode, 1, 0);
        SPARE_INIT_PORT->ODR |= SPARE_INIT_PIN;
    }else{
        SPARE_INIT_PORT->ODR &= ~SPARE_INIT_PIN;
    }
}

// oooooooooo  ooooooooooo      o      ooooooooo        oooooooooo   o      oooooooooo       o      oooo     oooo
//  888    888  888    88      888      888    88o       888    888 888      888    888     888      8888o   888
//  888oooo88   888ooo8       8  88     888    888       888oooo88 8  88     888oooo88     8  88     88 888o8 88
//  888  88o    888    oo    8oooo88    888    888       888      8oooo88    888  88o     8oooo88    88  888  88
// o888o  88o8 o888ooo8888 o88o  o888o o888ooo88        o888o   o88o  o888o o888o  88o8 o88o  o888o o88o  8  o88o

int8_t mbd_holding_reg(uint16_t reg, uint16_t* value)
{
    switch(reg)
    {
        case 0: *value = MODULE_ID;
            return 0;
        case 1: *value = PCB_VER;
            return 0;
        case 2: *value = PCB_MNF; // manufacturer
            return 0;
        case 3: *value = PCB_ASSEM;
            return 0;
        case 4: *value = SW_VER;
            return 0;
        case 5: *value = SW_SUBVER;
            return 0;
        case 6: *value = SW_BUILD;
            return 0;
        case 7:
            *value = device.operation_time >> 16;
            return 0;
        case 8:
            *value = device.operation_time & 0xFFFF;
            return 0;
        case 9:
            *value = HAL_GetTick() % 1000; // CURR_MS
            return 0;
        case 10:
            *value = device.program_crc;
            return 0;
        case 11:
            *value = device.ext_mem_crc;
            return 0;
        case 12:
            *value = device.change_time >> 16;
            return 0;
        case 13:
            *value = device.change_time & 0xFFFF;
            return 0;
        case 14:
            *value = device.total_rq >> 16;
            return 0;
        case 15:
            *value = device.total_rq & 0xFFFF;
            return 0;
        case 16:
            *value = device.handled_rq >> 16;
            return 0;
        case 17:
            *value = device.handled_rq & 0xFFFF;
            return 0;
        case 18:
            *value = device.err_crc_rq >> 16;
            return 0;
        case 19:
            *value = device.err_crc_rq & 0xFFFF;
            return 0;
        case 20:
            *value = device.err_param_rq >> 16;
            return 0;
        case 21:
            *value = device.err_param_rq & 0xFFFF;
            return 0;

        case RS485_ADDRESS_REG:
            *value = device.addr;
            return 0;
        case RS485_BAUDRATE_HREG:
            *value = device.baudrate >> 16;
            return 0;
        case RS485_BAUDRATE_LREG:
            *value = device.baudrate & 0xFFFF;
            return 0;   

        //  входы
        case DI_REG:
            update_input();
            *value = device.input_reg;
            return 0;
        case AND_MASK_REGISTER:
            *value = device.and_mask;
            return 0;
        case OR_MASK_REGISTER:
            *value = device.or_mask;
            return 0;
        case MASKED_REGISTER:
            update_masked();
            *value = device.masked_input;
            return 0;

        // режимы измерений и их параметры
        // PA0
        case DI0_MODE_REG:
            *value = device.input[0].mode;
            return 0;
        case PARAM1_REG_0:
            *value = device.input[0].param_1;
            return 0;
        case PARAM2_1_REG_0:
            *value = device.input[0].param_2 >> 16;
            return 0;
        case PARAM2_2_REG_0:
            *value = device.input[0].param_2 & 0xFFFF;
            return 0;
        // PA1
        case DI1_MODE_REG:
            *value = device.input[1].mode;
            return 0;
        case PARAM1_REG_1:
            *value = device.input[1].param_1;
            return 0;
        case PARAM2_1_REG_1:
            *value = device.input[1].param_2 >> 16;
            return 0;
        case PARAM2_2_REG_1:
            *value = device.input[1].param_2 & 0xFFFF;
            return 0;
        // PC6
        case DI2_MODE_REG:
            *value = device.input[2].mode;
            return 0;
        case PARAM1_REG_2:
            *value = device.input[2].param_1;
            return 0;
        case PARAM2_1_REG_2:
            *value = device.input[2].param_2 >> 16;
            return 0;
        case PARAM2_2_REG_2:
            *value = device.input[2].param_2 & 0xFFFF;
            return 0;
        // PC7
        case DI3_MODE_REG:
            *value = device.input[3].mode;
            return 0;
        case PARAM1_REG_3:
            *value = device.input[3].param_1;
            return 0;
        case PARAM2_1_REG_3:
            *value = device.input[3].param_2 >> 16;
            return 0;
        case PARAM2_2_REG_3:
            *value = device.input[3].param_2 & 0xFFFF;
            return 0;
        // PA8
        case DI4_MODE_REG:
            *value = device.input[4].mode;
            return 0;
        case PARAM1_REG_4:
            *value = device.input[4].param_1;
            return 0;
        case PARAM2_1_REG_4:
            *value = device.input[4].param_2 >> 16;
            return 0;
        case PARAM2_2_REG_4:
            *value = device.input[4].param_2 & 0xFFFF;
            return 0;
        // PA9
        case DI5_MODE_REG:
            *value = device.input[5].mode;
            return 0;
        case PARAM1_REG_5:
            *value = device.input[5].param_1;
            return 0;
        case PARAM2_1_REG_5:
            *value = device.input[5].param_2 >> 16;
            return 0;
        case PARAM2_2_REG_5:
            *value = device.input[5].param_2 & 0xFFFF;
            return 0;
        // PA10
        case DI6_MODE_REG:
            *value = device.input[6].mode;
            return 0;
        case PARAM1_REG_6:
            *value = device.input[6].param_1;
            return 0;
        case PARAM2_1_REG_6:
            *value = device.input[6].param_2 >> 16;
            return 0;
        case PARAM2_2_REG_6:
            *value = device.input[6].param_2 & 0xFFFF;
            return 0;
        // PA11
        case DI7_MODE_REG:
            *value = device.input[7].mode;
            return 0;
        case PARAM1_REG_7:
            *value = device.input[7].param_1;
            return 0;
        case PARAM2_1_REG_7:
            *value = device.input[7].param_2 >> 16;
            return 0;
        case PARAM2_2_REG_7:
            *value = device.input[7].param_2 & 0xFFFF;
            return 0;
        case SPARE_INIT:
            *value = (SPARE_INIT_PORT->ODR & SPARE_INIT_PIN) ? 1 : 0;
            return 0;
        default:
            break;
    }
    return DATA_ADDR_ERR;
}

// oooooooooo  ooooooooooo      o      ooooooooo        ooooo oooo   oooo oooooooooo ooooo  oooo ooooooooooo
//  888    888  888    88      888      888    88o       888   8888o  88   888    888 888    88  88  888  88
//  888oooo88   888ooo8       8  88     888    888       888   88 888o88   888oooo88  888    88      888
//  888  88o    888    oo    8oooo88    888    888       888   88   8888   888        888    88      888
// o888o  88o8 o888ooo8888 o88o  o888o o888ooo88        o888o o88o    88  o888o        888oo88      o888o

// здесь только значения результаты обработки входов
int8_t mbd_input_reg(uint16_t reg, uint16_t* value)
{
    switch(reg)
    {
        case RESULT_ID_0:
            *value = device.input[0].result_id;
            return 0;
        case FLAGS_0:
            *value = device.input[0].flags;
            return 0;
        case VAL_0_0_0:
            *value = device.input[0].val_0 >> 16;
            return 0;
        case VAL_0_1_0:
            *value = device.input[0].val_0 & 0xFFFF;
            return 0;
        case VAL_1_0_0:
            *value = device.input[0].val_1 >> 16;
            return 0;
        case VAL_1_1_0:
            *value = device.input[0].val_1 & 0xFFFF;
            return 0;

        case RESULT_ID_1:
            *value = device.input[1].result_id;
            return 0;
        case FLAGS_1:
            *value = device.input[1].flags;
            return 0;
        case VAL_0_0_1:
            *value = device.input[1].val_0 >> 16;
            return 0;
        case VAL_0_1_1:
            *value = device.input[1].val_0 & 0xFFFF;
            return 0;
        case VAL_1_0_1:
            *value = device.input[1].val_1 >> 16;
            return 0;
        case VAL_1_1_1:
            *value = device.input[1].val_1 & 0xFFFF;
            return 0;

        case RESULT_ID_2:
            *value = device.input[2].result_id;
            return 0;
        case FLAGS_2:
            *value = device.input[2].flags;
            return 0;
        case VAL_0_0_2:
            *value = device.input[2].val_0 >> 16;
            return 0;
        case VAL_0_1_2:
            *value = device.input[2].val_0 & 0xFFFF;
            return 0;
        case VAL_1_0_2:
            *value = device.input[2].val_1 >> 16;
            return 0;
        case VAL_1_1_2:
            *value = device.input[2].val_1 & 0xFFFF;
            return 0;

        case RESULT_ID_3:
            *value = device.input[3].result_id;
            return 0;
        case FLAGS_3:
            *value = device.input[3].flags;
            return 0;
        case VAL_0_0_3:
            *value = device.input[3].val_0 >> 16;
            return 0;
        case VAL_0_1_3:
            *value = device.input[3].val_0 & 0xFFFF;
            return 0;
        case VAL_1_0_3:
            *value = device.input[3].val_1 >> 16;
            return 0;
        case VAL_1_1_3:
            *value = device.input[3].val_1 & 0xFFFF;
            return 0;

        case RESULT_ID_4:
            *value = device.input[4].result_id;
            return 0;
        case FLAGS_4:
            *value = device.input[4].flags;
            return 0;
        case VAL_0_0_4:
            *value = device.input[4].val_0 >> 16;
            return 0;
        case VAL_0_1_4:
            *value = device.input[4].val_0 & 0xFFFF;
            return 0;
        case VAL_1_0_4:
            *value = device.input[4].val_1 >> 16;
            return 0;
        case VAL_1_1_4:
            *value = device.input[4].val_1 & 0xFFFF;
            return 0;

        case RESULT_ID_5:
            *value = device.input[5].result_id;
            return 0;
        case FLAGS_5:
            *value = device.input[5].flags;
            return 0;
        case VAL_0_0_5:
            *value = device.input[5].val_0 >> 16;
            return 0;
        case VAL_0_1_5:
            *value = device.input[5].val_0 & 0xFFFF;
            return 0;
        case VAL_1_0_5:
            *value = device.input[5].val_1 >> 16;
            return 0;
        case VAL_1_1_5:
            *value = device.input[5].val_1 & 0xFFFF;
            return 0;

        case RESULT_ID_6:
            *value = device.input[6].result_id;
            return 0;
        case FLAGS_6:
            *value = device.input[6].flags;
            return 0;
        case VAL_0_0_6:
            *value = device.input[6].val_0 >> 16;
            return 0;
        case VAL_0_1_6:
            *value = device.input[6].val_0 & 0xFFFF;
            return 0;
        case VAL_1_0_6:
            *value = device.input[6].val_1 >> 16;
            return 0;
        case VAL_1_1_6:
            *value = device.input[6].val_1 & 0xFFFF;
            return 0;

        case RESULT_ID_7:
            *value = device.input[7].result_id;
            return 0;
        case FLAGS_7:
            *value = device.input[7].flags;
            return 0;
        case VAL_0_0_7:
            *value = device.input[7].val_0 >> 16;
            return 0;
        case VAL_0_1_7:
            *value = device.input[7].val_0 & 0xFFFF;
            return 0;
        case VAL_1_0_7:
            *value = device.input[7].val_1 >> 16;
            return 0;
        case VAL_1_1_7:
            *value = device.input[7].val_1 & 0xFFFF;
            return 0;
        default:
            break;
    }
    return DATA_ADDR_ERR;
}

void init_input(uint8_t DI)
{
#ifdef _UNITTEST_
    printf("%s\n", __FUNCTION__);
#endif
    memset(&device.input[DI], 0, sizeof(input_t));
}

void init_non_alternate_pin(uint8_t DI)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    // rising / falling edge is active
    GPIO_InitStruct.Pull = ((device.input[DI].param_1 & MBD_FRONT_BIT) == 0) ? GPIO_PULLDOWN : GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    switch(DI)
    {
        case 0:
            GPIO_InitStruct.Pin = DI0_PIN;
            HAL_GPIO_DeInit(DI0_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DI0_PORT, &GPIO_InitStruct);
            break;
        case 1:
            GPIO_InitStruct.Pin = DI1_PIN;
            HAL_GPIO_DeInit(DI1_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DI1_PORT, &GPIO_InitStruct);
            break;
        case 2:
            GPIO_InitStruct.Pin = DI2_PIN;
            HAL_GPIO_DeInit(DI2_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DI2_PORT, &GPIO_InitStruct);
            break;
        case 3:
            GPIO_InitStruct.Pin = DI3_PIN;
            HAL_GPIO_DeInit(DI3_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DI3_PORT, &GPIO_InitStruct);
            break;
        case 4:
            GPIO_InitStruct.Pin = DI4_PIN;
            HAL_GPIO_DeInit(DI4_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DI4_PORT, &GPIO_InitStruct);
            break;
        case 5:
            GPIO_InitStruct.Pin = DI5_PIN;
            HAL_GPIO_DeInit(DI5_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DI5_PORT, &GPIO_InitStruct);
            break;
        case 6:
            GPIO_InitStruct.Pin = DI6_PIN;
            HAL_GPIO_DeInit(DI6_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DI6_PORT, &GPIO_InitStruct);
            break;
        case 7:
            GPIO_InitStruct.Pin = DI7_PIN;
            HAL_GPIO_DeInit(DI7_PORT, GPIO_InitStruct.Pin);
            HAL_GPIO_Init(DI7_PORT, &GPIO_InitStruct);
            break;
        default:
            break;            
    }
}

void init_flutter_reduction(uint8_t DI)
{
    device.input[DI].flags = 0;
    // set same levels input and output
    device.input[DI].flags &= ~1;
    device.input[DI].flags |= (device.input[DI].param_1 & 1) ? 0 : 1;
}

void init_impulse_count(uint8_t DI)
{
    if(~device.input[DI].param_1 & 2)
    { // param 2 already loaded at this moment
        device.input[DI].param_2 = 0;
    }
    device.input[DI].val_0 = device.input[DI].param_2;
}

void init_active_count(uint8_t DI) // mode
{
    if(~device.input[DI].param_1 & 2)
    {
        device.input[DI].param_2 = 0;
    }
    device.input[DI].val_0 = device.input[DI].param_2;
}

void timer_7_init(void)
{
    if(htim7.State == HAL_TIM_STATE_READY)
        return;
    HAL_NVIC_SetPriority(TIM7_IRQn, 4, 0);
    /* Enable the TIM7 global Interrupt */
    HAL_NVIC_EnableIRQ(TIM7_IRQn);

    __HAL_RCC_TIM7_CLK_ENABLE();
    htim7.Instance = TIM7;
    htim7.Init.Period = TIM7_PERIOD;

    htim7.Init.Prescaler = TIM_PSC;
    htim7.Init.ClockDivision = 0;
    htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
    if(HAL_TIM_Base_Init(&htim7) == HAL_OK)
    {
        HAL_TIM_Base_Start_IT(&htim7);
    }
}

void init_rate_measure_soft(uint8_t DI)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull = ((device.input[DI].param_1 & MBD_FRONT_BIT) == 0) ? GPIO_PULLDOWN : GPIO_PULLUP;
    // GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;

    // better rewrtite this with switch and without <<
    if(DI == 0 || DI == 1)
    {
        __HAL_RCC_GPIOA_CLK_ENABLE(); /* (1) */
        GPIO_InitStruct.Pin = 1 << DI;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    }
    else if(DI == 2)
    {
        __HAL_RCC_GPIOC_CLK_ENABLE(); /* (1) */
        GPIO_InitStruct.Pin = DI2_PIN;
        HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    }
    else if(DI == 3)
    {
        __HAL_RCC_GPIOC_CLK_ENABLE(); /* (1) */
        GPIO_InitStruct.Pin = DI3_PIN;
        HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    }
    else if(DI > 3) // 4 5 6 7
    {
        __HAL_RCC_GPIOA_CLK_ENABLE(); /* (1) */
        GPIO_InitStruct.Pin = 1 << (DI + 4);
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    }

    // NVIC init
    if(DI < 2){ // 0 - 1
        NVIC_EnableIRQ(EXTI0_1_IRQn);
        NVIC_SetPriority(EXTI0_1_IRQn, 4);
    }else{ // 6 - 11
        NVIC_EnableIRQ(EXTI4_15_IRQn);
        NVIC_SetPriority(EXTI4_15_IRQn, 4);
    }
    timer_7_init();
    device.input[DI].counter = 0;
    device.input[DI].last_capture = 0;
}

int8_t init_rate_measure_hard(uint8_t DI) // input capture
{
    // uint32_t uwTimclock = 48000000;
    /* Compute the prescaler value to have TIM2 counter clock equal to 1MHz */
    // uint32_t uwPrescalerValue = (uint32_t) ((uwTimclock / 1000000) - 1);
    // TIM2
    if((DI == 0)
    || (DI == 1))
    {
        memset(TIM2, 0, sizeof(*TIM2)); // без полной очистки не переинициализируется CCMR1

        // Нужно проверять в каком режиме настроены соседние входы
        // и перенастраивать с учетом уже сконфиганых входов
        __HAL_RCC_GPIOA_CLK_ENABLE();
        GPIO_InitTypeDef GPIO_InitStruct = {0};
        GPIO_InitStruct.Pin = 1 << DI;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
        GPIO_InitStruct.Pull = ((device.input[DI].param_1 & MBD_FRONT_BIT) == 0) ? GPIO_PULLDOWN : GPIO_PULLUP;
        // GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF2_TIM2;
        HAL_GPIO_DeInit(GPIOA, GPIO_InitStruct.Pin);
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        __HAL_RCC_TIM2_CLK_ENABLE();
        htim2.Instance = TIM2;
        htim2.Init.Period = 0xFFFF;
        htim2.Init.Prescaler = TIM_PSC; // ставим самый мелкий делитель
        htim2.Init.ClockDivision = 0;
        htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
        if(HAL_TIM_Base_Init(&htim2) != HAL_OK)
            return -1;

        // (1) Select the active input TI1 (CC1S = 01),
        // program the input filter for 8 clock cycles (IC1F = 0011),
        // select the rising edge on CC1 (CC1P = 0, reset value)
        // and prescaler at each valid transition (IC1PS = 00, reset value)
        // (2) Enable capture by setting CC1E
        // (3) Enable interrupt on Capture/Compare
        // (4) Enable counter

        // clear old flags
        TIM2->DIER &= ~(TIM_DIER_CC1IE | TIM_DIER_CC2IE | TIM_DIER_CC3IE | TIM_DIER_CC1DE | TIM_DIER_CC2DE | TIM_DIER_UIE);
        TIM2->SMCR &= ~(TIM_SMCR_TS | TIM_SMCR_SMS);
        TIM2->CCER &= ~TIM_CCER_CC3E;

        if(device.input[0].mode == RATE_MEASURE_HARD || DI == 0)
        {
            TIM2->CCMR1 &= ~(TIM_CCMR1_CC1S | TIM_CCMR1_CC2S | TIM_CCMR1_IC1F);
            TIM2->CCMR1 |= TIM_CCMR1_CC1S_0 | TIM_CCMR1_IC1F_0 | TIM_CCMR1_IC1F_1; // 1
            TIM2->CCER |= TIM_CCER_CC1E; // 2
            if(device.input[0].param_1 & MBD_FALLING_FRONT_BIT)
                TIM2->CCER |= TIM_CCER_CC1P;
            else
                TIM2->CCER &= ~TIM_CCER_CC1P;
            TIM2->DIER |= TIM_DIER_CC1IE; // 3
        }
        if(device.input[1].mode == RATE_MEASURE_HARD || DI == 1)
        {
            TIM2->CCMR1 &= ~(TIM_CCMR1_CC1S | TIM_CCMR1_CC2S | TIM_CCMR1_IC1F);
            TIM2->CCMR1 |= TIM_CCMR1_CC2S_0 | TIM_CCMR1_IC2F_0 | TIM_CCMR1_IC2F_1;
            TIM2->CCER |= TIM_CCER_CC2E;
            if(device.input[1].param_1 & MBD_FALLING_FRONT_BIT)
                TIM2->CCER |= TIM_CCER_CC2P;
            else
                TIM2->CCER &= ~TIM_CCER_CC2P;
            TIM2->DIER |= TIM_DIER_CC2IE;
        }

        // в прерывании будет инкремент counter'a
        HAL_TIM_Base_Start_IT(&htim2);
        HAL_NVIC_SetPriority(TIM2_IRQn, 4, 0);
        HAL_NVIC_EnableIRQ(TIM2_IRQn);
    }

    // TIM3
    if((DI == 2)
    || (DI == 3))
    {
        // Нужно проверять в каком режиме настроены соседние входы
        // и перенастраивать с учетом уже сконфиганых входов
        __HAL_RCC_GPIOC_CLK_ENABLE();
        GPIO_InitTypeDef GPIO_InitStruct = {0};
        GPIO_InitStruct.Pin = (DI == 2) ? DI2_PIN : DI3_PIN;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
        GPIO_InitStruct.Pull = ((device.input[DI].param_1 & MBD_FRONT_BIT) == 0) ? GPIO_PULLDOWN : GPIO_PULLUP;
        // GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF0_TIM3;
        HAL_GPIO_DeInit(GPIOC, GPIO_InitStruct.Pin);
        HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

        __HAL_RCC_TIM3_CLK_ENABLE();
        htim3.Instance = TIM3;
        htim3.Init.Period = 0xFFFF;
        htim3.Init.Prescaler = TIM_PSC;
        htim3.Init.ClockDivision = 0;
        htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
        if(HAL_TIM_Base_Init(&htim3) != HAL_OK)
            return -1;

        // (1) Select the active input TI1 (CC1S = 01),
        // program the input filter for 8 clock cycles (IC1F = 0011),
        // select the rising edge on CC1 (CC1P = 0, reset value)
        // and prescaler at each valid transition (IC1PS = 00, reset value)
        // (2) Enable capture by setting CC1E
        // (3) Enable interrupt on Capture/Compare
        // (4) Enable counter
        if(device.input[3].mode == RATE_MEASURE_HARD || DI == 3)
        {
            TIM3->CCMR1 |= TIM_CCMR1_CC1S_0 | TIM_CCMR1_IC1F_0 | TIM_CCMR1_IC1F_1; // 1
            TIM3->CCER  |= TIM_CCER_CC1E; // 2
            if(device.input[2].param_1 & MBD_FALLING_FRONT_BIT)
                TIM3->CCER |= TIM_CCER_CC1P;
            else
                TIM3->CCER &= ~TIM_CCER_CC1P;
            TIM3->DIER  |= TIM_DIER_CC1IE; // 3
        }
        if(device.input[2].mode == RATE_MEASURE_HARD || DI == 2)
        {
            TIM3->CCMR1 |= TIM_CCMR1_CC2S_0 | TIM_CCMR1_IC2F_0 | TIM_CCMR1_IC2F_1;
            TIM3->CCER  |= TIM_CCER_CC2E;
            if(device.input[2].param_1 & MBD_FALLING_FRONT_BIT)
                TIM3->CCER |= TIM_CCER_CC2P;
            else
                TIM3->CCER &= ~TIM_CCER_CC2P;
            TIM3->DIER  |= TIM_DIER_CC2IE;
        }
        // в прерывании будет инкремент counter'a
        HAL_TIM_Base_Start_IT(&htim3);
        HAL_NVIC_SetPriority(TIM3_IRQn, 4, 0);
        HAL_NVIC_EnableIRQ(TIM3_IRQn);
    }

    // TIM1
    if((DI == 4)
    || (DI == 5)
    || (DI == 6)
    || (DI == 7))
    {
        memset(TIM1, 0, sizeof(*TIM1)); // без полной очистки не переинициализируется CCMR1

        __HAL_RCC_GPIOA_CLK_ENABLE();

        GPIO_InitTypeDef GPIO_InitStruct = {0};
        GPIO_InitStruct.Pin = 1 << (DI + 4);
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = ((device.input[DI].param_1 & MBD_FRONT_BIT) == 0) ? GPIO_PULLDOWN : GPIO_PULLUP;
        // GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF2_TIM1;
        HAL_GPIO_DeInit(GPIOA, GPIO_InitStruct.Pin);
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        __HAL_RCC_TIM1_CLK_ENABLE();
        htim1.Instance = TIM1;
        htim1.Init.Period = 0xFFFF;
        htim1.Init.Prescaler = TIM_PSC;
        htim1.Init.ClockDivision = 0;
        htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
        if(HAL_TIM_Base_Init(&htim1) != HAL_OK)
            return -1;
        // clear old flags
        TIM1->DIER &= ~(TIM_DIER_CC1IE |TIM_DIER_CC2IE | TIM_DIER_CC3IE | TIM_DIER_CC4IE | TIM_DIER_CC1DE | TIM_DIER_CC2DE | TIM_DIER_UIE);
        TIM1->SMCR &= ~(TIM_SMCR_TS | TIM_SMCR_SMS);
        TIM1->CCMR1 &= ~(TIM_CCMR1_CC1S | TIM_CCMR1_CC2S);
        TIM1->CCMR2 &= ~(TIM_CCMR2_CC3S | TIM_CCMR2_CC4S);

        if(device.input[4].mode == RATE_MEASURE_HARD || DI == 4)
        {
            TIM1->CCMR1 |= TIM_CCMR1_CC1S_0 | TIM_CCMR1_IC1F_0 | TIM_CCMR1_IC1F_1; // 1
            TIM1->CCER  |= TIM_CCER_CC1E;   // 2
            if(device.input[4].param_1 & MBD_FALLING_FRONT_BIT)
                TIM1->CCER |= TIM_CCER_CC1P;
            else
                TIM1->CCER &= ~TIM_CCER_CC1P; // важно при перенастройке
            TIM1->DIER  |= TIM_DIER_CC1IE;  // 3
        }
        if(device.input[5].mode == RATE_MEASURE_HARD || DI == 5)
        {
            TIM1->CCMR1 |= TIM_CCMR1_CC2S_0 | TIM_CCMR1_IC2F_0 | TIM_CCMR1_IC2F_1; // 1
            TIM1->CCER  |= TIM_CCER_CC2E;   // 2
            if(device.input[5].param_1 & MBD_FALLING_FRONT_BIT)
                TIM1->CCER |= TIM_CCER_CC2P;
            else
                TIM1->CCER &= ~TIM_CCER_CC2P;
            TIM1->DIER  |= TIM_DIER_CC2IE; // 3
        }
        if(device.input[6].mode == RATE_MEASURE_HARD || DI == 6)
        {
            TIM1->CCMR2 |= TIM_CCMR2_CC3S_0 | TIM_CCMR2_IC3F_0 | TIM_CCMR2_IC3F_1; // 1
            TIM1->CCER  |= TIM_CCER_CC3E;   // 2
            if(device.input[6].param_1 & MBD_FALLING_FRONT_BIT)
                TIM1->CCER |= TIM_CCER_CC3P;
            else
                TIM1->CCER &= ~TIM_CCER_CC3P;
            TIM1->DIER  |= TIM_DIER_CC3IE;  // 3
        }
        if(device.input[7].mode == RATE_MEASURE_HARD || DI == 7)
        {
            TIM1->CCMR2 |= TIM_CCMR2_CC4S_0 | TIM_CCMR2_IC4F_0 | TIM_CCMR2_IC4F_1; // 1
            TIM1->CCER  |= TIM_CCER_CC4E;   // 2
            if(device.input[7].param_1 & MBD_FALLING_FRONT_BIT)
                TIM1->CCER |= TIM_CCER_CC4P;
            else
                TIM1->CCER &= ~TIM_CCER_CC4P;
            TIM1->DIER  |= TIM_DIER_CC4IE;  // 3
        }
        // в прерывании будет инкремент counter'a
        HAL_TIM_Base_Start_IT(&htim1);
        HAL_NVIC_SetPriority(TIM1_BRK_UP_TRG_COM_IRQn, 4, 0);
        HAL_NVIC_EnableIRQ(TIM1_BRK_UP_TRG_COM_IRQn);
        HAL_NVIC_SetPriority(TIM1_CC_IRQn, 4, 0);
        HAL_NVIC_EnableIRQ(TIM1_CC_IRQn);
    }
    device.input[DI].counter = 0;
    device.input[DI].last_capture = 0;
    return 0;
}

int8_t init_impulse_measure(uint8_t DI)
{
#ifdef _UNITTEST_
    return 0;
#endif
    bool is_switch_polarity = device.input[DI].param_1 & 1;
    // TIM1
    if((DI == 4)
    || (DI == 5))
    {
        __HAL_RCC_TIM1_CLK_ENABLE();
        __HAL_RCC_GPIOA_CLK_ENABLE();

        memset(TIM1, 0, sizeof(*TIM1)); // без полной очистки не переинициализируется CCMR1
        htim1.Instance = TIM1;
        htim1.Init.Period = 0xFFFF;
        htim1.Init.Prescaler = TIM_PSC;
        htim1.Init.ClockDivision = 0;
        htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
        if(HAL_TIM_Base_Init(&htim1) != HAL_OK)
            return -1;

        GPIO_InitTypeDef GPIO_InitStruct = {0};
        GPIO_InitStruct.Pin = (DI == 4) ? GPIO_PIN_8 : GPIO_PIN_9;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = ((device.input[DI].param_1 & MBD_FRONT_BIT) == 0) ? GPIO_PULLDOWN : GPIO_PULLUP;
        // GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
        GPIO_InitStruct.Alternate = GPIO_AF2_TIM1;
        HAL_GPIO_DeInit(GPIOA, GPIO_InitStruct.Pin);
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        /* (1) Select the active input TI1 for TIMx_CCR1 (CC1S = 01),
        select the active input TI1 for TIMx_CCR2 (CC2S = 10) */
        /* (2) Select TI1FP1 as valid trigger input (TS = 101)
        configure the slave mode in reset mode (SMS = 100) */
        /* (3) Enable capture by setting CC1E and CC2E
        select the rising edge on CC1 and CC1N (CC1P = 0 and CC1NP = 0, reset
        value),
        select the falling edge on CC2 (CC2P = 1). */
        /* (4) Enable interrupt on Capture/Compare 1, DMA interrupt enable*/
        /* (5) Enable counter */
        TIM1->CR1 &= ~TIM_CR1_CEN;
        TIM1->SMCR &= ~(TIM_SMCR_TS | TIM_SMCR_SMS);
        TIM1->CCMR1 &= ~(TIM_CCMR1_CC1S | TIM_CCMR1_CC2S);
        TIM1->CCMR2 &= ~(TIM_CCMR2_OC3M | TIM_CCMR2_CC3S);
        TIM1->CCER &= ~(TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC1P | TIM_CCER_CC2P | TIM_CCER_CC3E);
        TIM1->DIER &= ~(TIM_DIER_CC1IE | TIM_DIER_CC2IE | TIM_DIER_CC3IE | TIM_DIER_UIE);

        if(DI == 4)
        {
            TIM1->CCMR1 |= TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_1;             /* (1)*/
            TIM1->SMCR |= TIM_SMCR_TS_2 | TIM_SMCR_TS_0 | TIM_SMCR_SMS_2;   /* (2) */ // TI1FP1
            // TIM1->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC2P;    /* (3) */
            if(is_switch_polarity)
                TIM1->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC1P;    /* (3) */
            else
                TIM1->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC2P;    /* (3) */
        }else{   // DI 5
            TIM1->CCMR1 |= TIM_CCMR1_CC1S_1 | TIM_CCMR1_CC2S_0;             /* (1)*/
            TIM1->SMCR |= TIM_SMCR_TS_2 | TIM_SMCR_TS_1 | TIM_SMCR_SMS_2;   /* (2) */ // TI2FP2
            // TIM1->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC1P;    /* (3) */
            if(is_switch_polarity)
                TIM1->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC2P;    /* (3) */
            else
                TIM1->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC1P;    /* (3) */
        }

        // таймаут для измерения, если сработал таймаут - сбрасываем значения
        TIM1->DIER |= TIM_DIER_CC1IE | TIM_DIER_CC2IE | TIM_DIER_CC3IE;
        TIM1->CCR3 = PWM_INPUT_TIMEOUT; /* timeout value */
        TIM1->CCER |= TIM_CCER_CC3E;
        TIM1->CR1 |= TIM_CR1_CEN;       /* (5) */

        HAL_NVIC_SetPriority(TIM1_CC_IRQn, 4, 0);
        HAL_NVIC_EnableIRQ(TIM1_CC_IRQn);
        return 0;
    }

    // TIM2
    if((DI == 0)
    || (DI == 1))
    {
        __HAL_RCC_TIM2_CLK_ENABLE();
        __HAL_RCC_GPIOA_CLK_ENABLE();

        memset(TIM2, 0, sizeof(*TIM2)); // без полной очистки не переинициализируется CCMR1

        htim2.Instance = TIM2;
        htim2.Init.Period = 0xFFFF;
        htim2.Init.Prescaler = TIM_PSC;
        htim2.Init.ClockDivision = 0;
        htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
        if(HAL_TIM_Base_Init(&htim2) != HAL_OK)
            return -1;

        GPIO_InitTypeDef GPIO_InitStruct = {0};
        GPIO_InitStruct.Pin = (DI == 0) ? GPIO_PIN_0 : GPIO_PIN_1;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = ((device.input[DI].param_1 & MBD_FRONT_BIT) == 0) ? GPIO_PULLDOWN : GPIO_PULLUP;
        // GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
        GPIO_InitStruct.Alternate = GPIO_AF2_TIM2;
        HAL_GPIO_DeInit(GPIOA, GPIO_InitStruct.Pin);
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        /* (1) Select the active input TI1 for TIMx_CCR1 (CC1S = 01),
        select the active input TI1 for TIMx_CCR2 (CC2S = 10) */
        /* (2) Select TI1FP1 as valid trigger input (TS = 101)
        configure the slave mode in reset mode (SMS = 100) */
        /* (3) Enable capture by setting CC1E and CC2E
        select the rising edge on CC1 and CC1N (CC1P = 0 and CC1NP = 0, reset
        value),
        select the falling edge on CC2 (CC2P = 1). */
        /* (4) Enable interrupt on Capture/Compare 1, DMA interrupt enable*/
        /* (5) Enable counter */
        TIM2->CR1 &= ~TIM_CR1_CEN;
        TIM2->CCMR1 &= ~(TIM_CCMR1_CC1S | TIM_CCMR1_CC2S);
        TIM2->CCMR2 &= ~(TIM_CCMR2_OC3M | TIM_CCMR2_CC3S);
        TIM2->SMCR &= ~(TIM_SMCR_TS | TIM_SMCR_SMS);
        TIM2->CCER &= ~(TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC1P | TIM_CCER_CC2P | TIM_CCER_CC3E);
        TIM2->DIER &= ~(TIM_DIER_CC1IE | TIM_DIER_CC2IE | TIM_DIER_CC3IE | TIM_DIER_UIE);
        if(DI == 0)
        {
            TIM2->CCMR1 |= TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_1;             /* (1)*/
            TIM2->SMCR |= TIM_SMCR_TS_2 | TIM_SMCR_TS_0 | TIM_SMCR_SMS_2;   /* (2) */
            if(is_switch_polarity)
                TIM2->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC1P;    /* (3) */
            else
                TIM2->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC2P;    /* (3) */
        }else{ // DI1
            TIM2->CCMR1 |= TIM_CCMR1_CC1S_1 | TIM_CCMR1_CC2S_0;          /* (1)*/
            TIM2->SMCR |= TIM_SMCR_TS_2 | TIM_SMCR_TS_1 | TIM_SMCR_SMS_2;   /* (2) */
            if(is_switch_polarity)
                TIM2->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC2P;    /* (3) */
            else
                TIM2->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC1P;    /* (3) */
        }

        // таймаут для измерения, если сработал таймаут - сбрасываем значения
        TIM2->DIER |= TIM_DIER_CC1IE | TIM_DIER_CC2IE | TIM_DIER_CC3IE;
        TIM2->CCR3 = PWM_INPUT_TIMEOUT; // timeout value
        TIM2->CCER |= TIM_CCER_CC3E;
        TIM2->CR1 |= TIM_CR1_CEN;       /* (5) */

        HAL_NVIC_SetPriority(TIM2_IRQn, 4, 0);
        HAL_NVIC_EnableIRQ(TIM2_IRQn);

        return 0;
    }
    return -1;
}

int8_t check_init_posibility(uint8_t DI, uint8_t mode)
{
    if(mode == RATE_MEASURE_HARD)
    {
        switch(DI)
        {   // TIM2
            case 0:
                if(device.input[1].mode == IMPULSE_MEASURE) // так как режим 6 использует CC1 для таймаута
                    return -1;
                break;
            case 1:
                if(device.input[0].mode == IMPULSE_MEASURE) // так как режим 6 использует CC1 для таймаута
                    return -1;
                break;
            // TIM1
            case 4:
                if(device.input[5].mode == IMPULSE_MEASURE) // так как режим 6 использует CC1 для таймаута
                    return -1;
                break;
            case 5:
                if(device.input[4].mode == IMPULSE_MEASURE) // так как режим 6 использует CC1 для таймаута
                    return -1;
                break;
            case 6:
                if((device.input[4].mode == IMPULSE_MEASURE) // так как режим 6 использует CC1 для таймаута
                || (device.input[5].mode == IMPULSE_MEASURE))
                    return -1;
                break;
            case 7:
                if((device.input[4].mode == IMPULSE_MEASURE)
                || (device.input[5].mode == IMPULSE_MEASURE))
                    return -1;
                break;
            case 2:
            case 3:
                return 0;
            default:
                return -1;
        }
    }
    else if(mode == IMPULSE_MEASURE)
    {
        switch(DI)
        {   // TIM3
            case 0:
                if(device.input[1].mode >= RATE_MEASURE_HARD)
                    return -1;
                break;
            case 1:
                if(device.input[0].mode >= RATE_MEASURE_HARD)
                    return -1;
                break;
            // TIM1
            case 4:
                if((device.input[5].mode >= RATE_MEASURE_HARD)
                || (device.input[6].mode >= RATE_MEASURE_HARD)
                || (device.input[7].mode >= RATE_MEASURE_HARD))
                    return -1;
                break;
            case 5:
                if((device.input[4].mode >= RATE_MEASURE_HARD)
                || (device.input[6].mode >= RATE_MEASURE_HARD)
                || (device.input[7].mode >= RATE_MEASURE_HARD))
                    return -1;
                break;
            default:
                return -1;
        }
    }
    return 0;
}

// channel = DI
int8_t input_configure(uint8_t DI, int8_t new_mode)
{
    uint8_t mode = device.input[DI].mode;
    if(new_mode != -1)
    {
        mode = new_mode;
    }
    // выполнить инициализацию в соответствии с новыми параметрами
    switch(mode)
    {
        case COMPARATOR: // 0 в этом режиме не происходит обработки, чтение из регистра
            init_input(DI); // clear input
            init_non_alternate_pin(DI);
            break;
        case FLUTTER_REDUCTION: // 1 устранение дребезга
            init_non_alternate_pin(DI);
            init_flutter_reduction(DI);
            break;
        case IMPULSE_COUNT: // 2
            init_non_alternate_pin(DI);
            init_flutter_reduction(DI);
            // счет импульсов низкочастотный, обработка после подавления дребезга
            init_impulse_count(DI);
            break;
        case ACTIVE_COUNT: // 3
            init_non_alternate_pin(DI);
            init_active_count(DI);
            break;
        case RATE_MEASURE_SOFT: // 4 здесь предполагается, что сигнал не дребезжащий, вход таймера
            init_rate_measure_soft(DI);
            break;
        case RATE_MEASURE_HARD: // 5 здесь предполагается, что сигнал не дребезжащий, вход таймера
            if(check_init_posibility(DI, new_mode)) // проверка если один из входов аппаратных таймеров настроен на режим 6
            {
                ++device.err_param_rq;
                return CANNOT_DOIT;
            }
            init_rate_measure_hard(DI);
            break;
        case IMPULSE_MEASURE: // 6 не дребезжащий, вход таймера
            if(check_init_posibility(DI, new_mode))
            {
                ++device.err_param_rq;
                return CANNOT_DOIT;
            }
            init_impulse_measure(DI);
            break;
        default:
            return REQUEST_ERR;
    }
    device.input[DI].mode = mode;
    device.input[DI].new_mode = -1;
    return 0;
}

// oooo     oooo oooooooooo  ooooo ooooooooooo ooooooooooo      oooooooooo   o      oooooooooo       o      oooo     oooo
//  88   88  88   888    888  888  88  888  88  888    88        888    888 888      888    888     888      8888o   888
//   88 888 88    888oooo88   888      888      888ooo8          888oooo88 8  88     888oooo88     8  88     88 888o8 88
//    888 888     888  88o    888      888      888    oo        888      8oooo88    888  88o     8oooo88    88  888  88
//     8   8     o888o  88o8 o888o    o888o    o888ooo8888      o888o   o88o  o888o o888o  88o8 o88o  o888o o88o  8  o88o

int8_t mbd_write_reg(uint16_t reg, uint16_t value)
{
    uint16_t update_ext_mem = 0;
    switch(reg)
    {
        // маски
        case AND_MASK_REGISTER:
            device.and_mask = value;
            update_ext_mem |= (1 << 8);
            break;
        case OR_MASK_REGISTER:
            device.or_mask = value;
            update_ext_mem |= (1 << 8);
            break;
        // параметры входов
        case DI0_MODE_REG:
            if(value <= IMPULSE_MEASURE)
            {
                device.input[0].new_mode = value;
                update_ext_mem |= (1 << 0);
                break;
            }else
                return REQUEST_ERR;
        case PARAM1_REG_0:
            device.input[0].param_1 = value;
            update_ext_mem |= (1 << 0);
            break;
        case PARAM2_1_REG_0:
            device.input[0].param_2 &= ~0xFFFFFFFF;
            device.input[0].param_2 |= value << 16;
            update_ext_mem |= (1 << 0);
            break;
        case PARAM2_2_REG_0:
            device.input[0].param_2 &= ~0xFFFF;
            device.input[0].param_2 |= value;
            update_ext_mem |= (1 << 0);
            break;

        case DI1_MODE_REG:
            if(value <= IMPULSE_MEASURE)
            {
                device.input[1].new_mode = value;
                update_ext_mem |= (1 << 1);
                break;
            }else
                return REQUEST_ERR;
        case PARAM1_REG_1:
            device.input[1].param_1 = value;
            update_ext_mem |= (1 << 1);
            break;
        case PARAM2_1_REG_1:
            device.input[1].param_2 &= ~0xFFFFFFFF;
            device.input[1].param_2 |= value << 16;
            update_ext_mem |= (1 << 1);
            break;
        case PARAM2_2_REG_1:
            device.input[1].param_2 &= ~0xFFFF;
            device.input[1].param_2 |= value;
            update_ext_mem |= (1 << 1);
            break;

        case DI2_MODE_REG:
            if(value <= IMPULSE_MEASURE)
            {
                device.input[2].new_mode = value;
                update_ext_mem |= (1 << 2);
                break;
            }else
                return REQUEST_ERR;
        case PARAM1_REG_2:
            device.input[2].param_1 = value;
            update_ext_mem |= (1 << 2);
            break;
        case PARAM2_1_REG_2:
            device.input[2].param_2 &= ~0xFFFFFFFF;
            device.input[2].param_2 |= value << 16;
            update_ext_mem |= (1 << 2);
            break;
        case PARAM2_2_REG_2:
            device.input[2].param_2 &= ~0xFFFF;
            device.input[2].param_2 |= value;
            update_ext_mem |= (1 << 2);
            break;

        case DI3_MODE_REG:
            if(value <= IMPULSE_MEASURE)
            {
                device.input[3].new_mode = value;
                update_ext_mem |= (1 << 3);
                break;
            }else
                return REQUEST_ERR;
        case PARAM1_REG_3:
            device.input[3].param_1 = value;
            update_ext_mem |= (1 << 3);
            break;
        case PARAM2_1_REG_3:
            device.input[3].param_2 &= ~0xFFFFFFFF;
            device.input[3].param_2 |= value << 16;
            update_ext_mem |= (1 << 3);
            break;
        case PARAM2_2_REG_3:
            device.input[3].param_2 &= ~0xFFFF;
            device.input[3].param_2 |= value;
            update_ext_mem |= (1 << 3);
            break;

        case DI4_MODE_REG:
            if(value <= IMPULSE_MEASURE)
            {
                device.input[4].new_mode = value;
                update_ext_mem |= (1 << 4);
                break;
            }else
                return REQUEST_ERR;
        case PARAM1_REG_4:
            device.input[4].param_1 = value;
            update_ext_mem |= (1 << 4);
            break;
        case PARAM2_1_REG_4:
            device.input[4].param_2 &= ~0xFFFFFFFF;
            device.input[4].param_2 |= value << 16;
            update_ext_mem |= (1 << 4);
            break;
        case PARAM2_2_REG_4:
            device.input[4].param_2 &= ~0xFFFF;
            device.input[4].param_2 |= value;
            update_ext_mem |= (1 << 4);
            break;

        case DI5_MODE_REG:
            if(value <= IMPULSE_MEASURE)
            {
                device.input[5].new_mode = value;
                update_ext_mem |= (1 << 5);
                break;
            }else
                return REQUEST_ERR;
        case PARAM1_REG_5:
            device.input[5].param_1 = value;
            update_ext_mem |= (1 << 5);
            break;
        case PARAM2_1_REG_5:
            device.input[5].param_2 &= ~0xFFFFFFFF;
            device.input[5].param_2 |= value << 16;
            update_ext_mem |= (1 << 5);
            break;
        case PARAM2_2_REG_5:
            device.input[5].param_2 &= ~0xFFFF;
            device.input[5].param_2 |= value;
            update_ext_mem |= (1 << 5);
            break;

        case DI6_MODE_REG:
            if(value <= IMPULSE_MEASURE)
            {
                device.input[6].new_mode = value;
                update_ext_mem |= (1 << 6);
                break;
            }else
                return REQUEST_ERR;
        case PARAM1_REG_6:
            device.input[6].param_1 = value;
            update_ext_mem |= (1 << 6);
            break;
        case PARAM2_1_REG_6:
            device.input[6].param_2 &= ~0xFFFFFFFF;
            device.input[6].param_2 |= value << 16;
            update_ext_mem |= (1 << 6);
            break;
        case PARAM2_2_REG_6:
            device.input[6].param_2 &= ~0xFFFF;
            device.input[6].param_2 |= value;
            update_ext_mem |= (1 << 6);
            break;

        case DI7_MODE_REG:
            if(value <= IMPULSE_MEASURE)
            {
                device.input[7].new_mode = value;
                update_ext_mem |= (1 << 7);
                break;
            }else
                return REQUEST_ERR;
        case PARAM1_REG_7:
            device.input[7].param_1 = value;
            update_ext_mem |= (1 << 7);
            break;
        case PARAM2_1_REG_7:
            device.input[7].param_2 &= ~0xFFFFFFFF;
            device.input[7].param_2 |= value << 16;
            update_ext_mem |= (1 << 7);
            break;
        case PARAM2_2_REG_7:
            device.input[7].param_2 &= ~0xFFFF;
            device.input[7].param_2 |= value;
            update_ext_mem |= (1 << 7);
            break;
            
        case SPARE_INIT:
            set_spare_output(value);
            break;
        case RS485_ADDRESS_REG:
            if(device.is_init)
            {
                device.addr = value;
                update_ext_mem |= (1 << 8);
            }
            break;
        case RS485_BAUDRATE_HREG:
            if(device.is_init)
            {
                device.baudrate &= ~(0xFFFF << 16);
                device.baudrate |= value << 16;
                // no update here
            }
            break;
        case RS485_BAUDRATE_LREG:
            if(device.is_init)
            {
                device.baudrate &= ~0xFFFF;
                device.baudrate |= value;
                update_ext_mem |= (1 << 8);
            }
            break;
        default:
            return DATA_ADDR_ERR;
    }
    if(update_ext_mem)
    {
        for(uint8_t i = 0; i < 8; ++i)
        {
            if(update_ext_mem & (1 << i))
            {
                if(input_configure(i, device.input[i].new_mode))
                {
                    return REQUEST_ERR;
                }
                break;
            }
        }
        int8_t ret = ext_mem_store();
        if(ret)
        {
            ERROR(EXT_MEM_WRITE_ERROR);
            return FATAL_ERR;
        }
    }
    return 0;
}

void mbd_inc_crc_err_rq(void)
{
    ++device.err_crc_rq;
}

void mbd_inc_handled_rq(void)
{
    ++device.handled_rq;
}

void mbd_inc_total_rq(void)
{
    ++device.total_rq;
}

void mdb_inc_send_err(void)
{
    ++device.err_send;
}

// return 0 если не произошло переключения
// return 1 если произошло переключение с 0 на 1
// return -1 если произошло переключение с 1 на 0

int8_t process_flutter_reduction(uint8_t DI)
{
    // текущее состояние
    // положение выхода антидребезга лежит во флагах!!! - для комбинирования режимов
    bool filter_out = (device.input[DI].flags & 1) ? 1 : 0;
    update_masked();

    bool filter_input = (device.masked_input & (1 << DI)) ? 1 : 0;
    if(filter_input == filter_out) // если значение совпадает с текущим
    {
        // декремент счетчика
        if(device.input[DI].counter > 0)
        {
            --device.input[DI].counter;
        }
    }else{
        if(device.input[DI].counter < 0x3F) // проверка на переполнение 6 bit value
            ++device.input[DI].counter;
        if(filter_input)
        { // здесь копим единицы
            uint16_t upper_threshold = (device.input[DI].param_1 >> 9) & 0x3F;
            if(device.input[DI].counter > upper_threshold)
            {
                device.input[DI].flags |= 1;
                device.input[DI].counter = 0;
                return 1;
            }
        }else{
            uint16_t lower_threshold = (device.input[DI].param_1 >> 3) & 0x3F;
            if(device.input[DI].counter > lower_threshold)
            {
                device.input[DI].flags &= ~1;
                device.input[DI].counter = 0;
                return -1;
            }
        }
    }

    return 0;
}

void fill_comparator_registers(uint8_t DI)
{
    device.input[DI].val_0 = (device.input_reg & (1 << DI)) ? 1 : 0;
    ++device.input[DI].result_id;
}

void fill_flutter_registers(uint8_t DI)
{
    device.input[DI].val_0 = device.input[DI].flags;
    ++device.input[DI].result_id;
}

// called on front changed
void process_impulse_count(uint8_t DI)
{
    if((device.input[DI].flags & 1) == (~device.input[DI].param_1 & 1)) // фронт совпадает с флагом
    {
        device.input[DI].val_0 = ++device.input[DI].param_2;
        if(device.input[DI].val_0 == 0)
            device.input[DI].flags |= 1 << 15; // oveflow flag
    }
    ++device.input[DI].result_id;

    if(device.input[DI].param_1 & 2) // сброс флага overflow
    {
        device.input[DI].flags &= ~(1 << 15);
        device.input[DI].param_1 &= ~(1 << 1);
    }
}

void process_active_count(uint8_t DI)
{
    uint32_t ticks = device.operation_time;
    update_masked();

    if(!device.input[DI].last_capture)
        device.input[DI].last_capture = ticks;

    if(((device.masked_input >> DI) & 1) == (device.input[DI].param_1 & 1))
    {
        uint32_t old_val = device.input[DI].val_0;
        device.input[DI].param_2 += (ticks - device.input[DI].last_capture);
        device.input[DI].val_0 = device.input[DI].param_2;
        if(device.input[DI].val_0 < old_val)
            device.input[DI].flags |= 1 << 15; // oveflow flag
    }
    device.input[DI].last_capture = ticks;

    if(device.input[DI].param_1 & 2) // сброс флага overflow
    {
        device.input[DI].flags &= ~(1 << 15);
        device.input[DI].param_1 &= ~(1 << 1);
    }
}

void reset_input(uint8_t di)
{
    device.input[di].val_0 = 0;
    device.input[di].val_1 = 0;
    device.input[di].counter = 0;
    device.input[di].duty_counter = 0;
    // log_info("reset input %d\n", di);
}

void sort(int32_t list[], uint32_t n)
{
    int32_t t;
    uint32_t c, d;

    for (c = 0 ; c < (n - 1); c++)
    {
        for (d = 0 ; d < n - c - 1; d++)
        {
            if (list[d] > list[d+1])
            {
                t         = list[d];
                list[d]   = list[d+1];
                list[d+1] = t;
            }
        }
    }
}

uint32_t get_sample_average(uint8_t DI)
{
    int32_t values[NUM_SAMPLES];
    memcpy(values, device.input[DI].ic_period, sizeof(values));
    sort(values, NUM_SAMPLES);
    int32_t ic_period = (values[1] + values[2]) / 2;

    if(ic_period > 0)
    {
        uint64_t value = TIM_CLOCK * 100 / ((TIM_PSC + 1) * ic_period);
        //     // проверка валидности
        //     uint32_t last_value = device.input[DI].val_0;
        //     if(!is_valid(DI, (uint32_t)value / 100, last_value / 100))
        //     {
        //         device.input[DI].flags |= 1; // if not valid set 1
        //     }else{
        //         device.input[DI].flags &= ~1; // if valid set 0
        //     }
        return (uint32_t)(value & 0xFFFFFFFF);
    }else{
        device.input[DI].flags |= 1; // not valid
    }
    return 0;
}

void process_rate_measure_soft(uint8_t DI)
{
    (void)DI;
}

void process_rate_measure_hard(uint8_t DI)
{
    device.input[DI].val_0 = get_sample_average(DI);
}

void process_impulse_measure(uint8_t DI)
{
    // DMA кладет размер периода в rate_meter
    // здесь выполнить расчет частоты и скважности
    // в зависимости от периода таймера - можно настраивать диапазон измеряемых частот
    if(device.input[DI].mode != IMPULSE_MEASURE)
        return;

    // TIM2
    if((DI == 0)
    || (DI == 1))
    {
        if(device.update & TIM2_UPDATE_FLAG)
        {
            volatile uint32_t* CCR1 = (DI == 0) ? &TIM2->CCR1 : &TIM2->CCR2;
            volatile uint32_t* CCR2 = (DI == 0) ? &TIM2->CCR2 : &TIM2->CCR1;

            uint64_t period = *CCR1 + device.input[DI].counter * PWM_INPUT_TIMEOUT + device.input[DI].duty_counter * PWM_INPUT_TIMEOUT;
            uint64_t duty   = *CCR2 + device.input[DI].duty_counter * PWM_INPUT_TIMEOUT;
            if((*CCR1 > 0) || (device.input[DI].counter > 0) || (device.input[DI].duty_counter > 0))
            {
                uint64_t rate = TIM_CLOCK * 100 / ((TIM2->PSC + 1) * period);
                device.input[DI].val_0 = (uint32_t)(rate & 0xFFFFFFFF);
                device.input[DI].val_1 = 1000 * duty / period;
                *CCR1 = 0;
                *CCR2 = 0;
                device.input[DI].counter = 0;
                device.input[DI].duty_counter = 0;
            }
            device.update &= ~(TIM2_UPDATE_FLAG | TIM2_DUTY_FLAG);
        }
    }
    // TIM1
    else if((DI == 4)
        || (DI == 5))
    {
        if(device.update & TIM1_UPDATE_FLAG)
        {
            volatile uint32_t* CCR1 = (DI == 4) ? &TIM1->CCR1 : &TIM1->CCR2;
            volatile uint32_t* CCR2 = (DI == 4) ? &TIM1->CCR2 : &TIM1->CCR1;

            uint64_t period = *CCR1 + device.input[DI].counter * PWM_INPUT_TIMEOUT + device.input[DI].duty_counter * PWM_INPUT_TIMEOUT;
            uint64_t duty   = *CCR2 + device.input[DI].duty_counter * PWM_INPUT_TIMEOUT;
            if((*CCR1 > 0) || (device.input[DI].counter > 0) || (device.input[DI].duty_counter > 0))
            {
                uint64_t rate = TIM_CLOCK * 100 / ((TIM1->PSC + 1) * period);
                device.input[DI].val_0 = (uint32_t)(rate & 0xFFFFFFFF);
                device.input[DI].val_1 = 1000 * duty / period;
                *CCR1 = 0;
                *CCR2 = 0;
                device.input[DI].counter = 0;
                device.input[DI].duty_counter = 0;
            }
            device.update &= ~(TIM1_UPDATE_FLAG | TIM1_DUTY_FLAG);
        }
    }
}

void mbd_process(void)
{
    int8_t ret = 0;
    for(uint8_t DI = 0; DI < MBD_NUM_INPUTS; ++DI)
    {
        switch(device.input[DI].mode)
        {
            case COMPARATOR: // в этом режиме не происходит обработки, чтение из регистра
                update_masked();
                fill_comparator_registers(DI);
                break;
            case FLUTTER_REDUCTION: // устранение дребезга
                ret = process_flutter_reduction(DI);
                if(ret)
                    fill_flutter_registers(DI);
                break;
            case IMPULSE_COUNT: // 2
                // счет импульсов низкочастотный, обработка после подавления дребезга
                ret = process_flutter_reduction(DI);
                if(ret)
                    process_impulse_count(DI);
                break;
            case ACTIVE_COUNT: // 3
                // счет импульсов низкочастотный, без подавления дребезга
                process_active_count(DI);
                break;
            case RATE_MEASURE_SOFT: // 4 - здесь предполагается, что сигнал не дребезжащий, вход таймера
                process_rate_measure_soft(DI);
                break;
            case RATE_MEASURE_HARD: // 5 - здесь предполагается, что сигнал не дребезжащий, вход таймера
                process_rate_measure_hard(DI);
                break;
            case IMPULSE_MEASURE: // 6 - не дребезжащий, вход таймера
                process_impulse_measure(DI);
                break;
            default:
                break;                
        }
    }
}

extern UART_HandleTypeDef huart4;

int8_t mbd_rs485_init(uint32_t baudrate)
{
    if(!baudrate)
        return -1;
    HAL_NVIC_DisableIRQ(USART3_8_IRQn);
    HAL_UART_DeInit(&huart4);
    huart4.Init.BaudRate = baudrate;
    huart4.Init.WordLength = UART_WORDLENGTH_8B;
    huart4.Init.StopBits = UART_STOPBITS_1;
    huart4.Init.Parity = UART_PARITY_NONE;
    huart4.Init.Mode = UART_MODE_TX_RX;
    huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart4.Init.OverSampling = UART_OVERSAMPLING_16;
    huart4.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
    huart4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
    HAL_RS485Ex_Init(&huart4, UART_DE_POLARITY_HIGH, 31, 31);
    if (HAL_UART_Init(&huart4) != HAL_OK)
    {
        return -1;
    }
    dma_usart_idle_reinit();
    HAL_NVIC_EnableIRQ(USART3_8_IRQn);
    return 0;
}

void mbd_init(void)
{
    update_input();
    device.and_mask = 0xFFFF;
    device.is_init = false;
    device.baudrate = huart4.Init.BaudRate;

    if(ext_mem_load())
    {
        ERROR(CONFIG_LOAD_ERROR);
        input_configure(0, 0);
        input_configure(1, 0);
        input_configure(2, 0);
        input_configure(3, 0);
        input_configure(4, 0);
        input_configure(5, 0);
        input_configure(6, 0);
        input_configure(7, 0);
    }
    mbd_rs485_init(device.baudrate);
}

bool is_valid(uint8_t DI, uint32_t new_value, uint32_t last_value)
{
    if(0 == device.input[DI].param_2)
        return true;
    int32_t delta;
    if(new_value > last_value)
    {
        delta = new_value - last_value;
    }else{
        delta = last_value - new_value;
    }
    if(delta < 0)
        return false;
    if((uint32_t)delta > device.input[DI].param_2)
        return false;
    return true;
}

void update_pwm_input(TIM_HandleTypeDef *htim)
{
    if(TIM2 == htim->Instance)
    {
        uint8_t DI = (device.input[0].mode == IMPULSE_MEASURE) ? 0 : 1; // active hard input
        if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)
        {
            if(DI == 0){
                device.update |= TIM2_UPDATE_FLAG;
            }else{
                device.update |= TIM2_DUTY_FLAG;
            }
        }
        else if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
        {
            if(DI == 0){
                device.update |= TIM2_DUTY_FLAG;
            }else{
                device.update |= TIM2_UPDATE_FLAG;
            }
        }
    }
    else if(TIM1 == htim->Instance)
    {
        uint8_t DI = (device.input[4].mode == IMPULSE_MEASURE) ? 4 : 5;
        if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)
        {
            if(DI == 4){
                device.update |= TIM1_UPDATE_FLAG;
            }else{
                device.update |= TIM1_DUTY_FLAG;
            }
        }
        else if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
        {
            if(DI == 4){
                device.update |= TIM1_DUTY_FLAG;
            }else{
                device.update |= TIM1_UPDATE_FLAG;
            }
        }
    }
}

#define GET_TIM_CCR(__HANDLE__, __CHANNEL__) \
    (*(__IO uint16_t *)(&((__HANDLE__)->Instance->CCR1) + (__CHANNEL__ - 1)))

uint8_t testt = 0;

void update_rate_measure_hard(uint8_t DI, TIM_HandleTypeDef *htim, uint8_t timchannel)
{
    uint16_t val = GET_TIM_CCR(htim, timchannel);
    int32_t ic_period = 0;
    const uint32_t counter = device.input[DI].counter;

    if(counter < IC_TIMEOUT)
    {
        ic_period = htim->Instance->ARR * counter + val - device.input[DI].last_capture;
        uint8_t sample = device.input[DI].sample % NUM_SAMPLES;
        device.input[DI].ic_period[sample] = ic_period;
        device.input[DI].sample = ++sample;
    }else{
        device.input[DI].flags |= 1;
    }
    device.input[DI].last_capture = val;
    device.input[DI].counter = 0;
}

void mbd_on_input_capture(TIM_HandleTypeDef *htim)
{
    uint8_t timchannel = 0;
    uint8_t tmp = htim->Channel;
    while(tmp)
    {
        tmp = tmp >> 1;
        ++timchannel;
    }
    if(htim->Instance == TIM1)
    {
        if((device.input[4].mode == IMPULSE_MEASURE) || (device.input[5].mode == IMPULSE_MEASURE))
        {
            update_pwm_input(htim);
            uint8_t input = (device.input[4].mode == IMPULSE_MEASURE) ? 4 : 5;
            ++device.input[input].result_id;
        }else{
            const uint8_t DI = timchannel + 3; // TIMER CHANNEL !!! (1..4)
            update_rate_measure_hard(DI, htim, timchannel);
            ++device.input[DI].result_id;
        }
    }
    else if(htim->Instance == TIM2)
    {
        if((device.input[0].mode == IMPULSE_MEASURE) || (device.input[1].mode == IMPULSE_MEASURE))
        {
            update_pwm_input(htim);
            uint8_t input = (device.input[0].mode == IMPULSE_MEASURE) ? 0 : 1;
            ++device.input[input].result_id;
        }else{
            const uint8_t DI = timchannel - 1; // DI0 .. DI1
            update_rate_measure_hard(DI, htim, timchannel);
            ++device.input[DI].result_id;
        }
    }
    else if(htim->Instance == TIM3)
    {
        const uint8_t DI = (timchannel == 1) ? 3 : 2; // Hi, Ilias
        update_rate_measure_hard(DI, htim, timchannel);
        ++device.input[DI].result_id;
    }
}

void mbd_on_exti(uint8_t DI, uint16_t val)
{
    uint32_t period;
    if((val <= device.input[DI].last_capture) && (device.input[DI].counter == 0))
    {
        // ситуация когда счетчик еще не успел инкрементироваться
        // такого не должно происходить
        // приоритет прерывания от таймера 7 должен быть равен приоритету exti
        period = (TIM7_PERIOD + 1) + val - device.input[DI].last_capture;
        // устанавливаем флаг ошибки
        device.input[DI].flags |= 1;
    }else{
        period = (TIM7_PERIOD + 1) * device.input[DI].counter + val - device.input[DI].last_capture;
    }

    uint32_t last_value = device.input[DI].val_0; // из регистров val_0

    uint64_t value = TIM_CLOCK * 100 / ((TIM7->PSC + 1) * period);

    if(!is_valid(DI, value / 100, last_value / 100))
        device.input[DI].flags |= 1;
    else
        device.input[DI].flags &= ~1; // if not valid set 0

    device.input[DI].val_0 = (uint32_t)(value & 0xFFFFFFFF);

    ++device.input[DI].result_id;
    device.input[DI].last_capture = val;
    device.input[DI].counter = 0;
}

void mbd_on_impulse_measure_timeout(TIM_HandleTypeDef *htim)
{
    TIM_TypeDef *TIMER = htim->Instance;
    if(TIMER == TIM1)
    {
        uint8_t DI = (device.input[4].mode == IMPULSE_MEASURE) ? 4 : 5;
        if(device.update & TIM1_DUTY_FLAG)
        {
            ++device.input[DI].counter;
            if(device.input[DI].counter >= IC_TIMEOUT)
                reset_input(DI);
        }else{
            ++device.input[DI].duty_counter;
            if(device.input[DI].duty_counter >= IC_TIMEOUT)
                reset_input(DI);
        }
    }
    else if(TIMER == TIM2)
    {
        uint8_t DI = (device.input[0].mode == IMPULSE_MEASURE) ? 0 : 1;
        if(device.update & TIM2_DUTY_FLAG)
        {
            ++device.input[DI].counter;
            if(device.input[DI].counter >= IC_TIMEOUT)
                reset_input(DI);
        }else{
            ++device.input[DI].duty_counter;
            if(device.input[DI].duty_counter >= IC_TIMEOUT)
                reset_input(DI);
        }
    }
}

void inc_counter(uint8_t DI)
{
    if(device.input[DI].counter < IC_TIMEOUT)
    {
        ++device.input[DI].counter;
    }else{
        device.input[DI].val_0 = 0;
        device.input[DI].val_1 = 0;
        device.input[DI].counter = 0;
    }
}

// timer overflow
// counter increment
void mbd_inc_counter(TIM_HandleTypeDef *htim)
{
    if(htim->Instance == TIM1)
    {
        for(uint8_t DI = 4; DI < 8; ++DI)
        {
            if(device.input[DI].mode > RATE_MEASURE_SOFT)
            {
                inc_counter(DI);
            }
        }
    }
    if(htim->Instance == TIM2)
    {
        for(uint8_t DI = 0; DI < 2; ++DI)
        {
            if(device.input[DI].mode > RATE_MEASURE_SOFT)
            {
                inc_counter(DI);
            }
        }
    }
    if(htim->Instance == TIM3)
    {
        for(uint8_t DI = 2; DI < 4; ++DI)
        {
            if(device.input[DI].mode > RATE_MEASURE_SOFT)
            {
                inc_counter(DI);
            }
        }
    }
}

void mbd_on_ovf(void)
{
    for(uint8_t DI = 0; DI < MBD_NUM_INPUTS; ++DI)
    {
        if(device.input[DI].mode == RATE_MEASURE_SOFT)
        {
            inc_counter(DI);
        }
    }
}

void mbd_input_params(input_params_t* input, uint8_t input_num)
{
    // кастить нельзя потому что input packed
    input->mode = device.input[input_num].mode;
    input->param_1 = device.input[input_num].param_1;
    input->param_2 = device.input[input_num].param_2;
}

void mbd_input_load(input_params_t* input, uint8_t input_num)
{
    if(!input)
    {
        ERROR(MDB_INPUT_CONFIG_ERROR);
        return;
    }
    if(input_configure(input_num, input->mode))
    {
        ERROR(MDB_INPUT_CONFIG_ERROR);
        return;
    }
    device.input[input_num].mode    = input->mode;
    device.input[input_num].param_1 = input->param_1;
    device.input[input_num].param_2 = input->param_2;
}

void mbd_common_params(common_params_t* cp)
{
    cp->addr            = device.addr;
    cp->module_id       = MODULE_ID;
    cp->operation_time  = device.operation_time;
    cp->program_crc     = device.program_crc;
    cp->total_rq        = device.total_rq;   // счетчик запросов
    cp->handled_rq      = device.handled_rq; // счетчик обработанных запросов
    cp->err_crc_rq      = device.err_crc_rq; // счетчик запросов с ошибкой CRC
    cp->err_param_rq    = device.err_param_rq; // счетчик запросов с некорректными параметрами
    cp->err_send        = device.err_send;  // счетчик ошибок при отправке
    cp->and_mask        = device.and_mask;
    cp->or_mask         = device.or_mask;
    cp->change_time     = device.operation_time;
    cp->baudrate        = device.baudrate;
}

void mbd_log_status(void)
{
    log_out("start_time:\t %lu \n",     device.start_time);
    log_out("program_crc:\t %u \n",     device.program_crc);
    log_out("total_rq:\t %lu \n",       device.total_rq);
    log_out("handled_rq:\t %lu \n",     device.handled_rq);
    log_out("err_crc_rq:\t %lu \n",     device.err_crc_rq);
    log_out("err_param_rq:\t %lu \n",   device.err_param_rq);
    log_out("err_send:\t %lu \n",       device.err_send);
    log_out("and_mask:\t 0x%x \n",      device.and_mask);
    log_out("or_mask:\t 0x%x \n",       device.or_mask);
    log_out("change_time:\t %lu\n",     device.change_time);
}

void mbd_log_config(void)
{
    log_out("---------------- DI config ----------------\n");
    log_out("DI baudrate: %lu\n", huart4.Init.BaudRate);
    log_out("DI0: mode: %lu\tparam1: %u\tparam2: %u\n", device.input[0].mode, device.input[0].param_1, device.input[0].param_2);
    log_out("DI1: mode: %lu\tparam1: %u\tparam2: %u\n", device.input[1].mode, device.input[1].param_1, device.input[1].param_2);
    log_out("DI2: mode: %lu\tparam1: %u\tparam2: %u\n", device.input[2].mode, device.input[2].param_1, device.input[2].param_2);
    log_out("DI3: mode: %lu\tparam1: %u\tparam2: %u\n", device.input[3].mode, device.input[3].param_1, device.input[3].param_2);
    log_out("DI4: mode: %lu\tparam1: %u\tparam2: %u\n", device.input[4].mode, device.input[4].param_1, device.input[4].param_2);
    log_out("DI5: mode: %lu\tparam1: %u\tparam2: %u\n", device.input[5].mode, device.input[5].param_1, device.input[5].param_2);
    log_out("DI6: mode: %lu\tparam1: %u\tparam2: %u\n", device.input[6].mode, device.input[6].param_1, device.input[6].param_2);
    log_out("DI7: mode: %lu\tparam1: %u\tparam2: %u\n", device.input[7].mode, device.input[7].param_1, device.input[7].param_2);
}

void mbd_set_common_params(common_params_t* cp)
{
    if(cp->addr)
        device.addr     = cp->addr;
    device.start_time   = cp->operation_time;
    device.program_crc  = cp->program_crc;
    device.total_rq     = cp->total_rq;     // счетчик запросов
    device.handled_rq   = cp->handled_rq;   // счетчик обработанных запросов
    device.err_crc_rq   = cp->err_crc_rq;   // счетчик запросов с ошибкой CRC
    device.err_param_rq = cp->err_param_rq; // счетчик запросов с некорректными параметрами
    device.err_send     = cp->err_send;     // счетчик ошибок при отправке
    device.and_mask     = cp->and_mask;
    device.or_mask      = cp->or_mask;
    device.change_time  = cp->change_time;
    device.baudrate     = cp->baudrate;
}

void mbd_set_baudrate(uint32_t baudrate)
{
    device.baudrate = baudrate;
    mbd_rs485_init(device.baudrate);
}

uint8_t mbd_addr(void)
{
    if(!device.addr)
        return 1;
    return device.addr;
}

#define IS_EOF_INIT() (SPARE_INIT_PORT->ODR & SPARE_INIT_PIN)

bool mbd_can_init(void)
{
    if(device.is_init)
        return false;
    if(IS_EOF_INIT())
        return false;
    return true;
}

bool mbd_is_init(void)
{
    return device.is_init;
}

void mbd_int_init(void)
{
    device.is_init = true;
    INIT_MODE_LED_PORT->ODR |= INIT_MODE_LED_PIN;
    mbd_rs485_init(INT_INIT_BAUDRATE); // 4800
}

void mbd_ext_init(void)
{
    device.is_init = true;
    INIT_MODE_LED_PORT->ODR |= INIT_MODE_LED_PIN;
    mbd_rs485_init(EXT_INIT_BAUDRATE); // 38400
}
