
# тесты
    ✔ тест - запрос некорректных настроек, должно вернуть ошибку @done (19-05-22 17:35)
        если один из входов таймера использует режим 6,
        то данный вход уже нельзя настроить на 5й режим,
        так как таймер уже занят - возвращается ошибка данных в modbus.
    ✔ тест - на очистку всех настроек при перенастройке, настроить на канал сначала режим 6 (например для канала 6), а потом режим 5 для СС3 @done (19-06-05 14:47)
    ✔ фронт срабатывания (передний/задний) в настройки (надо проверить!) - работает не на всех режимах - описать в доке @done (19-06-20 16:01)

    ☐ проверка скорости изменения частоты, флаги недостоверности - разорвать связь -> проверить флаг.

## тест1:
    input_configure(0, IMPULSE_MEASURE);
    input_configure(1, RATE_MEASURE_HARD); // должно проигнорироваться если 0 канал в режиме IMPULSE_MEASURE
    input_configure(2, RATE_MEASURE_HARD);
    input_configure(3, RATE_MEASURE_HARD);
    input_configure(4, IMPULSE_MEASURE);
    input_configure(5, RATE_MEASURE_HARD); // должно проигнорироваться если 0 канал в режиме IMPULSE_MEASURE
    input_configure(6, RATE_MEASURE_HARD); // должно проигнорироваться если 0 канал в режиме IMPULSE_MEASURE
    input_configure(7, RATE_MEASURE_HARD); // должно проигнорироваться если 0 канал в режиме IMPULSE_MEASURE

# Защита от рассинхронизации RS-485
    как здесь http://masters.donntu.org/2007/fvti/arutyunyan/library/art7.htm

# Используемые таймеры:
 - TIM1    - hard pwm input
 - TIM2    - hard pwm input
 - TIM3    - hard input capture
 - TIM7    - soft input capture
 - TIM6    - freertos tick

# Debug
* При генерации кода в cubemx, восстановить значение TICK_INT_PRIORITY = 3, но лучше не запускать cubemx
* При отладке отключить HAL_Tick gdb define killtick
* Для отключения вачдога используй макрос __HAL_DBGMCU_FREEZE_IWDG();
