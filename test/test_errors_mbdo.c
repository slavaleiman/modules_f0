#include "unity.h"
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include "stm32f091xc.h"
#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal_gpio.h"
#include "stm32f0xx_hal_uart.h"
#include "modbus_device.h"
#include "soft_timer.h"
#include "modbus.h"
#include "errors.h"

void dma_usart_idle_reinit(void){}

int8_t ext_mem_load(void){return -1;}
int8_t ext_mem_store(void){return -1;}
uint32_t uwTick = 0;
uint32_t HAL_GetTick(void){return uwTick;}
void     HAL_GPIO_DeInit(GPIO_TypeDef  *GPIOx, uint32_t GPIO_Pin){}
void     HAL_GPIO_Init(GPIO_TypeDef  *GPIOx, GPIO_InitTypeDef *GPIO_Init){}
void HAL_NVIC_DisableIRQ(IRQn_Type IRQn){}
void HAL_NVIC_EnableIRQ(IRQn_Type IRQn){}
void HAL_NVIC_SetPriority(IRQn_Type IRQn,uint32_t PreemptPriority, uint32_t SubPriority){}
HAL_StatusTypeDef HAL_RS485Ex_Init(UART_HandleTypeDef *huart, uint32_t Polarity, uint32_t AssertionTime, uint32_t DeassertionTime){}
HAL_StatusTypeDef HAL_TIM_Base_Init(TIM_HandleTypeDef *htim){}
HAL_StatusTypeDef HAL_TIM_Base_Start_IT(TIM_HandleTypeDef *htim){}
HAL_StatusTypeDef HAL_UART_DeInit (UART_HandleTypeDef *huart){}
HAL_StatusTypeDef HAL_UART_Init(UART_HandleTypeDef *huart){}
UART_HandleTypeDef huart4;
void log_out(const char *fmt, ...){}
uint32_t SystemCoreClock = 48000000;
void TIM_CCxChannelCmd(TIM_TypeDef* TIMx, uint32_t Channel, uint32_t ChannelState){}

HAL_StatusTypeDef HAL_UART_Transmit_DMA(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size){return HAL_OK;}
bool is_usart4_sending = false;

void * xQueueGenericReceive( void * d, void * const pvBuffer, void * xTicksToWait, const void * xJustPeeking ){return false;}
void *xQueueGenericSend( void * xQueue, const void * const pvItemToQueue, void * xTicksToWait, const void * xCopyPosition ){return false;}

int xQueueCreateMutex( const uint8_t ucQueueType ){return 1;}

extern device_t device;
void setUp(void)
{
    memset(&device, 0, sizeof(device_t));
}

void tearDown(void)
{
}

void test_errors_apearance(void)
{
    modbus_init();
    TEST_ASSERT_EQUAL(true, errors_is_error(CONFIG_LOAD_ERROR));

    // EXT_MEM_WRITE_ERROR, // never saved to ext mem
    int8_t ret = mbd_write_reg(27, 0xAA);
    TEST_ASSERT_NOT_EQUAL(0, ret);
    TEST_ASSERT_EQUAL(true, errors_is_error(EXT_MEM_WRITE_ERROR));

    // RS485_OVERRUN_ERROR // tested manualy

    // MODBUS_HOLDING_REG_LOCKED
    // MODBUS_INPUT_REG_LOCKED

    output_params_t* output;
    ret = mbd_write_reg(DO5_MODE_REG, 39);
    TEST_ASSERT_NOT_EQUAL(0, ret);
}

int main(void)
{
    UNITY_BEGIN();
	RUN_TEST(test_errors_apearance);
    return UNITY_END();
}
