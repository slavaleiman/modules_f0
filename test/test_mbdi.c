#include "unity.h"
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include "stm32f091xc.h"
#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal_gpio.h"
#include "stm32f0xx_hal_uart.h"
#include "modbus_device.h"

void dma_usart_idle_reinit(void){}
void errors_on_error(int err){}
int8_t ext_mem_load(void){}
uint32_t uwTick = 0;
uint32_t HAL_GetTick(void){return uwTick;}
void     HAL_GPIO_DeInit(GPIO_TypeDef  *GPIOx, uint32_t GPIO_Pin){}
void     HAL_GPIO_Init(GPIO_TypeDef  *GPIOx, GPIO_InitTypeDef *GPIO_Init){}
void HAL_NVIC_DisableIRQ(IRQn_Type IRQn){}
void HAL_NVIC_EnableIRQ(IRQn_Type IRQn){}
void HAL_NVIC_SetPriority(IRQn_Type IRQn,uint32_t PreemptPriority, uint32_t SubPriority){}
HAL_StatusTypeDef HAL_RS485Ex_Init(UART_HandleTypeDef *huart, uint32_t Polarity, uint32_t AssertionTime, uint32_t DeassertionTime){}
HAL_StatusTypeDef HAL_TIM_Base_Init(TIM_HandleTypeDef *htim){}
HAL_StatusTypeDef HAL_TIM_Base_Start_IT(TIM_HandleTypeDef *htim){}
HAL_StatusTypeDef HAL_UART_DeInit (UART_HandleTypeDef *huart){}
HAL_StatusTypeDef HAL_UART_Init(UART_HandleTypeDef *huart){}
UART_HandleTypeDef huart4;
void log_out(const char *fmt, ...){}
void soft_timer_init(uint32_t period, void (*cb)(uint8_t), uint8_t is_one_shot, uint8_t index){}
void soft_timer_stop(uint8_t id){}
uint32_t SystemCoreClock = 48000000;
void TIM_CCxChannelCmd(TIM_TypeDef* TIMx, uint32_t Channel, uint32_t ChannelState){}
int8_t ext_mem_store(void){return 0;}

extern device_t device;
void setUp(void)
{
    memset(&device, 0, sizeof(device_t));
}

void tearDown(void)
{
}

void init_flutter_reduction(uint8_t DI);

int8_t process_flutter_reduction_test(uint8_t DI)
{
    bool filter_out = (device.input[DI].flags & 1) ? 1 : 0;
	// update_masked(); // commented cause to change masked_output which was set earlier
    bool filter_input = (device.masked_input & (1 << DI)) ? 1 : 0;
    if(filter_input == filter_out) // если значение совпадает с текущим
    {
        // декремент счетчика
        if(device.input[DI].counter > 0)
        {
            --device.input[DI].counter;
        }
    }else{
        if(device.input[DI].counter < 0x3F) // проверка на переполнение 6 bit value
            ++device.input[DI].counter;
        if(filter_input)
        { // здесь копим единицы
            uint16_t upper_threshold = (device.input[DI].param_1 >> 9) & 0x3F;
            if(device.input[DI].counter > upper_threshold)
            {
                device.input[DI].flags |= 1;
                device.input[DI].counter = 0;
                return 1;
            }
        }else{
            uint16_t lower_threshold = (device.input[DI].param_1 >> 3) & 0x3F;
            if(device.input[DI].counter > lower_threshold)
            {
                device.input[DI].flags &= ~1;
                device.input[DI].counter = 0;
                return -1;
            }
        }
    }
    return 0;	
}

void test_flutter_init_state_should_not_change_counter_from1(void)
{
	uint8_t di = 0;
	device.masked_input = 1; // init state 1
    device.input[0].param_1 = 0;
    device.input[0].param_1 |= (5 << 3) | (5 << 9);

	init_flutter_reduction(di);

	process_flutter_reduction_test(di);

	TEST_ASSERT_EQUAL(0, device.input[0].counter);

	device.masked_input = 0;
	process_flutter_reduction_test(di);
	TEST_ASSERT_EQUAL(1, device.input[0].counter);

	device.masked_input = 0;
	process_flutter_reduction_test(di);
	TEST_ASSERT_EQUAL(2, device.input[0].counter);

	// tipo drebezg
	device.masked_input = 1;
	process_flutter_reduction_test(di);
	TEST_ASSERT_EQUAL(1, device.input[0].counter);
}

void test_flutter_init_state_should_not_change_counter_from0(void)
{
	uint8_t di = 0;
	device.masked_input = 0; // init state 0
    device.input[0].param_1 |= (5 << 3) | (5 << 9);
	device.input[0].param_1 |= 1;

	init_flutter_reduction(di);
	process_flutter_reduction_test(di);
	TEST_ASSERT_EQUAL(0, device.input[0].counter);

	device.masked_input = 1;
	process_flutter_reduction_test(di);
	TEST_ASSERT_EQUAL(1, device.input[0].counter);

	device.masked_input = 1;
	process_flutter_reduction_test(di);
	TEST_ASSERT_EQUAL(2, device.input[0].counter);

	device.masked_input = 1;
	process_flutter_reduction_test(di);
	TEST_ASSERT_EQUAL(3, device.input[0].counter);

	device.masked_input = 0;
	process_flutter_reduction_test(di);
	TEST_ASSERT_EQUAL(2, device.input[0].counter);
}

void _update_rate_measure_hard(uint8_t DI, uint32_t ARR, uint32_t PSC, uint16_t ccr)
{
    // uint16_t ccr = GET_TIM_CCR(htim, timchannel);
    uint16_t counter = device.input[DI].counter;
    int32_t ic_period;

    if(ccr > device.input[DI].last_capture)
    {
        ic_period = ARR * counter + ccr - device.input[DI].last_capture;

        printf("period : 0x%x, ccr: %d, prev_ccr:%d, ", ic_period, ccr, device.input[DI].last_capture);
        if(ic_period > 0)
        {
            uint64_t value = (uint64_t)48000000 * 100 / ((PSC + 1) * ic_period);
            device.input[DI].val_0 = (uint32_t)(value & 0xFFFFFFFF);
        }else{
            printf("Xyu Te6e!!!!\n");
            device.input[DI].flags |= 1; // not valid
        }
    }
    device.input[DI].last_capture = ccr;
    device.input[DI].counter = 0;
}

void test_rate_measure_hard(void)
{
    // test value after error val

    device.input[0].last_capture = 1000;
    uint16_t val = 0xFFFE;
    for(int32_t i = 0; i < 0xFFFF / 2; i += 10)
    {
        val = i - 2;
        _update_rate_measure_hard(0, 0xFFFF, 0, val);
        printf("val: %d val_0: %d\n", val, device.input[0].val_0);
    }
    // TEST_ASSERT_EQUAL(0, device.input[0].val_0);

    // test value after error val
    // device.input[0].last_capture = 0;
    // _update_rate_measure_hard(0, 0xFFFF, 0, 0xFFFF);
    // TEST_ASSERT_EQUAL(0, device.input[0].val_0);
    // printf("bad val: val_0: %d\n", device.input[0].val_0);
}

static void update_masked(void)
{
}

void process_active_count_test(uint8_t DI)
{
    uint32_t ticks = device.operation_time;
    update_masked();

    if(!device.input[DI].last_capture)
        device.input[DI].last_capture = ticks;

    if(((device.masked_input >> DI) & 1) == (device.input[DI].param_1 & 1))
    {
        uint32_t old_val = device.input[DI].val_0;
        device.input[DI].param_2 += (ticks - device.input[DI].last_capture);
        device.input[DI].val_0 = device.input[DI].param_2;
        if(device.input[DI].val_0 < old_val)
            device.input[DI].flags |= 1 << 15; // oveflow flag
    }
    device.input[DI].last_capture = ticks;

    if(device.input[DI].param_1 & 2) // сброс флага overflow
    {
        device.input[DI].flags &= ~(1 << 15);
        device.input[DI].param_1 &= ~(1 << 1);
    }
}

void test_active_count_from0(void)
{
    uint8_t di = 0;
    input_configure(di, 3);

    device.masked_input = 0; // init state 0
    device.input[0].param_1 = 1; // active state 

    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(0, device.input[0].val_0);

    device.masked_input = 1;
    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(1, device.input[0].val_0);

    device.masked_input = 1;
    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(2, device.input[0].val_0);

    device.masked_input = 1;
    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(3, device.input[0].val_0);

    device.masked_input = 0;
    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(3, device.input[0].val_0);
}

void test_active_count_from1(void)
{
    uint8_t di = 0;
    input_configure(di, 3);

    device.masked_input = 1; // init state 1
    device.input[0].param_1 = 0; // active 0

    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(0, device.input[0].val_0);

    device.masked_input = 0;
    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(1, device.input[0].val_0);

    device.masked_input = 0;
    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(2, device.input[0].val_0);

    device.masked_input = 0;
    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(3, device.input[0].val_0);

    device.masked_input = 1;
    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(3, device.input[0].val_0);
}

void test_active_count_must_count_if_init_level_equal_active_level(void)
{
    uint8_t di = 0;
    input_configure(di, 3);

    device.masked_input = 1; // init state 1
    device.input[0].param_1 = 1;

    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(0, device.input[0].val_0); // first get last capture
    // device.input[0].last_capture = device.operation_time;
    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(1, device.input[0].val_0);

    device.masked_input = 0;
    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(1, device.input[0].val_0);

    device.masked_input = 1;
    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(2, device.input[0].val_0);

    device.masked_input = 1;
    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(3, device.input[0].val_0);

    device.masked_input = 0;
    ++device.operation_time;
    process_active_count_test(di);
    TEST_ASSERT_EQUAL(3, device.input[0].val_0);
}

void test_input_config(void)
{
    mbd_write_reg(DI0_MODE_REG, 6);
    mbd_write_reg(PARAM1_REG_0, 1);
    TEST_ASSERT_EQUAL(6, device.input[0].mode);
    TEST_ASSERT_EQUAL(1, device.input[0].param_1);
    
    device.input[0].mode = 1;
    device.input[0].param_1 = 0xD3;
    device.input[0].param_2 = 0xD3;

    mbd_write_reg(DI0_MODE_REG, 3);
    TEST_ASSERT_EQUAL(3, device.input[0].mode);
    printf("param_1: %d, param_2: %d\n", device.input[0].param_1, device.input[0].param_2);
}

int main(void)
{
    UNITY_BEGIN();
	// RUN_TEST(test_flutter_init_state_should_not_change_counter_from1);
	// RUN_TEST(test_flutter_init_state_should_not_change_counter_from0);
    // RUN_TEST(test_active_count_from0);
    // RUN_TEST(test_active_count_from1);
    // RUN_TEST(test_active_count_must_count_if_init_level_equal_active_level);
    // RUN_TEST(test_input_config);
	RUN_TEST(test_rate_measure_hard);

    return UNITY_END();
}
