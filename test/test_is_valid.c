#include "unity.h"
#include <stdbool.h>

uint32_t valid_delta;

void initall(void)
{
}

bool is_valid(uint8_t DI, uint32_t new_value, uint32_t last_value)
{
    (void)DI;
    int32_t delta;
    // if(device.input[DI].valid_delta == 0)
    if(valid_delta == 0)
        return true;

    if(new_value > last_value)
    {
        // в аргументы передаются значения частот множенные на 100,
        // поэтому здесь нет множителя
        delta = new_value - last_value;
        printf("1: delta = %d\n", delta);
    }else{
        delta = last_value - new_value;
        printf("2: delta = %d\n", delta);
    }
    if(delta < 0)
        return false;
    if((uint32_t)delta > valid_delta)
        return false;
    return true;
}

void test_oneofzero(void)
{
    valid_delta = 500;
    TEST_ASSERT_EQUAL(0, is_valid(0, 0, 0xFFFF * 100));
    TEST_ASSERT_EQUAL(0, is_valid(0, 0xFFFF * 100, 0));
}

void test_100(void)
{
    uint16_t i = 6257;
    valid_delta = 100;
    TEST_ASSERT_EQUAL(1, is_valid(0, i, i - valid_delta + 1));
    TEST_ASSERT_EQUAL(1, is_valid(0, i - 1, i));
    TEST_ASSERT_EQUAL(1, is_valid(0, i - 1, i));
}

void setUp(void)
{
    valid_delta = 0;
}

void tearDown(void)
{
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_oneofzero);
    RUN_TEST(test_100);

    return UNITY_END();
}
