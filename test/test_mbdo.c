#include "unity.h"
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include "stm32f091xc.h"
#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal_gpio.h"
#include "stm32f0xx_hal_uart.h"
#include "modbus_device.h"
#include "soft_timer.h"

void dma_usart_idle_reinit(void){}
void errors_on_error(int err){}
int8_t ext_mem_load(void){}
int8_t ext_mem_store(void){}
uint32_t uwTick = 0;
uint32_t HAL_GetTick(void){return uwTick;}
void     HAL_GPIO_DeInit(GPIO_TypeDef  *GPIOx, uint32_t GPIO_Pin){}
void     HAL_GPIO_Init(GPIO_TypeDef  *GPIOx, GPIO_InitTypeDef *GPIO_Init){}
void HAL_NVIC_DisableIRQ(IRQn_Type IRQn){}
void HAL_NVIC_EnableIRQ(IRQn_Type IRQn){}
void HAL_NVIC_SetPriority(IRQn_Type IRQn,uint32_t PreemptPriority, uint32_t SubPriority){}
HAL_StatusTypeDef HAL_RS485Ex_Init(UART_HandleTypeDef *huart, uint32_t Polarity, uint32_t AssertionTime, uint32_t DeassertionTime){}
HAL_StatusTypeDef HAL_TIM_Base_Init(TIM_HandleTypeDef *htim){}
HAL_StatusTypeDef HAL_TIM_Base_Start_IT(TIM_HandleTypeDef *htim){}
HAL_StatusTypeDef HAL_UART_DeInit (UART_HandleTypeDef *huart){}
HAL_StatusTypeDef HAL_UART_Init(UART_HandleTypeDef *huart){}
UART_HandleTypeDef huart4;
void log_out(const char *fmt, ...){}
uint32_t SystemCoreClock = 48000000;
void TIM_CCxChannelCmd(TIM_TypeDef* TIMx, uint32_t Channel, uint32_t ChannelState){}

extern device_t device;
void setUp(void)
{
    memset(&device, 0, sizeof(device_t));
}

void tearDown(void)
{
}

void test_operation_time_ovf(void)
{
	uwTick = 0xFFFFFFFF;
	mbd_update_time();
	mbd_process();
	printf("0x%x\n", device.operation_time);
	uwTick += 1001; // + 1,000s
	mbd_update_time();
	mbd_process();	
	printf("0x%x\n", device.operation_time);
	TEST_ASSERT_GREATER_THAN(0, device.operation_time);
}

void test_status(void)
{
	uint16_t status = mbd_update_status();
	TEST_ASSERT_EQUAL(0, status);
}

static uint32_t get_tim_chan(uint8_t DO)
{
    uint32_t channel = 0;
    switch(DO)
    {
    	case 0: channel = TIM_CHANNEL_4;
    		break;
    	case 1: channel = TIM_CHANNEL_3;
    		break;
    	case 2: channel = TIM_CHANNEL_2;
    		break;
    	case 3: channel = TIM_CHANNEL_1;
    		break;
    	case 4: channel = TIM_CHANNEL_4;
    		break;
    	case 5: channel = TIM_CHANNEL_3;
    		break;
    	case 6: channel = TIM_CHANNEL_2;
    		break;
    	case 7: channel = TIM_CHANNEL_1;
    		break;
        default:
            break;
    }
    return channel;
}

void test_get_tim_chan(void)
{
	uint8_t i = 8;
	while(i --> 0)
	{
		printf("do: %d %x\n", i, get_tim_chan(i));
	}
}

void test_set_mode(void)
{
    printf("tes set mode\n");
    mbd_init();
    mbd_process();

    printf("set 3 mode\n");
    mbd_write_reg(DO0_MODE_REG, 3);
    mbd_process();
    mbd_write_reg(PARAM1_REG_0, 1000);
    mbd_write_reg(PARAM2_REG_0, 50);
    mbd_write_reg(PARAM3_REG_0, 1000);
    mbd_process();
    TEST_ASSERT_EQUAL(3, device.output[0].mode);

    uint32_t i = 150;
    while(i --> 0)
    {
        soft_timer_tick();
        soft_timer_poll();
    }

    printf("set 2 mode\n");
    mbd_write_reg(DO0_MODE_REG, 2);
    mbd_write_reg(PARAM1_REG_0, 1000);
    // mbd_write_reg(PARAM2_REG_0, 2);
    mbd_process();
    TEST_ASSERT_EQUAL(2, device.output[0].mode);

    i = 150;
    while(i --> 0)
    {
        soft_timer_tick();
        soft_timer_poll();
    }
}

static void test_init_pwm_timer(void)
{
    uint16_t PSC = 4 - 1;
    for(uint16_t rate = 1; rate <= 20; rate += 1)
    {
        uint32_t arr = SystemCoreClock / ((PSC + 1) * rate);
        while(arr > 0xFFFF)
        {
            if(PSC < 0xFFFF)
                PSC += 1;
            else
                break;
            printf("----- inc psc %d --\n", PSC);
            arr = SystemCoreClock / ((PSC + 1) * rate);
        }
        printf("rate:%d ARR:%d PSC:%d\n", rate, arr, PSC);
    }
}

int main(void)
{
    UNITY_BEGIN();
	// RUN_TEST(test_operation_time_ovf);
	// RUN_TEST(test_status);
    // RUN_TEST(test_get_tim_chan);
    // RUN_TEST(test_set_mode);
    // RUN_TEST(test_init_pwm_timer);
	RUN_TEST(test_init_pwm_timer);

    return UNITY_END();
}
